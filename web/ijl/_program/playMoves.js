/************************************************************
 *  playMoves.js
 *  Copyright 1999, Hyoungsoo Yoon. All Rights Reserved.
 *  This software is distributed on an "AS IS" basis, 
 *  WITHOUT WARRANTY OF ANY KIND, either express or implied.
 ************************************************************/

var milliSec = 1000;
function playMoves()
{
  if (currentNumber != move.length) {
     var mv = "Move" + currentNumber;
     if(isNav4) {
       document.layers[mv].visibility = "show";
     } else if(isIE4) {
       document.all[mv].style.visibility = "visible";
     } else {
       currentNumber = move.length;
       return;
     }
     window.setTimeout ("playMoves();",milliSec);
     currentNumber++;
  }
}

function moveForward()
{
  if (currentNumber != move.length) {
     var mv = "Move" + currentNumber;
     if(isNav4) {
       document.layers[mv].visibility = "show";
     } else if(isIE4) {
       document.all[mv].style.visibility = "visible";
     }
     currentNumber++;
  }
}

function moveBackward()
{
  if (currentNumber != 0) {
     currentNumber--;
     var mv = "Move" + currentNumber;
     if(isNav4) {
       document.layers[mv].visibility = "hide";
     } else if(isIE4) {
       document.all[mv].style.visibility = "hidden";
     }
  }
}

