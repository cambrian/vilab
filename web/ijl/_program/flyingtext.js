//
// flyingtext.js
//

function FlyFromTop (oDiv,stopY)
{
   oDiv.style.pixelTop += 10;
   if (oDiv.style.pixelTop >= stopY) {
     oDiv.style.pixelTop = stopY;
     currentSequence++;     
     window.setTimeout ("flyIn();",500);
   }
   else
   {
      copyDiv = oDiv;
      copyY   = stopY;
      window.setTimeout ("FlyFromTop (copyDiv,copyY);", 10);
   }     
}

function FlyFromBottom (oDiv,stopY)
{
   oDiv.style.pixelTop -= 10;
   if (oDiv.style.pixelTop <= stopY) {
     oDiv.style.pixelTop = stopY;
     currentSequence++;     
     window.setTimeout ("flyIn();",500);
   }
   else
   {
      copyDiv = oDiv;
      copyY   = stopY;
      window.setTimeout ("FlyFromBottom (copyDiv,copyY);", 10);
   }     
}

function FlyFromRight (oImg,stopX)
{
   oImg.style.pixelLeft -= 10;
   if (oImg.style.pixelLeft <= stopX) {
     oImg.style.pixelLeft = stopX;
     currentSequence++;     
     window.setTimeout ("flyIn();",500);
   }   
   else
   {
     copyImg = oImg;
     copyX   = stopX;
     window.setTimeout ("FlyFromRight (copyImg,copyX);",10);
   }
}

function FlyFromLeft (oImg,stopX)
{
   oImg.style.pixelLeft += 10;
   if (oImg.style.pixelLeft >= stopX) {
     oImg.style.pixelLeft = stopX;
     currentSequence++;     
     window.setTimeout ("flyIn();",500);
   }
   else
   {
     copyImg = oImg;
     copyX   = stopX;
     window.setTimeout ("FlyFromLeft (copyImg,copyX);",10);
   }
}

function flyIn()
{ 
   if (currentSequence != flyInSequence.length) {
      i = currentSequence;
      if(flyInDirection[i] == 'top') {
         // Fly From Top
         flyInSequence[i].style.pixelTop = document.body.offsetTop 
                                    - flyInSequence[i].offsetTop  
                                    - flyInSequence[i].offsetHeight;
         flyInSequence[i].style.visibility= "visible";
         window.setTimeout ("FlyFromTop (flyInSequence[i],0);",10);
      }
      else if(flyInDirection[i] == 'bottom') {
         // Fly From Bottom
         flyInSequence[i].style.pixelTop = document.body.offsetHeight
                                    - flyInSequence[i].offsetTop;
         flyInSequence[i].style.visibility= "visible";
         window.setTimeout ("FlyFromBottom (flyInSequence[i],0);",10);
      }
      else if(flyInDirection[i] == 'right') {
         // Fly From Right
         flyInSequence[i].style.pixelLeft = document.body.offsetWidth
                                    - flyInSequence[i].offsetLeft;
         flyInSequence[i].style.visibility= "visible";
         window.setTimeout ("FlyFromRight (flyInSequence[i],0);",10);
      }
      else if(flyInDirection[i] == 'left') {
         // Fly From Left
         flyInSequence[i].style.pixelLeft = document.body.offsetLeft
                                    - flyInSequence[i].offsetLeft
                                    - flyInSequence[i].offsetWidth;
         flyInSequence[i].style.visibility= "visible";
         window.setTimeout ("FlyFromLeft (flyInSequence[i],0);",10);
      }
   }
}

