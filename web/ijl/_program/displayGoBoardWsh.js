/************************************************************
 *  displayGoBoardWsh.js
 *  Copyright 1999, Hyoungsoo Yoon. All Rights Reserved.
 *  This software is distributed on an "AS IS" basis, 
 *  WITHOUT WARRANTY OF ANY KIND, either express or implied.
 ************************************************************/

// Global variables
var mSGF = "";
var boardColor = "gold";
var backgroundColor = "ivory";
var commentsHolder = "";

var fso, hFile;
var filePath = "";
var fileTitle = "";
var fileHeader = "";
function CreateHtmlFile()
{
  fso = new ActiveXObject("Scripting.FileSystemObject");
  hFile = fso.CreateTextFile(filePath, true);
  hFile.WriteLine('<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0//EN">');
  hFile.WriteLine('<HTML lang="en">') ;
  hFile.WriteLine('<HEAD>') ;
  hFile.WriteLine('<META http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">');
  hFile.WriteLine('<TITLE>') ;
  hFile.WriteLine(fileTitle) ;
  hFile.WriteLine('</TITLE>') ;

  // Write style tags here.
  hFile.WriteLine('<STYLE TYPE="text/css">');
  hFile.Write('BODY { ');
  hFile.Write('background: ' + backgroundColor + '; ');
  hFile.WriteLine('}');
  hFile.Write('TABLE.goboard { ');
  hFile.Write('margin: 0px; ');
  hFile.Write('padding: 0.0in; ');
  hFile.Write('border: 0.0in ; ');
  hFile.Write('background: ' + boardColor + '; ');
  hFile.WriteLine('}');
  hFile.Write('TABLE.goboard TR { ');
  hFile.Write('margin: 0in; ');
  hFile.Write('padding: 0in; ');
  hFile.Write('border: 0in; ');
  hFile.WriteLine('}');
  hFile.Write('TABLE.goboard TD { ');
  hFile.Write('margin: 0in; ');
  hFile.Write('padding: 0in; ');
  hFile.WriteLine('}');
  hFile.Write('DIV.arrows { ');
  hFile.Write('text-align: center; ');
  hFile.WriteLine('}');
  hFile.Write('DIV.comments { ');
  hFile.Write('text-align: left; ');
  hFile.Write('margin-left: 0.2in; ');
  hFile.WriteLine('}');
  hFile.WriteLine('</STYLE>');

  // Browser sniffer
  hFile.WriteLine('<SCRIPT type="text/javascript" language="JavaScript">');
  hFile.WriteLine('  var isNav4, isIE4;');
  hFile.WriteLine('  if (parseInt(navigator.appVersion.charAt(0)) >= 4) {');
  hFile.WriteLine('     if (navigator.appName == "Netscape") {');
  hFile.WriteLine('	isNav4 = true;');
  hFile.WriteLine('     } else if (navigator.appVersion.indexOf("MSIE") != -1) {');
  hFile.WriteLine('	isIE4 = true;');
  hFile.WriteLine('     }');
  hFile.WriteLine('  }');
  hFile.WriteLine('</SCR' + 'IPT>');
  // Go board
  hFile.WriteLine('<SCRIPT type="text/javascript" language="JavaScript"');
  hFile.WriteLine('src="goBoard.js">');
  hFile.WriteLine('</SCR' + 'IPT>');
  hFile.WriteLine('<SCRIPT type="text/javascript" language="JavaScript"');
  hFile.WriteLine('src="boardImages.js">');
  hFile.WriteLine('</SCR' + 'IPT>');

  // Move functions
  hFile.WriteLine('<SCRIPT type="text/javascript" language="JavaScript"');
  hFile.WriteLine('src="displayMoves.js">');
  hFile.WriteLine('</SCR' + 'IPT>');

  hFile.WriteLine('</HEAD>') ;
  hFile.WriteLine('<BODY>') ;
}

function CloseHtmlFile()
{
  hFile.WriteLine('</BODY>');
  hFile.WriteLine('</HTML>');
  hFile.Close();
}

// Default (empty) board
var board = new Array();
for(k=0;k<19;k++) {
  board[k] = new Array("2", "5", "5", "5", "5", "5", "5", "5", "5",
             "5", "5", "5", "5", "5", "5", "5", "5", "5", "8");
}
board[0] = ["1", "4", "4", "4", "4", "4", "4", "4", "4",
            "4", "4", "4", "4", "4", "4", "4", "4", "4", "7"];
board[18] = ["3", "6", "6", "6", "6", "6", "6", "6", "6",
            "6", "6", "6", "6", "6", "6", "6", "6", "6", "9"];
board[3][3] = "H"; board[3][9] = "H"; board[3][15] = "H";
board[9][3] = "H"; board[9][9] = "H"; board[9][15] = "H";
board[15][3] = "H"; board[15][9] = "H"; board[15][15] = "H";

// Move sequence
var move = new Array();
var mvAdded = new Array();

// VW rectangle
var xstart = 0;
var ystart = 0;
var xend =  18;
var yend =  18;

// Parse "Minimal-SGF" String mSGF
function  parseMSgf() {
  var sgf = mSGF.split(";");
  var a = "a";

  var numAdded = 0;
  var mm = 0;
  for(m=1;m<sgf.length;m++) {
    var firstChar = sgf[m].charAt(0);
    if(firstChar == "V") { 
      var secondChar = sgf[m].charAt(1);
      if(secondChar == "W") {
	xstart = sgf[1].charCodeAt(3) - a.charCodeAt(0);
	ystart = sgf[1].charCodeAt(4) - a.charCodeAt(0);
	xend =  sgf[1].charCodeAt(6) - a.charCodeAt(0);
	yend =  sgf[1].charCodeAt(7) - a.charCodeAt(0);
      } // else if(secondChar == "[") it's value attribute.
    } else if(firstChar == "A") {
      var aColor = sgf[m].charAt(1);
      if(aColor == "B" || aColor == "W" || aColor == "E") {
        var mvAd = sgf[m].split("[");
        for(h=1;h<mvAd.length;h++) {
          var r = mvAd[h].charCodeAt(0) - a.charCodeAt(0);
          var s = mvAd[h].charCodeAt(1) - a.charCodeAt(0);
          mvAdded[numAdded] = new Object();
          mvAdded[numAdded].color = aColor;
          mvAdded[numAdded].x = r;
          mvAdded[numAdded].y = s;
          numAdded++;
        }
      } //else ignore...
    } else if(firstChar == "E") {
      var mvDel = sgf[m].split("[");
      for(h=1;h<mvDel.length;h++) {
        var c = mvDel[h].charCodeAt(0) - a.charCodeAt(0);
        var d = mvDel[h].charCodeAt(1) - a.charCodeAt(0);
	  move[mm] = new Object();
	  move[mm].color = "E";
	  move[mm].x = c;
	  move[mm].y = d;
	  mm++;
      }
    } else if(firstChar == "B" || firstChar == "W") {
      var p = sgf[m].charCodeAt(2) - a.charCodeAt(0);
      var q = sgf[m].charCodeAt(3) - a.charCodeAt(0);
      move[mm] = new Object();
      move[mm].color = sgf[m].charAt(0);
      move[mm].x = p;
      move[mm].y = q;
      mm++;
    } //else do nothing...
  }
}

function  displayGoBoard() {

  // [1] "Parse" Minimal-SGF String
  parseMSgf();

  // [2] Write move sequence
  hFile.WriteLine('<SCRIPT type="text/javascript" language="JavaScript">');
  hFile.WriteLine('var move = new Array();');
  for(n=0;n<move.length;n++) {
    hFile.WriteLine(' move['+n+'] = new Object();');
    hFile.Write(' move['+n+'].color = "' + move[n].color + '";');
    hFile.Write(' move['+n+'].x = ' + move[n].x + ';');
    hFile.WriteLine(' move['+n+'].y = ' + move[n].y + ';');
  }
  hFile.WriteLine('var mvAdded = new Array();');
  for(n=0;n<mvAdded.length;n++) {
    hFile.WriteLine(' mvAdded['+n+'] = new Object();');
    hFile.Write(' mvAdded['+n+'].color = "' + mvAdded[n].color + '";');
    hFile.Write(' mvAdded['+n+'].x = ' + mvAdded[n].x + ';');
    hFile.WriteLine(' mvAdded['+n+'].y = ' + mvAdded[n].y + ';');
  }
  hFile.WriteLine('</SCR' + 'IPT>');

  // [3] Write heading
  hFile.WriteLine('<H2>');
  hFile.WriteLine(fileHeader);
  hFile.WriteLine('</H2>');

  var boardWidth = (xend-xstart+1)*15;
  var boardHeight = (yend-ystart+1)*15;

  // [4-1] Draw outer table  
  hFile.Write('<TABLE>');
  hFile.Write('<TBODY>');
  hFile.Write('<TR>');
  hFile.WriteLine('<TD noWrap vAlign="top">');

  // [5] "Draw" Go Board
  hFile.Write('<TABLE class="goboard" id="boardlayer">');
  hFile.WriteLine('<TBODY>');
  hFile.Write('<TR>');
  hFile.WriteLine('<TD noWrap>');
  for(j=ystart;j<=yend;j++) {
    for(i=xstart;i<=xend;i++) {
      hFile.Write('<IMG name="board_'+i+'_'+j+'" src="gifs/' + board[i][j] + '.gif">');
    }
    hFile.WriteLine('<BR>');
  }
  hFile.Write('</TD>');
  hFile.WriteLine('</TR>');
  hFile.Write('</TBODY>');
  hFile.WriteLine('</TABLE>');

  // [6] Show move-buttons
  hFile.Write('<DIV class="arrows" ');
  hFile.WriteLine('style="width: ' + boardWidth + ';">');
  hFile.Write('&nbsp;<A href="javascript:moveToBeginning();" ');
  hFile.Write('onMouseOver="self.status=\'Move To Beginning\'; return true;"');
  hFile.Write('onMouseOut="self.status=\'\'; return true;"');
  hFile.Write('title="Move To Beginning"');
  hFile.WriteLine('><IMG src="gifs/lleft.gif" border="0"></A>&nbsp;&nbsp;');
  hFile.Write('<A href="javascript:moveBackward();" ');
  hFile.Write('onMouseOver="self.status=\'Move Backward\'; return true;"');
  hFile.Write('onMouseOut="self.status=\'\'; return true;"');
  hFile.Write('title="Move Backward"');
  hFile.WriteLine('><IMG src="gifs/left.gif" border="0"></A>&nbsp;&nbsp;');
  hFile.Write('<A href="javascript:playMoves();"');
  hFile.Write('onMouseOver="self.status=\'Continuous Play\'; return true;"');
  hFile.Write('onMouseOut="self.status=\'\'; return true;"');
  hFile.Write('title="Continuous Play"');
  hFile.WriteLine('><IMG src="gifs/clock.gif" border="0"></A>&nbsp;&nbsp;');
  hFile.Write('<A href="javascript:moveForward();"');
  hFile.Write('onMouseOver="self.status=\'Move Forward\'; return true;"');
  hFile.Write('onMouseOut="self.status=\'\'; return true;"');
  hFile.Write('title="Move Forward"');
  hFile.WriteLine('><IMG src="gifs/right.gif" border="0"></A>&nbsp;&nbsp;');
  hFile.Write('<A href="javascript:moveToEnd();"');
  hFile.Write('onMouseOver="self.status=\'Move To End\'; return true;"');
  hFile.Write('onMouseOut="self.status=\'\'; return true;"');
  hFile.Write('title="Move To End"');
  hFile.WriteLine('><IMG src="gifs/rright.gif" border="0"></A>');
  hFile.WriteLine('</DIV>');

  // [4-2] Draw outer table  
  hFile.WriteLine('</TD><TD vAlign="top">');
  hFile.WriteLine('<DIV class="comments">');
  //hFile.WriteLine('comments here');
  hFile.WriteLine(commentsHolder);
  hFile.WriteLine('</DIV>');
  hFile.Write('</TD>');
  hFile.Write('</TR>');
  hFile.Write('</TBODY>');
  hFile.WriteLine('</TABLE>');

  // [7] Start sripting
  hFile.WriteLine('<SCRIPT type="text/javascript" language="JavaScript">');
  hFile.WriteLine('milliSec = 600;');
  hFile.WriteLine('var initDelay = 1500;');
  hFile.WriteLine('addMoves();');
  hFile.WriteLine('window.setTimeout ("playMoves();",initDelay);');
  hFile.WriteLine('</SCR' + 'IPT>');

  // [8] Write the rest of the document here
  hFile.WriteLine('<DIV class="mainbody">');
  hFile.WriteLine('<!-- Main part here -->');
  hFile.WriteLine(' ');
  hFile.WriteLine('</DIV>');

  // [9] Copyright statement
  hFile.WriteLine('<HR>');
  hFile.WriteLine('<SCRIPT type="text/javascript" language="JavaScript"');
  hFile.WriteLine('src="copyright.js">');
  hFile.WriteLine('</SCR' + 'IPT>');
}


// Main part
//boardColor = "gold";
//backgroundColor = "ivory";
commentsHolder = "<!-- Comments Here --> ";
fileTitle = "";
filePath = "testmsgf.html";
fileHeader = "";

mSGF = "(" + 
";B[pd];W[dp];B[pq];W[dd];B[fq];W[cn];B[fc];W[df];B[qk];W[ec]" +
";B[kd];W[id];B[jp];W[oo];B[np];W[ok];B[qm];W[pl];B[ql];W[no]" +
";B[mp];W[qp];B[qq];W[oi];B[nl];W[ol];B[nd];W[ci];B[ce];W[cd]" +
";B[fd];W[hc];B[eb];W[db];B[he];W[ie];B[if];W[jf];B[de];W[ee]" +
";B[ed];W[be];B[dc];E[ec];W[ke];B[cc];W[cf];E[ce][de];B[ld];W[hf];B[fo];W[dr]" +
";B[em];W[mo];B[lo];W[ln];B[ko];W[fr];B[gr];W[eq];B[fp];W[qc]" +
";B[qd];W[rd];B[re];W[rb];B[pb];W[pc];B[oc];W[qb];B[ob];W[qe]" +
";B[rf];W[pe];B[od];W[qg];B[qf];W[qi];B[qo];W[pf];B[sd];W[sc]" +
";B[rc];E[rd];W[bc];B[cb];W[rd];E[rc];B[pg];W[og];B[rc];E[rd];W[pp];B[rp];W[rd];E[rc]" +
";B[ph];W[of];B[rc];E[rd];W[gb];B[bb];W[rd];E[rc];B[qh];W[se];E[sd];B[rh];W[oh]" +
";B[km];W[qj];B[rj];W[ro];B[rn];W[po];B[so];E[ro];W[kb];B[lb];W[kc]" +
";B[lc];W[hr];B[gs];W[me];B[jc];W[gm];B[do];W[co];B[fs];W[er]" +
";B[hn];W[en];B[fm];W[gq];B[hq];W[gp];B[ir];W[hp];B[hs];E[hr];W[fn]" +
";B[gn];W[eo];B[gl];W[go];B[ip];W[ep];E[fq][fo][fp];B[lg];W[le];B[lj];W[jb]" +
";B[jd];W[ig];E[if];B[nj];W[oj];B[nm];W[ic];B[ka];W[ib];B[nb];W[ri]" +
";B[si];W[rk];B[sj];W[ab];B[fb];W[nk];B[mk];W[jj];B[jk];W[om]" +
";B[ij];W[dm];B[ek];W[dj];B[mh];W[kh];B[pn];W[on];B[lh];W[ni]" +
";B[mj];W[lf];B[fi];W[ii];B[hj];W[ei];B[fh];W[ej];B[fj];W[dk]" +
";B[el];W[ho];B[io];W[kj];B[kk];W[lm];B[ll];W[in];B[hm];E[gm];W[kn]" +
";B[jn];W[mn];B[kg];W[ki];B[dl];W[cl];B[es];W[ds];B[hi];W[jm]" +
";B[im];E[in];W[kl];E[km];B[jl];W[km];B[ih];W[hh];B[ji];E[ii];W[jh];B[ii];W[jg]" +
";B[mg];W[ne];B[eg];W[dg];B[je];W[kf];B[ge];W[gf];B[oe];W[nf]" +
";B[fe];W[gh];B[gg];W[ef];B[ad];W[ac];B[sf];W[sh];B[sg];E[sh];W[sk]" +
";B[sh];W[sd];B[rl];W[mc];B[md];W[pa];B[oa];W[ma];B[la];W[mb]" +
";B[qa];E[pa];W[ra];B[ff];W[pi];B[sl];E[rk][sk];W[pk];B[eh];W[dh];B[ga];W[ha]" +
";B[fa];W[gc];B[ja];W[ia];B[gi];W[hg];B[op];W[pm];B[qn];W[fg];E[gg]" +
";B[cm];W[bm];E[cm];B[gg];E[fg];W[mi];B[li];W[fg];E[gg];B[sb];W[sa];E[sb];B[gg];E[fg];W[nn]" +
";B[ml];W[fg];E[gg];B[pa];W[gg];B[ba];W[bd];B[lk]" +
")";


CreateHtmlFile();
displayGoBoard();
CloseHtmlFile();
