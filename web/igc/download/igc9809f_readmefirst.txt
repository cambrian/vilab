Igc98 is a Windows client program for internet go servers.
The current version is 0.99f.

Under the directory, Go/igs_clients/PC,
you can find the following four Igc98 files.

igc9809f.zip:              International edition
igc9809f_korean.zip:       Korean edition
igc9809f_readmefirst.txt:  This file
igc9809f_releasenote.htm:  Release note


[Note]

igc9809f.zip supports *both* English and Korean.
If your Windows is Korean, you can install the Korean
edition which has some language-specific DLL files.
Otherwise, do *not* install the Korean edition!
(If you are not sure, just install the international edition.)

Please refer to the release note for the system requirements
and the installation instructions.

