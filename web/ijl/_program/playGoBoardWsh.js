/************************************************************
 *  playGoBoardWsh.js
 *  Copyright 1999, Hyoungsoo Yoon. All Rights Reserved.
 *  This software is distributed on an "AS IS" basis, 
 *  WITHOUT WARRANTY OF ANY KIND, either express or implied.
 ************************************************************/

// Global variables
var mSGF = "";
var lPos = 0;
var tPos = 0;
var boardColor = "gold";

var fso, hFile;
var filePath = "";
var fileTitle = "";
function CreateHtmlFile()
{
  fso = new ActiveXObject("Scripting.FileSystemObject");
  hFile = fso.CreateTextFile(filePath, true);
  hFile.WriteLine('<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0//EN">');
  hFile.WriteLine('<HTML lang="en">') ;
  hFile.WriteLine('<HEAD>') ;
  hFile.WriteLine('<META http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">');
  hFile.WriteLine('<TITLE>') ;
  hFile.WriteLine(fileTitle) ;
  hFile.WriteLine('</TITLE>') ;

  // Write style tags here.
  hFile.WriteLine('<STYLE TYPE="text/css">');
  hFile.Write('TABLE.goboard { ');
  hFile.Write('width: 0.01in; ');
  hFile.Write('margin: 0px; ');
  hFile.Write('padding: 0.0in; ');
  hFile.Write('border: 0.0in ;');
  hFile.Write('border-collapse: collapse; ');
  hFile.WriteLine('}');
  hFile.Write('TABLE.goboard TR { ');
  hFile.Write('margin: 0in; ');
  hFile.Write('padding: 0in; ');
  hFile.Write('border: 0in; ');
  hFile.WriteLine('}');
  hFile.Write('TABLE.goboard TD { ');
  hFile.Write('margin: 0in; ');
  hFile.Write('padding: 0in; ');
  hFile.WriteLine('}');
  hFile.Write('#boardlayer { ');
  hFile.Write('position: absolute; ');
  hFile.Write('left: ' + lPos +'; top: ' + tPos +'; ');
  hFile.Write('visibility: hidden; ');
  hFile.WriteLine('}');
  hFile.Write('#movelayer { ');
  hFile.Write('position: absolute; ');
  hFile.Write('left: ' + lPos +'; top: ' + tPos +'; ');
  hFile.Write('visibility: hidden; ');
  hFile.WriteLine('}');
  hFile.Write('SPAN.moves { ');
  hFile.Write('position: relative; ');
  hFile.Write('visibility: hidden; ');
  hFile.Write('left: 0; top: 0; ');
  hFile.WriteLine('}');
  hFile.Write('#arrows { ');
  hFile.Write('position: absolute; ');
  hFile.Write('left: ' + lPos +'; top: ' + tPos +'; ');
  hFile.Write('visibility: hidden; ');
  hFile.Write('text-align: center; ');
  hFile.WriteLine('}');
  hFile.WriteLine('</STYLE>');

  // Browser sniffer
  hFile.WriteLine('<SCRIPT type="text/javascript" language="JavaScript">');
  hFile.WriteLine('  var isNav4, isIE4;');
  hFile.WriteLine('  if (parseInt(navigator.appVersion.charAt(0)) >= 4) {');
  hFile.WriteLine('     if (navigator.appName == "Netscape") {');
  hFile.WriteLine('	isNav4 = true;');
  hFile.WriteLine('     } else if (navigator.appVersion.indexOf("MSIE") != -1) {');
  hFile.WriteLine('	isIE4 = true;');
  hFile.WriteLine('     }');
  hFile.WriteLine('  }');
  hFile.WriteLine('</SCRIPT>');

  // Go board
  hFile.WriteLine('<SCRIPT type="text/javascript" language="JavaScript"');
  hFile.WriteLine('src="goBoard.js">');
  hFile.WriteLine('</SCRIPT>');

  // Move functions
  hFile.WriteLine('<SCRIPT type="text/javascript" language="JavaScript"');
  hFile.WriteLine('src="playMoves.js">');
  hFile.WriteLine('</SCRIPT>');

  hFile.WriteLine('</HEAD>') ;
  hFile.WriteLine('<BODY>') ;
}
function CloseHtmlFile()
{
  hFile.WriteLine('</BODY>');
  hFile.WriteLine('</HTML>');
  hFile.Close();
}

// Default (empty) board
var board = new Array();
for(k=0;k<19;k++) {
  board[k] = new Array("2", "5", "5", "5", "5", "5", "5", "5", "5",
             "5", "5", "5", "5", "5", "5", "5", "5", "5", "8");
}
board[0] = ["1", "4", "4", "4", "4", "4", "4", "4", "4",
            "4", "4", "4", "4", "4", "4", "4", "4", "4", "7"];
board[18] = ["3", "6", "6", "6", "6", "6", "6", "6", "6",
            "6", "6", "6", "6", "6", "6", "6", "6", "6", "9"];
board[3][3] = "H"; board[3][9] = "H"; board[3][15] = "H";
board[9][3] = "H"; board[9][9] = "H"; board[9][15] = "H";
board[15][3] = "H"; board[15][9] = "H"; board[15][15] = "H";

// Move sequence
var move = new Array();

function  playGoBoard() {

  // [1] "Parse" Minimal-SGF String
  var sgf = mSGF.split(";");

  var a = "a";
  var xstart = 0;
  var ystart = 0;
  var xend =  18;
  var yend =  18;

  var mStart = 2;
  if(sgf[1].charAt(0) == "V") { 
    xstart = sgf[1].charCodeAt(3) - a.charCodeAt(0);
    ystart = sgf[1].charCodeAt(4) - a.charCodeAt(0);
    xend =  sgf[1].charCodeAt(6) - a.charCodeAt(0);
    yend =  sgf[1].charCodeAt(7) - a.charCodeAt(0);
  } else {
    mStart = 1;
  }
  for(m=mStart;m<sgf.length;m++) {
    var p = sgf[m].charCodeAt(2) - a.charCodeAt(0);
    var q = sgf[m].charCodeAt(3) - a.charCodeAt(0);
    var mm = m - mStart;
    move[mm] = new Object();
    move[mm].color = sgf[m].charAt(0);
    move[mm].x = p;
    move[mm].y = q;
  }

  // [2] Write move sequence
  hFile.WriteLine('<SCRIPT type="text/javascript" language="JavaScript">');
  hFile.WriteLine('var move = new Array();');
  for(n=0;n<move.length;n++) {
    hFile.WriteLine(' move['+n+'] = new Object();');
    hFile.Write(' move['+n+'].color = "' + move[n].color + '";');
    hFile.Write(' move['+n+'].x = ' + move[n].x + ';');
    hFile.WriteLine(' move['+n+'].y = ' + move[n].y + ';');
  }
  hFile.WriteLine('</SCRIPT>');

  // [3] Write heading
  hFile.WriteLine('<H2>');
  hFile.WriteLine(fileTitle);
  hFile.WriteLine('</H2>');

  // [4] "Draw" Go Board
  var boardWidth = (xend-xstart+1)*15;
  var boardHeight = (yend-ystart+1)*15;
  
  hFile.Write('<DIV id="boardlayer" ');
  hFile.Write('style="height: ' + boardHeight + '; ');
  hFile.WriteLine('width: ' + boardWidth + ';">');
  hFile.Write('<TABLE class="goboard" ');
  hFile.Write('style="');
  hFile.Write('background: '+ boardColor + ';">');
  hFile.WriteLine('<TBODY>');
  hFile.Write('<TR>');
  hFile.WriteLine('<TD noWrap>');
  for(j=ystart;j<=yend;j++) {
    for(i=xstart;i<=xend;i++) {
      hFile.Write('<IMG src="gifs/' + board[i][j] + '.gif">');
    }
    hFile.WriteLine('<BR>');
  }
  hFile.Write('</TD>');
  hFile.WriteLine('</TR>');
  hFile.Write('</TBODY>');
  hFile.WriteLine('</TABLE>');
  hFile.WriteLine('</DIV>');

  // [5] Draw moves (hidden)
  hFile.Write('<DIV id="movelayer" ');
  hFile.Write('style="height: ' + boardHeight + '; ');
  hFile.WriteLine('width: ' + boardWidth + ';">');
  hFile.Write('<TABLE class="goboard">');
  hFile.WriteLine('<TBODY>');
  hFile.Write('<TR>');
  hFile.WriteLine('<TD noWrap>');
  for(j=ystart;j<=yend;j++) {
    for(i=xstart;i<=xend;i++) {
      var b = false;
      for(n=0;n<move.length;n++) {
        if(move[n].x == i && move[n].y == j) {
          hFile.Write('<SPAN class="moves" id="Move' + n + '">');
          hFile.Write('<IMG src="gifs/' + move[n].color + '.gif">');
          hFile.Write('</SPAN>');
          b = true;
          break;
        }
      }
      if(b == false) {
        hFile.Write('<SPAN ');
        hFile.Write('style="visibility:hidden;">');
        hFile.Write('<IMG src="gifs/' + board[i][j] + '.gif">');
        hFile.Write('</SPAN>');
      }
    }
    hFile.WriteLine('<BR>');
  }
  hFile.Write('</TD>');
  hFile.WriteLine('</TR>');
  hFile.Write('</TBODY>');
  hFile.WriteLine('</TABLE>');
  hFile.WriteLine('</DIV>');

  // [6] Play moves
  hFile.Write('<DIV id="arrows" ');
  hFile.WriteLine('style="width: ' + boardWidth + ';">');
  hFile.WriteLine('<A href="javascript:moveBackward()"><IMG src="gifs/left.gif" border="0"></A>&nbsp;&nbsp;');
  hFile.WriteLine('<A href="javascript:playMoves()"><IMG src="gifs/clock.gif" border="0"></A>&nbsp;&nbsp;');
  hFile.WriteLine('<A href="javascript:moveForward()"><IMG src="gifs/right.gif" border="0"></A>');
  hFile.WriteLine('</DIV>');
  hFile.WriteLine('<SCRIPT type="text/javascript" language="JavaScript">');
  hFile.WriteLine('currentNumber = 0;');
  hFile.WriteLine('if(isNav4) {');
  hFile.WriteLine('  document.layers.boardlayer.visibility = "show";');
  hFile.WriteLine('  document.layers.movelayer.visibility = "show";');
  hFile.WriteLine('  document.layers.arrows.visibility = "show";');
  hFile.WriteLine('  document.layers.arrows.top += ' + boardHeight + ';');
  hFile.WriteLine('} else if(isIE4) {');
  hFile.WriteLine('  document.all.boardlayer.style.visibility = "visible";');
  hFile.WriteLine('  document.all.movelayer.style.visibility = "visible";');
  hFile.WriteLine('  document.all.arrows.style.visibility = "visible";');
  hFile.WriteLine('  document.all.arrows.style.pixelTop += ' + (boardHeight+10) + ';');
  hFile.WriteLine('}');
  hFile.WriteLine('</SCRIPT>');

  // [7] Room for comments
  hFile.Write('<DIV class="comment" ');
  hFile.WriteLine('style="margin-left: ' + (boardWidth+10) + ';"');
  hFile.WriteLine('blah blah<BR>');
  hFile.WriteLine('blah blah<BR>');
  hFile.WriteLine('blah blah<BR>');
  hFile.WriteLine('</DIV>');
}


// Main part
//filePath = "E:\\JScript\\goboard\\test.htm";
filePath = "test2.htm";
fileTitle = "Testing...";
mSGF = "(;VW[ga:sm];B[qd];W[oc];B[ld];W[of];B[qg];W[id];B[mf])";
boardColor = "gold";
milliSec = 1000;
lPos = 10;
tPos = 50; // ?????

CreateHtmlFile();
playGoBoard();
CloseHtmlFile();

