/************************************************************
 *  boardImages.js
 *  Copyright 1999, Hyoungsoo Yoon. All Rights Reserved.
 *  This software is distributed on an "AS IS" basis, 
 *  WITHOUT WARRANTY OF ANY KIND, either express or implied.
 ************************************************************/

// Board and stone images
var brd = new Array();
for(i=0;i<19;i++) {
  brd[i] = new Array();
  for(j=0;j<19;j++) {
    brd[i][j] = new Image();
    brd[i][j].src = 'gifs/' + board[i][j] + '.gif';
  }
}
var B = new Image();
B.src = 'gifs/B.gif';
var W = new Image();
W.src = 'gifs/W.gif';

