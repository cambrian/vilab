# LaTeX2HTML 98.1p1 release (March 2nd, 1998)
# Associate images original text with physical files.


$key = q/{indisplay}displaystylesigma^{2}_{}{indisplay}MSF=1.4;AAT;/;
$cached_env_img{$key} = q|<IMG
 WIDTH="17" HEIGHT="31" ALIGN="MIDDLE" BORDER="0"
 SRC="img53.gif"
 ALT="$\displaystyle \sigma^{2}_{}$">|; 

$key = q/{inline}rangle{inline}MSF=1.4;AAT;/;
$cached_env_img{$key} = q|<IMG
 WIDTH="8" HEIGHT="28" ALIGN="MIDDLE" BORDER="0"
 SRC="img28.gif"
 ALT="$ \rangle$">|; 

$key = q/{indisplay}displaystyle{frac{|g_n|^2}{a-b+bslash(n^2varrho)}}{indisplay}MSF=1.4;AAT;/;
$cached_env_img{$key} = q|<IMG
 WIDTH="99" HEIGHT="49" ALIGN="MIDDLE" BORDER="0"
 SRC="img63.gif"
 ALT="$\displaystyle {\frac{\vert g_n\vert^2}{a-b + b/(n^2 \varrho)}}$">|; 

$key = q/{indisplay}displaystylesum_{mathrm{odd}}^{}{indisplay}MSF=1.4;AAT;/;
$cached_env_img{$key} = q|<IMG
 WIDTH="23" HEIGHT="45" ALIGN="MIDDLE" BORDER="0"
 SRC="img62.gif"
 ALT="$\displaystyle \sum_{\mathrm{odd}}^{}$">|; 

$key = q/{inline}{frac{2pi}{N}}{inline}MSF=1.4;AAT;/;
$cached_env_img{$key} = q|<IMG
 WIDTH="18" HEIGHT="30" ALIGN="MIDDLE" BORDER="0"
 SRC="img40.gif"
 ALT="$ {\frac{2\pi}{N}}$">|; 

$key = q/{indisplay}displaystyleleft(vphantom{(cos(theta)-1)slashsigma^2}right.{indisplay}MSF=1.4;AAT;/;
$cached_env_img{$key} = q|<IMG
 WIDTH="11" HEIGHT="31" ALIGN="MIDDLE" BORDER="0"
 SRC="img52.gif"
 ALT="$\displaystyle \left(\vphantom{(\cos(\theta)-1)/\sigma^2}\right.$">|; 

$key = q/{inline}mathcal{O}{inline}MSF=1.4;AAT;/;
$cached_env_img{$key} = q|<IMG
 WIDTH="14" HEIGHT="14" ALIGN="BOTTOM" BORDER="0"
 SRC="img48.gif"
 ALT="$ \mathcal {O}$">|; 

$key = q/{inline}lambda{inline}MSF=1.4;AAT;/;
$cached_env_img{$key} = q|<IMG
 WIDTH="11" HEIGHT="13" ALIGN="BOTTOM" BORDER="0"
 SRC="img41.gif"
 ALT="$ \lambda$">|; 

$key = q/{inline}neq{inline}MSF=1.4;AAT;/;
$cached_env_img{$key} = q|<IMG
 WIDTH="14" HEIGHT="26" ALIGN="MIDDLE" BORDER="0"
 SRC="img36.gif"
 ALT="$ \neq$">|; 

$key = q/{indisplay}displaystylesum_{i,j}^{}{indisplay}MSF=1.4;AAT;/;
$cached_env_img{$key} = q|<IMG
 WIDTH="23" HEIGHT="46" ALIGN="MIDDLE" BORDER="0"
 SRC="img14.gif"
 ALT="$\displaystyle \sum_{i,j}^{}$">|; 

$key = q/{indisplay}displaystyle{Novera}{indisplay}MSF=1.4;AAT;/;
$cached_env_img{$key} = q|<IMG
 WIDTH="19" HEIGHT="45" ALIGN="MIDDLE" BORDER="0"
 SRC="img30.gif"
 ALT="$\displaystyle {N \over a}$">|; 

$key = q/{inline}rightarrow{inline}MSF=1.4;AAT;/;
$cached_env_img{$key} = q|<IMG
 WIDTH="17" HEIGHT="13" ALIGN="BOTTOM" BORDER="0"
 SRC="img68.gif"
 ALT="$ \rightarrow$">|; 

$key = q/{indisplay}displaystyleleft(vphantom{-frac{1}{2}sum_{i,j}(r_i-f_i(theta))C^{-1}_{ij}(r_j-f_j(theta))}right.{indisplay}MSF=1.4;AAT;/;
$cached_env_img{$key} = q|<IMG
 WIDTH="17" HEIGHT="64" ALIGN="MIDDLE" BORDER="0"
 SRC="img12.gif"
 ALT="$\displaystyle \left(\vphantom{-\frac{1}{2} \sum_{i,j}
(r_i - f_i(\theta)) C^{-1}_{ij} (r_j - f_j(\theta)) }\right.$">|; 

$key = q/{inline}{group1groupoverN}{inline}MSF=1.4;AAT;/;
$cached_env_img{$key} = q|<IMG
 WIDTH="16" HEIGHT="30" ALIGN="MIDDLE" BORDER="0"
 SRC="img5.gif"
 ALT="$ {\begingroup 1\endgroup \over N}$">|; 

$key = q/{indisplay}displaystylesum_{j}^{}{indisplay}MSF=1.4;AAT;/;
$cached_env_img{$key} = q|<IMG
 WIDTH="23" HEIGHT="46" ALIGN="MIDDLE" BORDER="0"
 SRC="img33.gif"
 ALT="$\displaystyle \sum_{j}^{}$">|; 

$key = q/{inline}sqrt{N}{inline}MSF=1.4;AAT;/;
$cached_env_img{$key} = q|<IMG
 WIDTH="27" HEIGHT="33" ALIGN="MIDDLE" BORDER="0"
 SRC="img6.gif"
 ALT="$ \sqrt{N}$">|; 

$key = q/{inline}le{inline}MSF=1.4;AAT;/;
$cached_env_img{$key} = q|<IMG
 WIDTH="14" HEIGHT="25" ALIGN="MIDDLE" BORDER="0"
 SRC="img9.gif"
 ALT="$ \le$">|; 

$key = q/{indisplay}displaystylesum_{n}^{}{indisplay}MSF=1.4;AAT;/;
$cached_env_img{$key} = q|<IMG
 WIDTH="23" HEIGHT="45" ALIGN="MIDDLE" BORDER="0"
 SRC="img31.gif"
 ALT="$\displaystyle \sum_{n}^{}$">|; 

$key = q/{indisplay}displaystylesum_{mathrm{even}}^{}{indisplay}MSF=1.4;AAT;/;
$cached_env_img{$key} = q|<IMG
 WIDTH="24" HEIGHT="45" ALIGN="MIDDLE" BORDER="0"
 SRC="img61.gif"
 ALT="$\displaystyle \sum_{\mathrm{even}}^{}$">|; 

$key = q/{inline}ll{inline}MSF=1.4;AAT;/;
$cached_env_img{$key} = q|<IMG
 WIDTH="17" HEIGHT="25" ALIGN="MIDDLE" BORDER="0"
 SRC="img65.gif"
 ALT="$ \ll$">|; 

$key = q/{inline}{group2groupoverpi}{inline}MSF=1.4;AAT;/;
$cached_env_img{$key} = q|<IMG
 WIDTH="13" HEIGHT="30" ALIGN="MIDDLE" BORDER="0"
 SRC="img7.gif"
 ALT="$ {\begingroup 2\endgroup \over \pi}$">|; 

$key = q/{inline}pi{inline}MSF=1.4;AAT;/;
$cached_env_img{$key} = q|<IMG
 WIDTH="11" HEIGHT="13" ALIGN="BOTTOM" BORDER="0"
 SRC="img3.gif"
 ALT="$ \pi$">|; 

$key = q/{indisplay}displaystyletheta{indisplay}MSF=1.4;AAT;/;
$cached_env_img{$key} = q|<IMG
 WIDTH="10" HEIGHT="13" ALIGN="BOTTOM" BORDER="0"
 SRC="img10.gif"
 ALT="$\displaystyle \theta$">|; 

$key = q/{indisplay}displaystyle{bovera}{indisplay}MSF=1.4;AAT;/;
$cached_env_img{$key} = q|<IMG
 WIDTH="13" HEIGHT="45" ALIGN="MIDDLE" BORDER="0"
 SRC="img46.gif"
 ALT="$\displaystyle {b\over a}$">|; 

$key = q/{figure}centerleavevmoderesizebox9.7cm!<comment_mark>epsfigfile=figure_b.epscenter{figure}FSF=1.4;AAT;/;
$cached_env_img{$key} = q|<IMG
 WIDTH="377" HEIGHT="253"
 SRC="img55.gif"
 ALT="\begin{figure}
\begin{center}
\leavevmode
\resizebox{9.7cm}{!}%
{\epsfig{file=figure_b.eps} }
\end{center} \end{figure}">|; 

$key = q/{figure}centerleavevmoderesizebox9.7cm!<comment_mark>epsfigfile=figure_d.epscenter{figure}FSF=1.4;AAT;/;
$cached_env_img{$key} = q|<IMG
 WIDTH="377" HEIGHT="243"
 SRC="img67.gif"
 ALT="\begin{figure}
\begin{center}
\leavevmode
\resizebox{9.7cm}{!}%
{\epsfig{file=figure_d.eps} }
\end{center} \end{figure}">|; 

$key = q/{indisplay}displaystyle{textstylefrac{1}{2}}{indisplay}MSF=1.4;AAT;/;
$cached_env_img{$key} = q|<IMG
 WIDTH="12" HEIGHT="30" ALIGN="MIDDLE" BORDER="0"
 SRC="img13.gif"
 ALT="$\displaystyle {\textstyle\frac{1}{2}}$">|; 

$key = q/{inline}{frac{1}{N}}{inline}MSF=1.4;AAT;/;
$cached_env_img{$key} = q|<IMG
 WIDTH="16" HEIGHT="30" ALIGN="MIDDLE" BORDER="0"
 SRC="img56.gif"
 ALT="$ {\frac{1}{N}}$">|; 

$key = q/{indisplay}scriptstylephi_{i}{indisplay}MSF=1.4;AAT;/;
$cached_env_img{$key} = q|<IMG
 WIDTH="14" HEIGHT="25" ALIGN="MIDDLE" BORDER="0"
 SRC="img38.gif"
 ALT="$\scriptstyle \phi_{i}$">|; 

$key = q/{indisplay}displaystylemathcal{N}{indisplay}MSF=1.4;AAT;/;
$cached_env_img{$key} = q|<IMG
 WIDTH="16" HEIGHT="14" ALIGN="BOTTOM" BORDER="0"
 SRC="img11.gif"
 ALT="$\displaystyle \mathcal {N}$">|; 

$key = q/{indisplay}scriptstylephi_{j}{indisplay}MSF=1.4;AAT;/;
$cached_env_img{$key} = q|<IMG
 WIDTH="15" HEIGHT="25" ALIGN="MIDDLE" BORDER="0"
 SRC="img34.gif"
 ALT="$\scriptstyle \phi_{j}$">|; 

$key = q/{indisplay}displaystyle{frac{partialf_i(theta)}{partialtheta}}{indisplay}MSF=1.4;AAT;/;
$cached_env_img{$key} = q|<IMG
 WIDTH="44" HEIGHT="47" ALIGN="MIDDLE" BORDER="0"
 SRC="img25.gif"
 ALT="$\displaystyle {\frac{\partial f_i(\theta)}{\partial \theta}}$">|; 

$key = q/{indisplay}displaystyleleft.vphantom{-frac{1}{2}sum_{i,j}(r_i-f_i(theta))C^{-1}_{ij}(r_j-f_j(theta))}right){indisplay}MSF=1.4;AAT;/;
$cached_env_img{$key} = q|<IMG
 WIDTH="17" HEIGHT="64" ALIGN="MIDDLE" BORDER="0"
 SRC="img15.gif"
 ALT="$\displaystyle \left.\vphantom{-\frac{1}{2} \sum_{i,j}
(r_i - f_i(\theta)) C^{-1}_{ij} (r_j - f_j(\theta)) }\right)$">|; 

$key = q/{indisplay}displaystyle{frac{rho^{-2}+n^2}{1-(-1)^ne^{-pislashrho}}}{indisplay}MSF=1.4;AAT;/;
$cached_env_img{$key} = q|<IMG
 WIDTH="101" HEIGHT="49" ALIGN="MIDDLE" BORDER="0"
 SRC="img51.gif"
 ALT="$\displaystyle {\frac{\rho^{-2} + n^2}{1 - (-1)^n e^{-\pi/\rho}}}$">|; 

$key = q/{indisplay}displaystyleequiv{indisplay}MSF=1.4;AAT;/;
$cached_env_img{$key} = q|<IMG
 WIDTH="14" HEIGHT="13" ALIGN="BOTTOM" BORDER="0"
 SRC="img24.gif"
 ALT="$\displaystyle \equiv$">|; 

$key = q/{inline}sigma{inline}MSF=1.4;AAT;/;
$cached_env_img{$key} = q|<IMG
 WIDTH="11" HEIGHT="13" ALIGN="BOTTOM" BORDER="0"
 SRC="img4.gif"
 ALT="$ \sigma$">|; 

$key = q/{inline}langle{inline}MSF=1.4;AAT;/;
$cached_env_img{$key} = q|<IMG
 WIDTH="8" HEIGHT="28" ALIGN="MIDDLE" BORDER="0"
 SRC="img26.gif"
 ALT="$ \langle$">|; 

$key = q/{indisplay}displaystyle{frac{rho^{-2}+n^2}{rho^{-2}+n^2+(frac{2b}{aomegarho})(1-(-1)^ne^{-pislashrho})}}{indisplay}MSF=1.4;AAT;/;
$cached_env_img{$key} = q|<IMG
 WIDTH="216" HEIGHT="49" ALIGN="MIDDLE" BORDER="0"
 SRC="img47.gif"
 ALT="$\displaystyle {\frac{\rho^{-2} + n^2}{\rho^{-2} + n^2+
(\frac{2b}{a\omega\rho})
(1 - (-1)^n e^{-\pi/\rho})}}$">|; 

$key = q/{indisplay}displaystyle{Novera-b}{indisplay}MSF=1.4;AAT;/;
$cached_env_img{$key} = q|<IMG
 WIDTH="36" HEIGHT="45" ALIGN="MIDDLE" BORDER="0"
 SRC="img60.gif"
 ALT="$\displaystyle {N\over a-b}$">|; 

$key = q/{inline}theta{inline}MSF=1.4;AAT;/;
$cached_env_img{$key} = q|<IMG
 WIDTH="10" HEIGHT="13" ALIGN="BOTTOM" BORDER="0"
 SRC="img8.gif"
 ALT="$ \theta$">|; 

$key = q/{indisplay}displaystyle{frac{1-lambda}{lambda(1-lambda^{frac{N-1}{2}})}}{indisplay}MSF=1.4;AAT;/;
$cached_env_img{$key} = q|<IMG
 WIDTH="82" HEIGHT="45" ALIGN="MIDDLE" BORDER="0"
 SRC="img45.gif"
 ALT="$\displaystyle {\frac{1 - \lambda}{\lambda (1 - \lambda^{\frac{N-1}{2}})}}$">|; 

$key = q/{indisplay}displaystyle{pirhooverb}{indisplay}MSF=1.4;AAT;/;
$cached_env_img{$key} = q|<IMG
 WIDTH="22" HEIGHT="38" ALIGN="MIDDLE" BORDER="0"
 SRC="img50.gif"
 ALT="$\displaystyle {\pi \rho\over b}$">|; 

$key = q/{indisplay}displaystyle{frac{N(lambda^{-1}-1)}{a(lambda^{-1}-1)+2b}}{indisplay}MSF=1.4;AAT;/;
$cached_env_img{$key} = q|<IMG
 WIDTH="102" HEIGHT="49" ALIGN="MIDDLE" BORDER="0"
 SRC="img70.gif"
 ALT="$\displaystyle {\frac{N (\lambda^{-1}-1)}{a(\lambda^{-1}-1) + 2b}}$">|; 

$key = q/{inline}{frac{N-1}{2}}{inline}MSF=1.4;AAT;/;
$cached_env_img{$key} = q|<IMG
 WIDTH="30" HEIGHT="31" ALIGN="MIDDLE" BORDER="0"
 SRC="img35.gif"
 ALT="$ {\frac{N-1}{2}}$">|; 

$key = q/{indisplay}displaystyle{frac{1}{N}}{indisplay}MSF=1.4;AAT;/;
$cached_env_img{$key} = q|<IMG
 WIDTH="19" HEIGHT="44" ALIGN="MIDDLE" BORDER="0"
 SRC="img32.gif"
 ALT="$\displaystyle {\frac{1}{N}}$">|; 

$key = q/{indisplay}displaystyle{frac{|phi_i-phi_j|}{rho}}{indisplay}MSF=1.4;AAT;/;
$cached_env_img{$key} = q|<IMG
 WIDTH="64" HEIGHT="47" ALIGN="MIDDLE" BORDER="0"
 SRC="img23.gif"
 ALT="$\displaystyle {\frac{\Vert\phi_i - \phi_j\Vert}{\rho}}$">|; 

$key = q/{inline}sim{inline}MSF=1.4;AAT;/;
$cached_env_img{$key} = q|<IMG
 WIDTH="14" HEIGHT="13" ALIGN="BOTTOM" BORDER="0"
 SRC="img2.gif"
 ALT="$ \sim$">|; 

$key = q/{inline}hat{b}_{mathrm{min}}^{}{inline}MSF=1.4;AAT;/;
$cached_env_img{$key} = q|<IMG
 WIDTH="28" HEIGHT="34" ALIGN="MIDDLE" BORDER="0"
 SRC="img59.gif"
 ALT="$ \hat{b}_{\mathrm{min}}^{}$">|; 

$key = q/{inline}infty{inline}MSF=1.4;AAT;/;
$cached_env_img{$key} = q|<IMG
 WIDTH="17" HEIGHT="13" ALIGN="BOTTOM" BORDER="0"
 SRC="img69.gif"
 ALT="$ \infty$">|; 

$key = q/{inline}hat{theta}{inline}MSF=1.4;AAT;/;
$cached_env_img{$key} = q|<IMG
 WIDTH="10" HEIGHT="16" ALIGN="BOTTOM" BORDER="0"
 SRC="img27.gif"
 ALT="$ \hat{\theta}$">|; 

$key = q/{indisplay}displaystyle{frac{|g_n|^2}{C_{n}}}{indisplay}MSF=1.4;AAT;/;
$cached_env_img{$key} = q|<IMG
 WIDTH="34" HEIGHT="49" ALIGN="MIDDLE" BORDER="0"
 SRC="img37.gif"
 ALT="$\displaystyle {\frac{\vert g_n\vert^2}{C_{n}}}$">|; 

$key = q/{inline}scriptstyleomega{inline}MSF=1.4;AAT;/;
$cached_env_img{$key} = q|<IMG
 WIDTH="10" HEIGHT="13" ALIGN="BOTTOM" BORDER="0"
 SRC="img42.gif"
 ALT="$\scriptstyle \omega$">|; 

$key = q/{inline}scriptstylerho{inline}MSF=1.4;AAT;/;
$cached_env_img{$key} = q|<IMG
 WIDTH="9" HEIGHT="25" ALIGN="MIDDLE" BORDER="0"
 SRC="img43.gif"
 ALT="$\scriptstyle \rho$">|; 

$key = q/{inline}varrho{inline}MSF=1.4;AAT;/;
$cached_env_img{$key} = q|<IMG
 WIDTH="10" HEIGHT="25" ALIGN="MIDDLE" BORDER="0"
 SRC="img64.gif"
 ALT="$ \varrho$">|; 

$key = q/{inline}theta_{1}^{}{inline}MSF=1.4;AAT;/;
$cached_env_img{$key} = q|<IMG
 WIDTH="16" HEIGHT="26" ALIGN="MIDDLE" BORDER="0"
 SRC="img21.gif"
 ALT="$ \theta_{1}^{}$">|; 

$key = q/{inline}theta_{2}^{}{inline}MSF=1.4;AAT;/;
$cached_env_img{$key} = q|<IMG
 WIDTH="16" HEIGHT="26" ALIGN="MIDDLE" BORDER="0"
 SRC="img22.gif"
 ALT="$ \theta_{2}^{}$">|; 

$key = q/{inline}phi_{i}^{}{inline}MSF=1.4;AAT;/;
$cached_env_img{$key} = q|<IMG
 WIDTH="16" HEIGHT="26" ALIGN="MIDDLE" BORDER="0"
 SRC="img18.gif"
 ALT="$ \phi_{i}^{}$">|; 

$key = q/{inline}hat{b}{inline}MSF=1.4;AAT;/;
$cached_env_img{$key} = q|<IMG
 WIDTH="9" HEIGHT="16" ALIGN="BOTTOM" BORDER="0"
 SRC="img58.gif"
 ALT="$ \hat{b}$">|; 

$key = q/{inline}phi_{j}^{}{inline}MSF=1.4;AAT;/;
$cached_env_img{$key} = q|<IMG
 WIDTH="17" HEIGHT="26" ALIGN="MIDDLE" BORDER="0"
 SRC="img20.gif"
 ALT="$ \phi_{j}^{}$">|; 

$key = q/{indisplay}displaystyle{frac{1}{2N}}{indisplay}MSF=1.4;AAT;/;
$cached_env_img{$key} = q|<IMG
 WIDTH="26" HEIGHT="44" ALIGN="MIDDLE" BORDER="0"
 SRC="img44.gif"
 ALT="$\displaystyle {\frac{1}{2N}}$">|; 

$key = q/{figure}centerleavevmoderesizebox9.7cm!<comment_mark>epsfigfile=figure_a.epscenter{figure}FSF=1.4;AAT;/;
$cached_env_img{$key} = q|<IMG
 WIDTH="377" HEIGHT="243"
 SRC="img49.gif"
 ALT="\begin{figure}
\begin{center}
\leavevmode
\resizebox{9.7cm}{!}%
{\epsfig{file=figure_a.eps} }
\end{center} \end{figure}">|; 

$key = q/{indisplay}displaystyleleft.vphantom{(cos(theta)-1)slashsigma^2}right){indisplay}MSF=1.4;AAT;/;
$cached_env_img{$key} = q|<IMG
 WIDTH="11" HEIGHT="31" ALIGN="MIDDLE" BORDER="0"
 SRC="img54.gif"
 ALT="$\displaystyle \left.\vphantom{(\cos(\theta)-1)/\sigma^2}\right)$">|; 

$key = q/{figure}centerleavevmoderesizebox9.7cm!<comment_mark>epsfigfile=figure_c.epscenter{figure}FSF=1.4;AAT;/;
$cached_env_img{$key} = q|<IMG
 WIDTH="378" HEIGHT="251"
 SRC="img57.gif"
 ALT="\begin{figure}
\begin{center}
\leavevmode
\resizebox{9.7cm}{!}%
{\epsfig{file=figure_c.eps} }
\end{center} \end{figure}">|; 

$key = q/{inline}mathcal{N}{inline}MSF=1.4;AAT;/;
$cached_env_img{$key} = q|<IMG
 WIDTH="16" HEIGHT="14" ALIGN="BOTTOM" BORDER="0"
 SRC="img16.gif"
 ALT="$ \mathcal {N}$">|; 

$key = q/{indisplay}displaystyle{frac{1-lambdacos(nomega)-(-1)^nlambda^{frac{N+1}{2}}cos(nomega)(1-lambda)}{1-2lambdacos(nomega)+lambda^{2}}}{indisplay}MSF=1.4;AAT;/;
$cached_env_img{$key} = q|<IMG
 WIDTH="266" HEIGHT="54" ALIGN="MIDDLE" BORDER="0"
 SRC="img39.gif"
 ALT="$\displaystyle {\frac{1-\lambda \cos (n\omega)
- (-1)^n \lambda^{\frac{N+1}{2}} \cos (n\omega)
(1-\lambda)}{1 - 2\lambda \cos (n\omega)
+ \lambda^{2}}}$">|; 

$key = q/{inline}rho{inline}MSF=1.4;AAT;/;
$cached_env_img{$key} = q|<IMG
 WIDTH="10" HEIGHT="25" ALIGN="MIDDLE" BORDER="0"
 SRC="img1.gif"
 ALT="$ \rho$">|; 

$key = q/{inline}omega{inline}MSF=1.4;AAT;/;
$cached_env_img{$key} = q|<IMG
 WIDTH="12" HEIGHT="13" ALIGN="BOTTOM" BORDER="0"
 SRC="img19.gif"
 ALT="$ \omega$">|; 

$key = q/{inline}ge{inline}MSF=1.4;AAT;/;
$cached_env_img{$key} = q|<IMG
 WIDTH="14" HEIGHT="25" ALIGN="MIDDLE" BORDER="0"
 SRC="img29.gif"
 ALT="$ \ge$">|; 

$key = q/{indisplay}displaystyledelta_{ij}^{}{indisplay}MSF=1.4;AAT;/;
$cached_env_img{$key} = q|<IMG
 WIDTH="19" HEIGHT="26" ALIGN="MIDDLE" BORDER="0"
 SRC="img17.gif"
 ALT="$\displaystyle \delta_{ij}^{}$">|; 

$key = q/{inline}{frac{2}{pi}}{inline}MSF=1.4;AAT;/;
$cached_env_img{$key} = q|<IMG
 WIDTH="13" HEIGHT="30" ALIGN="MIDDLE" BORDER="0"
 SRC="img66.gif"
 ALT="$ {\frac{2}{\pi}}$">|; 

1;

