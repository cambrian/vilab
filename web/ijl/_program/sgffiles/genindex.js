/************************************************************
 *  Copyright 1999, Hyoungsoo Yoon. All Rights Reserved.
 *  This software is distributed on an "AS IS" basis, 
 *  WITHOUT WARRANTY OF ANY KIND, either express or implied.
 ************************************************************/

var backgroundColor = "ivory";
var commentsHolder = "";

var fso, hFile;
var fileTitle = "";
var fileHeader = "";
function CreateIndexFile(filePath)
{
  hFile = fso.CreateTextFile(filePath, true);
  hFile.WriteLine('<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0//EN">');
  hFile.WriteLine('<HTML lang="en">') ;
  hFile.WriteLine('<HEAD>') ;
  hFile.WriteLine('<META http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">');
  hFile.WriteLine('<TITLE>') ;
  hFile.WriteLine(fileTitle) ;
  hFile.WriteLine('</TITLE>') ;

  // Write style tags here.
  hFile.WriteLine('<STYLE TYPE="text/css">');
  hFile.Write('BODY { ');
  hFile.Write('background: ' + backgroundColor + '; ');
  hFile.WriteLine('}');
  hFile.WriteLine('</STYLE>');

  hFile.WriteLine('</HEAD>') ;
  hFile.WriteLine('<BODY>') ;
}

function CloseIndexFile()
{
  hFile.WriteLine('</BODY>');
  hFile.WriteLine('</HTML>');
  hFile.Close();
}

function WriteIndexLine(htmlName, sgfName) {
  hFile.WriteLine('<DT><A href="' + htmlName + '">');
  hFile.WriteLine(sgfName);
  hFile.WriteLine('</A></DT>');
  hFile.WriteLine('<DD>');
  hFile.WriteLine('</DD>');
}

function WriteCopyrightStatement() {
  hFile.WriteLine('<HR>');
  hFile.WriteLine('<SCRIPT type="text/javascript" language="JavaScript"');
  hFile.WriteLine('src="copyright.js">');
  hFile.WriteLine('</SCR' + 'IPT>');
}


// Main part
fileTitle = "IJL: Internet Joseki Library";
fileHeader = "index";
var folderPath = "chohunhyun";
var indexFilePath = "indextest.html";
fso = new ActiveXObject("Scripting.FileSystemObject");
CreateIndexFile(indexFilePath);

hFile.WriteLine('<H2>' + fileHeader + '</H2>');
hFile.WriteLine('<DIV class="mainbody">');
hFile.WriteLine('<!-- Main part here -->');
hFile.WriteLine('<DL>');

var f = fso.GetFolder(folderPath);
var fc = new Enumerator(f.files);
for (; !fc.atEnd(); fc.moveNext())
{
  var fileItem = fc.item();
  var htmlName = fileItem.name;
  var tmpName = htmlName.split('.');
  var fileRoot = tmpName[0];
  var fileExt = tmpName[1];
  if(fileExt == 'html') {
    var sgfName = fileRoot + '.sgf';
    WriteIndexLine(htmlName, sgfName);
  }
}

hFile.WriteLine('</DL>');
hFile.WriteLine('</DIV>');

WriteCopyrightStatement();
CloseIndexFile();


