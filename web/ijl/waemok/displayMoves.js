/************************************************************
 *  displayMoves.js
 *  Copyright 1999, Hyoungsoo Yoon. All Rights Reserved.
 *  This software is distributed on an "AS IS" basis, 
 *  WITHOUT WARRANTY OF ANY KIND, either express or implied.
 ************************************************************/

var milliSec = 1000;
var currentMove = 0;

function addMoves()
{
  for(i=0;i<mvAdded.length;i++) {
     var pos = "board_" + mvAdded[i].x + "_" + mvAdded[i].y;
     var posImg = document.images[pos];
     if(mvAdded[i].color == "B") {
         posImg.src = B.src;
     } else {
         posImg.src = W.src;
     }
  }
}

function playMoves()
{
  if (currentMove != move.length) {
     moveForward();
     window.setTimeout ("playMoves();",milliSec);
  }
}

function moveForward()
{
  if (currentMove != move.length) {
     var i =  move[currentMove].x;
     var j =  move[currentMove].y;
     var pos = "board_" + i + "_" + j;
     var posImg = document.images[pos];
     if(move[currentMove].color == "B") {
         posImg.src = B.src;
     } else if(move[currentMove].color == "W") {
         posImg.src = W.src;
     } else if(move[currentMove].color == "E") {
         posImg.src = "gifs/" + board[i][j] + ".gif";
     } //else do nothing.
     currentMove++;
     if(currentMove < move.length && move[currentMove].color == "E") {
        moveForward();
     }
  }
}

function moveBackward()
{
  if (currentMove != 0) {
     currentMove--;
     var i =  move[currentMove].x;
     var j =  move[currentMove].y;
     var pos = "board_" + i + "_" + j;
     document.images[pos].src = "gifs/" + board[i][j] + ".gif";
     if(move[currentMove].color == "E") {
       for(z=currentMove-1;z>=0;z--) {
          if(i == move[z].x && j == move[z].y) {
             if(move[z].color == "B") {
               document.images[pos].src = B.src;
             } else if(move[z].color == "W") {
               document.images[pos].src = W.src;
             }
             break;
          }
       }
       moveBackward();
     }
  }
}

function moveToEnd()
{
  while (currentMove != move.length) {
     moveForward();
  }
}

function moveToBeginning()
{
  while (currentMove != 0) {
     moveBackward();
  }
}
