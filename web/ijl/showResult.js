/************************************************************
 *  showResult.js
 *  Copyright 1999, Hyoungsoo Yoon. All Rights Reserved.
 *  This software is distributed on an "AS IS" basis, 
 *  WITHOUT WARRANTY OF ANY KIND, either express or implied.
 ************************************************************/

function showResult() {
  if(isNav4) {
    document.layers.result.visibility = "show";
  } else if(isIE4) {
    document.all.result.style.visibility = "visible";
  } else {
    window.alert('DHTML error!');
  }
}
