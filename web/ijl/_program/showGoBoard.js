/************************************************************
 *  showGoBoard.js
 *  Copyright 1999, Hyoungsoo Yoon. All Rights Reserved.
 *  This software is distributed on an "AS IS" basis, 
 *  WITHOUT WARRANTY OF ANY KIND, either express or implied.
 ************************************************************/

// Write style tags here.
document.writeln('<STYLE type="text/css">');
document.write('TABLE.goboard {');
document.write('border-collapse: collapse;');
document.write('width: 0.0in;');
document.write('margin: 0px;');
document.write('padding: 0.0in;');
document.write('border: 0.0in;');
document.writeln('}');
document.write('TABLE.goboard TR {');
document.write('margin: 0in;');
document.write('padding: 0in;');
document.write('border: 0in;');
document.writeln('}');
document.write('TABLE.goboard TD {');
document.write('margin: 0in;');
document.write('padding: 0in;');
document.writeln('}');
document.writeln('</STYLE>');

// Default (empty) board
var board = new Array();
for(k=0;k<19;k++) {
  board[k] = new Array("2", "5", "5", "5", "5", "5", "5", "5", "5",
             "5", "5", "5", "5", "5", "5", "5", "5", "5", "8");
}
board[0] = ["1", "4", "4", "4", "4", "4", "4", "4", "4",
            "4", "4", "4", "4", "4", "4", "4", "4", "4", "7"];
board[18] = ["3", "6", "6", "6", "6", "6", "6", "6", "6",
            "6", "6", "6", "6", "6", "6", "6", "6", "6", "9"];
board[3][3] = "H"; board[3][9] = "H"; board[3][15] = "H";
board[9][3] = "H"; board[9][9] = "H"; board[9][15] = "H";
board[15][3] = "H"; board[15][9] = "H"; board[15][15] = "H";

// Global variables
var boardColor = "gold";
var mSGF = "";

function  showGoBoard() {

  // [1] "Parse" Minimal-SGF String
  var sgf = mSGF.split(";");

  var a = "a";
  var xstart = 0;
  var ystart = 0;
  var xend =  18;
  var yend =  18;

  var mStart = 2;
  if(sgf[1].charAt(0) == "V") { 
    xstart = sgf[1].charCodeAt(3) - a.charCodeAt(0);
    ystart = sgf[1].charCodeAt(4) - a.charCodeAt(0);
    xend =  sgf[1].charCodeAt(6) - a.charCodeAt(0);
    yend =  sgf[1].charCodeAt(7) - a.charCodeAt(0);
  } else {
    mStart = 1;
  }
  for(m=mStart;m<sgf.length;m++) {
    var p = sgf[m].charCodeAt(2) - a.charCodeAt(0);
    var q = sgf[m].charCodeAt(3) - a.charCodeAt(0);
    board[p][q] = sgf[m].charAt(0);
  }

  // [2] "Draw" Go Board
  document.write('<TABLE class="goboard" ');
  document.writeln('style="background: ' + boardColor + '">');
  document.write('<TBODY>');
  document.write('<TR>');
  document.writeln('<TD noWrap>');
  for(j=ystart;j<=yend;j++) {
    for(i=xstart;i<=xend;i++) {
      document.write('<IMG src="gifs/' + board[i][j] + '.gif">');
    }
    document.writeln('<BR>');
  }
  document.write('</TD>');
  document.write('</TR>');
  document.write('</TBODY>');
  document.writeln('</TABLE>');
}
