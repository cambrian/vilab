/************************************************************
 *  displayGoBoard.js
 *  Copyright 1999, Hyoungsoo Yoon. All Rights Reserved.
 *  This software is distributed on an "AS IS" basis, 
 *  WITHOUT WARRANTY OF ANY KIND, either express or implied.
 ************************************************************/

// Global variables
var boardColor = "gold";
var mSGF = "";
var milliSec = 1000;
var currentMove = 0;

// Browser sniffer
var isNav4Up = false; 
var isIE4Up = false;
if (parseInt(navigator.appVersion.charAt(0)) >= 4) {
   if (navigator.appName == "Netscape") {
      isNav4Up = true
   } else if (navigator.appVersion.indexOf("MSIE") != -1) {
      isIE4Up = true
   }
}

// Default (empty) board
var board = new Array();
for(k=0;k<19;k++) {
  board[k] = new Array("2", "5", "5", "5", "5", "5", "5", "5", "5",
             "5", "5", "5", "5", "5", "5", "5", "5", "5", "8");
}
board[0] = ["1", "4", "4", "4", "4", "4", "4", "4", "4",
            "4", "4", "4", "4", "4", "4", "4", "4", "4", "7"];
board[18] = ["3", "6", "6", "6", "6", "6", "6", "6", "6",
            "6", "6", "6", "6", "6", "6", "6", "6", "6", "9"];
board[3][3] = "H"; board[3][9] = "H"; board[3][15] = "H";
board[9][3] = "H"; board[9][9] = "H"; board[9][15] = "H";
board[15][3] = "H"; board[15][9] = "H"; board[15][15] = "H";

// Board and stone images
var brd = new Array();
for(i=0;i<19;i++) {
  brd[i] = new Array();
  for(j=0;j<19;j++) {
    brd[i][j] = new Image();
    brd[i][j].src = 'gifs/' + board[i][j] + '.gif';
  }
}
var B = new Image();
B.src = 'gifs/B.gif';
var W = new Image();
W.src = 'gifs/W.gif';

// Move sequence
var move = new Array();

// Write style tags here.
function  writeStyles()
{
  document.writeln('<STYLE type="text/css">');
  document.writeln('TABLE.goboard {');
  //document.writeln('width: 0.01in;');
  document.writeln('margin: 0px;');
  document.writeln('padding: 0.0in;');
  document.writeln('border: 0.0in;');
  document.writeln('background: ' + boardColor + ';');
  document.writeln('}');
  document.writeln('TABLE.goboard TR {');
  document.writeln('margin: 0in;');
  document.writeln('padding: 0in;');
  document.writeln('border: 0in;');
  document.writeln('}');
  document.writeln('TABLE.goboard TD {');
  document.writeln('margin: 0in;');
  document.writeln('padding: 0in;');
  document.writeln('}');
  document.writeln('DIV.arrows {');
  document.writeln('text-align: center;');
  document.writeln('}');
  document.writeln('</STYLE>');
}

// Setup board and display moves
function  displayGoBoard() {

  // [1] "Parse" Minimal-SGF String
  var sgf = mSGF.split(";");

  var a = "a";
  var xstart = 0;
  var ystart = 0;
  var xend =  18;
  var yend =  18;

  var mStart = 2;
  if(sgf[1].charAt(0) == "V") { 
    xstart = sgf[1].charCodeAt(3) - a.charCodeAt(0);
    ystart = sgf[1].charCodeAt(4) - a.charCodeAt(0);
    xend =  sgf[1].charCodeAt(6) - a.charCodeAt(0);
    yend =  sgf[1].charCodeAt(7) - a.charCodeAt(0);
  } else {
    mStart = 1;
  }
  for(m=mStart;m<sgf.length;m++) {
    var p = sgf[m].charCodeAt(2) - a.charCodeAt(0);
    var q = sgf[m].charCodeAt(3) - a.charCodeAt(0);
    var mm = m - mStart;
    move[mm] = new Object();
    move[mm].color = sgf[m].charAt(0);
    move[mm].x = p;
    move[mm].y = q;
  }

  var boardWidth = (xend-xstart+1)*15;
  var boardHeight = (yend-ystart+1)*15;


  // [2] "Draw" Go Board
  document.write('<TABLE class="goboard" id="boardlayer">');
  document.write('<TBODY>');
  document.write('<TR>');
  document.writeln('<TD noWrap>');
  for(j=ystart;j<=yend;j++) {
    for(i=xstart;i<=xend;i++) {
      //var pos = "board_" + i + "_" + j;
      document.write('<IMG name="board_'+i+'_'+j+'" src="gifs/' + board[i][j] + '.gif">');
  }
    document.writeln('<BR>');
  }
  document.write('</TD>');
  document.write('</TR>');
  document.write('</TBODY>');
  document.writeln('</TABLE>');

  document.write('<DIV class="arrows" ');
  document.writeln('style="width: ' + boardWidth + ';">');
  document.writeln('<A href="javascript:moveBackward()"><IMG src="gifs/left.gif" border="0"></A>&nbsp;&nbsp;');
  document.writeln('<A href="javascript:playMoves()"><IMG src="gifs/clock.gif" border="0"></A>&nbsp;&nbsp;');
  document.writeln('<A href="javascript:moveForward()"><IMG src="gifs/right.gif" border="0"></A>');
  document.write('</DIV>');

  // [4] Play moves
  window.setTimeout ("playMoves();",2000);
}

function changeBoardColor(newColor) 
{
  if(isNav4Up) {
    document.layers["goboard"].background = newColor;
  } else if(isIE4Up) {
    document.all["boardlayer"].style.background = newColor;
  }
}

function playMoves()
{
  if (currentMove != move.length) {
     moveForward();
     window.setTimeout ("playMoves();",milliSec);
  }
}

function moveForward()
{
  if (currentMove != move.length) {
     var pos = "board_" + move[currentMove].x + "_" + move[currentMove].y;
     var posImg = document.images[pos];
     if(move[currentMove].color == "B") {
         posImg.src = B.src;
     } else {
         posImg.src = W.src;
     }
     currentMove++;
  }
}

function moveBackward()
{
  if (currentMove != 0) {
     currentMove--;
     var i =  move[currentMove].x;
     var j =  move[currentMove].y;
     var pos = "board_" + i + "_" + j;
     document.images[pos].src = "gifs/" + board[i][j] + ".gif";
  }
}
