/************************************************************
 *  showlogos.js
 *  Copyright 1999, Hyoungsoo Yoon. All Rights Reserved.
 *  This software is distributed on an "AS IS" basis, 
 *  WITHOUT WARRANTY OF ANY KIND, either express or implied.
 ************************************************************/

document.writeln('<TABLE width="90%">');
document.writeln('<COL width="*"><COL width="*"><COL width="*"><TBODY>');
document.writeln('<TR><TD align="center">');
document.writeln('<A href="http://www.w3.org/Style/CSS/Buttons"><IMG ');
document.writeln('src="images/logo_css.gif" ');
document.writeln('width="88" height="31" border="0"');
document.writeln('alt="Made with CSS"></A>');
document.writeln('</TD><TD align="center">');
document.writeln('<A href="http://www.microsoft.com/ie/logo.asp"><IMG ');
document.writeln('src="images/logo_ie.gif" ');
document.writeln('width="88" height="31" border="0"');
document.writeln('alt="Get Microsoft Internet Explorer"></A> ');
document.writeln('</TD><TD align="center">');
document.writeln('<A href="http://validator.w3.org/"><IMG ');
document.writeln('src="images/logo_html40.gif" ');
document.writeln('width="88" height="31" border="0"');
document.writeln('alt="Valid HTML 4.0!"></A>');
document.writeln('</TD></TR></TBODY></TABLE>');
