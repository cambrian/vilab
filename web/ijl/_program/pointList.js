/************************************************************
 *  showGoBoardWsh.js
 *  Copyright 1999, Hyoungsoo Yoon. All Rights Reserved.
 *  This software is distributed on an "AS IS" basis, 
 *  WITHOUT WARRANTY OF ANY KIND, either express or implied.
 ************************************************************/


var fso, hFile;
var filePath = "";
var fileTitle = "";
function CreateHtmlFile()
{
  fso = new ActiveXObject("Scripting.FileSystemObject");
  hFile = fso.CreateTextFile(filePath, true);
  hFile.WriteLine('<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0//EN">');
  hFile.WriteLine('<HTML lang="en">') ;
  hFile.WriteLine('<HEAD>') ;
  hFile.WriteLine('<META http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">');
  hFile.WriteLine('<TITLE>') ;
  hFile.WriteLine(fileTitle) ;
  hFile.WriteLine('</TITLE>') ;

  // Write style tags here.
  hFile.WriteLine('<STYLE>');
  hFile.WriteLine('BODY {');
  hFile.WriteLine('background: ivory;'); 
  hFile.WriteLine('}');
  hFile.WriteLine('TABLE.goboard {');
  hFile.WriteLine('margin: 0px;');
  hFile.WriteLine('padding: 0.0in;');
  hFile.WriteLine('border: 0.0in;');
  hFile.WriteLine('}');
  hFile.WriteLine('TABLE.goboard TR {');
  hFile.WriteLine('margin: 0in;');
  hFile.WriteLine('padding: 0in;');
  hFile.WriteLine('border: 0in;');
  hFile.WriteLine('}');
  hFile.WriteLine('TABLE.goboard TD {');
  hFile.WriteLine('margin: 0in;');
  hFile.WriteLine('padding: 0in;');
  hFile.WriteLine('}');
  hFile.WriteLine('</STYLE>');

  hFile.WriteLine('</HEAD>') ;
  hFile.WriteLine('<BODY>') ;
}
function CloseHtmlFile()
{
  hFile.WriteLine('</BODY>');
  hFile.WriteLine('</HTML>');
  hFile.Close();
}


// Default (empty) board
var board = new Array();
for(k=0;k<19;k++) {
  board[k] = new Array("2", "5", "5", "5", "5", "5", "5", "5", "5",
             "5", "5", "5", "5", "5", "5", "5", "5", "5", "8");
}
board[0] = ["1", "4", "4", "4", "4", "4", "4", "4", "4",
            "4", "4", "4", "4", "4", "4", "4", "4", "4", "7"];
board[18] = ["3", "6", "6", "6", "6", "6", "6", "6", "6",
            "6", "6", "6", "6", "6", "6", "6", "6", "6", "9"];
board[3][3] = "H"; board[3][9] = "H"; board[3][15] = "H";
board[9][3] = "H"; board[9][9] = "H"; board[9][15] = "H";
board[15][3] = "H"; board[15][9] = "H"; board[15][15] = "H";

// Global variables
var boardColor = "gold";
var mSGF = "";

function  showGoBoard() {

  // [1] "Parse" Minimal-SGF String
  var sgf = mSGF.split(";");

  var a = "a";
  var xstart = 0;
  var ystart = 0;
  var xend =  18;
  var yend =  18;

  var mStart = 2;
  if(sgf[1].charAt(0) == "V") { 
    xstart = sgf[1].charCodeAt(3) - a.charCodeAt(0);
    ystart = sgf[1].charCodeAt(4) - a.charCodeAt(0);
    xend =  sgf[1].charCodeAt(6) - a.charCodeAt(0);
    yend =  sgf[1].charCodeAt(7) - a.charCodeAt(0);
  } else {
    mStart = 1;
  }
  for(m=mStart;m<sgf.length;m++) {
    var p = sgf[m].charCodeAt(2) - a.charCodeAt(0);
    var q = sgf[m].charCodeAt(3) - a.charCodeAt(0);
    board[p][q] = sgf[m].charAt(0);
  }

  // [2] "Draw" Go Board
  hFile.Write('<TABLE class="goboard" ');
  hFile.WriteLine('style="background: ' + boardColor + '">');
  hFile.WriteLine('<TBODY>');
  hFile.Write('<TR>');
  hFile.Write('<TD noWrap>');
  for(j=ystart;j<=yend;j++) {
    for(i=xstart;i<=xend;i++) {
      hFile.Write('<IMG src="gifs/' + board[i][j] + '.gif">');
    }
    hFile.WriteLine('<BR>');
  }
  hFile.Write('</TD>');
  hFile.Write('</TR>');
  hFile.Write('</TBODY>');
  hFile.WriteLine('</TABLE>');
}


// Main part
boardColor = "gold";


//filePath = "huajum1.html"; mSGF = "(;VW[ia:sk];B[pd])";
//filePath = "huajum2.html"; mSGF = "(;VW[la:sh];B[pd])";

//filePath = "samsam1.html"; mSGF = "(;VW[ia:sk];B[qc])";
//filePath = "samsam2.html"; mSGF = "(;VW[la:sh];B[qc])";

//filePath = "sohmok1.html"; mSGF = "(;VW[ia:sk];B[qd])";
//filePath = "sohmok2.html"; mSGF = "(;VW[la:sh];B[qd])";

//filePath = "waemok1.html"; mSGF = "(;VW[ia:sk];B[qe])";
//filePath = "waemok2.html"; mSGF = "(;VW[la:sh];B[qe])";

//filePath = "gohmok1.html"; mSGF = "(;VW[ia:sk];B[pe])";
//filePath = "gohmok2.html"; mSGF = "(;VW[la:sh];B[pe])";

//filePath = "misc1.html"; mSGF = "(;VW[ia:sk])";
filePath = "misc2.html"; mSGF = "(;VW[la:sh])";



CreateHtmlFile();
showGoBoard();
CloseHtmlFile();
