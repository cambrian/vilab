# LaTeX2HTML 98.1p1 release (March 2nd, 1998)
# Associate internals original text with physical files.


$key = q/cite_DERJLMetal86:ParallelDistributed/;
$ref_files{$key} = "$dir".q|node9.html|; 
$noresave{$key} = "$nosave";

$key = q/cite_CMB95:NeuralNetworks/;
$ref_files{$key} = "$dir".q|node9.html|; 
$noresave{$key} = "$nosave";

$key = q/eqn:mthorder/;
$ref_files{$key} = "$dir".q|node3.html|; 
$noresave{$key} = "$nosave";

$key = q/cite_CLGTM:LearningInvariance/;
$ref_files{$key} = "$dir".q|node9.html|; 
$noresave{$key} = "$nosave";

$key = q/cite_MOWKetal:AbilityOptimal/;
$ref_files{$key} = "$dir".q|node9.html|; 
$noresave{$key} = "$nosave";

$key = q/cite_EBDHetal:StatisticalMechanics/;
$ref_files{$key} = "$dir".q|node9.html|; 
$noresave{$key} = "$nosave";

$key = q/cite_MR:LearningSequential/;
$ref_files{$key} = "$dir".q|node9.html|; 
$noresave{$key} = "$nosave";

$key = q/fig:secondorder/;
$ref_files{$key} = "$dir".q|node2.html|; 
$noresave{$key} = "$nosave";

$key = q/eqn:repVn/;
$ref_files{$key} = "$dir".q|node3.html|; 
$noresave{$key} = "$nosave";

$key = q/cite_EG:SpaceInteractions/;
$ref_files{$key} = "$dir".q|node9.html|; 
$noresave{$key} = "$nosave";

$key = q/cite_SH94:NeuralNetworks/;
$ref_files{$key} = "$dir".q|node9.html|; 
$noresave{$key} = "$nosave";

$key = q/cite_MOWK:StatisticalMechanics/;
$ref_files{$key} = "$dir".q|node9.html|; 
$noresave{$key} = "$nosave";

$key = q/eqn:lnV/;
$ref_files{$key} = "$dir".q|node3.html|; 
$noresave{$key} = "$nosave";

$key = q/cite_BHDGSetal:OptimalBrain/;
$ref_files{$key} = "$dir".q|node9.html|; 
$noresave{$key} = "$nosave";

$key = q/cite_HSNTetal:StatisticalMechanics/;
$ref_files{$key} = "$dir".q|node9.html|; 
$noresave{$key} = "$nosave";

$key = q/sec:conclusion/;
$ref_files{$key} = "$dir".q|node7.html|; 
$noresave{$key} = "$nosave";

$key = q/eqn:Q1Q2_0/;
$ref_files{$key} = "$dir".q|node3.html|; 
$noresave{$key} = "$nosave";

$key = q/cite_TMC:GeometricalStatistical/;
$ref_files{$key} = "$dir".q|node9.html|; 
$noresave{$key} = "$nosave";

$key = q/eqn:ysecond/;
$ref_files{$key} = "$dir".q|node2.html|; 
$noresave{$key} = "$nosave";

$key = q/eqn:Q1Q2_1/;
$ref_files{$key} = "$dir".q|node3.html|; 
$noresave{$key} = "$nosave";

$key = q/eqn:Q1Q2_2/;
$ref_files{$key} = "$dir".q|node3.html|; 
$noresave{$key} = "$nosave";

$key = q/cite_GAK:NeuralNetworks/;
$ref_files{$key} = "$dir".q|node9.html|; 
$noresave{$key} = "$nosave";

$key = q/cite_GLGD:SelfLearning/;
$ref_files{$key} = "$dir".q|node9.html|; 
$noresave{$key} = "$nosave";

$key = q/eqn:Px1/;
$ref_files{$key} = "$dir".q|node3.html|; 
$noresave{$key} = "$nosave";

$key = q/sec:introduction/;
$ref_files{$key} = "$dir".q|node1.html|; 
$noresave{$key} = "$nosave";

$key = q/cite_IK:EquivalenceDiscrete/;
$ref_files{$key} = "$dir".q|node9.html|; 
$noresave{$key} = "$nosave";

$key = q/eqn:alphac2/;
$ref_files{$key} = "$dir".q|node3.html|; 
$noresave{$key} = "$nosave";

$key = q/fig:perceptronalgorithm/;
$ref_files{$key} = "$dir".q|node6.html|; 
$noresave{$key} = "$nosave";

$key = q/eqn:saddle_1/;
$ref_files{$key} = "$dir".q|node5.html|; 
$noresave{$key} = "$nosave";

$key = q/fig:e_g/;
$ref_files{$key} = "$dir".q|node5.html|; 
$noresave{$key} = "$nosave";

$key = q/eqn:saddle_2/;
$ref_files{$key} = "$dir".q|node5.html|; 
$noresave{$key} = "$nosave";

$key = q/eqn:saddle_3/;
$ref_files{$key} = "$dir".q|node5.html|; 
$noresave{$key} = "$nosave";

$key = q/eqn:saddle_4/;
$ref_files{$key} = "$dir".q|node5.html|; 
$noresave{$key} = "$nosave";

$key = q/cite_CCJYC:BackPropagation/;
$ref_files{$key} = "$dir".q|node9.html|; 
$noresave{$key} = "$nosave";

$key = q/cite_DERGEHetal:LearningInternal/;
$ref_files{$key} = "$dir".q|node9.html|; 
$noresave{$key} = "$nosave";

$key = q/cite_JHOCKetal95:NeuralNetworks/;
$ref_files{$key} = "$dir".q|node9.html|; 
$noresave{$key} = "$nosave";

$key = q/sec:higherorder/;
$ref_files{$key} = "$dir".q|node2.html|; 
$noresave{$key} = "$nosave";

$key = q/eqn:spherical_1/;
$ref_files{$key} = "$dir".q|node2.html|; 
$noresave{$key} = "$nosave";

$key = q/sec:storage/;
$ref_files{$key} = "$dir".q|node3.html|; 
$noresave{$key} = "$nosave";

$key = q/cite_MLMSP69:Perceptrons/;
$ref_files{$key} = "$dir".q|node9.html|; 
$noresave{$key} = "$nosave";

$key = q/cite_KKJHO:GeneralizationTwo/;
$ref_files{$key} = "$dir".q|node9.html|; 
$noresave{$key} = "$nosave";

$key = q/eqn:dk/;
$ref_files{$key} = "$dir".q|node2.html|; 
$noresave{$key} = "$nosave";

$key = q/eqn:Hdefinition/;
$ref_files{$key} = "$dir".q|node3.html|; 
$noresave{$key} = "$nosave";

$key = q/eqn:Wspherical/;
$ref_files{$key} = "$dir".q|node3.html|; 
$noresave{$key} = "$nosave";

$key = q/eqn:Yspherical/;
$ref_files{$key} = "$dir".q|node3.html|; 
$noresave{$key} = "$nosave";

$key = q/eqn:Gammanormalize/;
$ref_files{$key} = "$dir".q|node3.html|; 
$noresave{$key} = "$nosave";

$key = q/cite_ACEG:PartiallyConnected/;
$ref_files{$key} = "$dir".q|node9.html|; 
$noresave{$key} = "$nosave";

$key = q/cite_IK:AsymmetricNeural/;
$ref_files{$key} = "$dir".q|node9.html|; 
$noresave{$key} = "$nosave";

$key = q/eqn:logz/;
$ref_files{$key} = "$dir".q|node4.html|; 
$noresave{$key} = "$nosave";

$key = q/cite_VNV95:NatureStatistical/;
$ref_files{$key} = "$dir".q|node9.html|; 
$noresave{$key} = "$nosave";

$key = q/sec:gibbs/;
$ref_files{$key} = "$dir".q|node4.html|; 
$noresave{$key} = "$nosave";

$key = q/cite_YCLGDetal:MachineLearning/;
$ref_files{$key} = "$dir".q|node9.html|; 
$noresave{$key} = "$nosave";

$key = q/cite_HSSHSetal:StatisticalMechanics/;
$ref_files{$key} = "$dir".q|node9.html|; 
$noresave{$key} = "$nosave";

1;

