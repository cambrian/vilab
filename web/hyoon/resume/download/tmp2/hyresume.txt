[1] Name
      Hyoungsoo Yoon, Ph.D.

[2] Contact
      http://www.vilab.com/hyoon/
      hyoon@vilab.com
      (831) 466-9055

[3] Mailing Address
      121 Felix St. APT #6 
      Santa Cruz, CA 95060-4824

[4] Summary of Qualifications
      (1) Problem identifying/solving ability acquired
      through higher education and postdoctoral
      experience, and through extensive software design
      and development.

      (2) Programming skills in scientific, GUI, and
      object-oriented/distributed software development
      both on MS Windows and Unix/Linux.  Also expertise
      in state-of-the-art Internet technologies.

[5] Technical Expertise
      (1) Object-oriented software development: C, C++/STL, 
      Java/JFC.  Windows programming with MFC/Win32 API.
      DirectX/OpenGL.  X-Window/Motif. Network programming.
      Object-oriented/distributed component development
      with RPC/RMI and DCOM/CORBA.  Scripting with Perl, 
      Tcl/Tk, VBScript, and JavaScript. 

      (2) Information technologies: Web site design and 
      development.  Dynamic HTML/CSS.  CGI programming with 
      C++/Perl.  Server-side/client-side scripting with 
      JavaScript.  Java applet/servlet.  Active server pages.
      ActiveX control development using ATL/MFC.  Expertise 
      in new standards of XML/XSL and DOM.  Database 
      programming with ODBC/JDBC.

      (3) Intelligent computation: Hands-on experience
      with various types of artificial neural networks; 
      multilayer perceptrons, radial basis function networks,
      higher-order neural networks, self-organizing maps, 
      on-line/batch learning dynamics, Hopfield networks,
      and support vector machines.  Broad working knowledge
      on modern tools of artificial intelligence; Expert 
      systems, Bayesian networks, hidden Markov models, 
      and fuzzy inference/control systems.

[6] Experience
      (1) Designed and developed numerous software
      modules, libraries, and applications.  Created and
      implemented many algorithms for neural networks,
      Bayesian networks, and fuzzy expert systems.
      Released HmmLib (Hidden Markov Model Library in Java)
      and NAPI (Neural-net API in Java) to the public domain.

      (2) Programmed and released a few shareware
      programs.  Intelligent Go Client (aka Igc98) is one
      of the most popular Windows client programs for
      various Internet go servers.  It is more than
      hundred thousand lines of code and it contains
      a basic go-playing module.
      
      (3) Designed, developed, and maintained numerous
      web sites.  Implemented a core-level API for W3C
      DOM recommendations.  Authored a few document type
      definition (DTD) files.  Programmed an XML editor on 
      Windows using Visual C++/MFC.

      (4) Analyzed the local minima problem in perceptron
      learning, which is one of the most fundamental
      questions in neural network learning theory.  Devised 
      an algorithm which avoids trapping in local minima
      in simple perceptron.
      
      (5) Created a new model of higher-order neural networks.
      Analyzed its learning properties for invariant pattern 
      recognition and classification.  Implemented its 
      algorithm in C++ and applied it to handwritten 
      character recognition.

      (6) Developed a new framework for sensitivity
      analysis in Bayesian networks based on information
      theory.  Implemented belief propagation algorithm
      under uncertainty.
      
      (7) Designed a new technique for polymer separation
      based on their molecular weights.  Implemented a 
      general-purpose simulation software for research in 
      polymer dynamics.  Created development environment
      for protein folding problem based on a lattice model
      and Monte-Carlo techniques.

      (8) Published more than ten papers in prestigious
      scientific journals.

[7] Employment History
      (1) September 1997 - Present, Postdoctoral researcher.
      Hebrew University, Jerusalem, Israel
      Racah Institute of Physics and Center for Neural Computation.
      
      (2) September 1995 - August 1997, Postdoctoral researcher.
      Pohang University of Science and Technology, Pohang, Korea
      Physics Department and Basic Science Research Institute.

[8] Education
      (1) Ph.D. in Physics, August 1995.
      University of California, Santa Cruz, USA.
      
      (2) M.S. in Physics, February 1990.
      Pohang University of Science and Technology, Pohang, Korea.
      
      (3) B.A. in Electronics Engineering, February 1988.
      Seoul National University, Seoul, Korea.

[9] References
      Available upon request.

