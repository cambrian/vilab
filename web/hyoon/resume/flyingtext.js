//
// flyingtext.js
//

var _isNav4, _isIE4;
if (parseInt(navigator.appVersion.charAt(0)) >= 4) {
  _isNav4 = (navigator.appName == "Netscape") ? true : false;
  _isIE4 = (navigator.appName.indexOf("Microsoft") != -1) ? true : false;
}

var dpx = 5;  // Distance per step (in pixel).
function getTop(layer) { 
    if(_isNav4) 
        return layer.top; 
    else
        return layer.style.pixelTop; 
}
function getLeft(layer) { 
    if(_isNav4) 
        return layer.left; 
    else
        return layer.style.pixelLeft; 
}
function setTop(layer, val) { 
    if(_isNav4) {
        layer.top = val; 
    } else {
        layer.style.pixelTop = val; 
    }
}
function setLeft(layer, val) { 
    if(_isNav4) {
        layer.left = val; 
    } else {
        layer.style.pixelLeft = val; 
    }
}
function incTop(layer, del) { 
    if(_isNav4) {
        layer.top += del; 
    } else {
        layer.style.pixelTop += del; 
    }
}
function incLeft(layer, del) { 
    if(_isNav4) {
        layer.left += del; 
    } else {
        layer.style.pixelLeft += del; 
    }
}


function FlyFromTop (oDiv,stopY)
{
   incTop(oDiv, dpx);
   if (getTop(oDiv) >= stopY) {
     setTop(oDiv, stopY);
     currentSequence++;     
     window.setTimeout ("flyIn();",500);
   }
   else
   {
      copyDiv = oDiv;
      copyY   = stopY;
      window.setTimeout ("FlyFromTop (copyDiv,copyY);", 10);
   }     
}

function FlyFromBottom (oDiv,stopY)
{
   incTop(oDiv, -dpx);
   if (getTop(oDiv) <= stopY) {
     setTop(oDiv, stopY);
     currentSequence++;     
     window.setTimeout ("flyIn();",500);
   }
   else
   {
      copyDiv = oDiv;
      copyY   = stopY;
      window.setTimeout ("FlyFromBottom (copyDiv,copyY);", 10);
   }     
}

function FlyFromRight (oImg,stopX)
{
   incLeft(oImg, -dpx);
   if (getLeft(oImg) <= stopX) {
     setLeft(oImg, stopX);
     currentSequence++;     
     window.setTimeout ("flyIn();",500);
   }   
   else
   {
     copyImg = oImg;
     copyX   = stopX;
     window.setTimeout ("FlyFromRight (copyImg,copyX);",10);
   }
}

function FlyFromLeft (oImg,stopX)
{
   incLeft(oImg, dpx);
   if (getLeft(oImg) >= stopX) {
     setLeft(oImg, stopX);
     currentSequence++;     
     window.setTimeout ("flyIn();",500);
   }
   else
   {
     copyImg = oImg;
     copyX   = stopX;
     window.setTimeout ("FlyFromLeft (copyImg,copyX);",10);
   }
}

function flyIn()
{ 
   if(!_isNav4 && !_isIE4) {
       for(var j=0; j<flyInSequence.length; j++) {
           flyInSequence[i].style.visibility= "visible";
       }
       return;
   }

   if (currentSequence != flyInSequence.length) {
      i = currentSequence;
      if(flyInDirection[i] == 'top') {
         // Fly From Top
         setTop(flyInSequence[i], document.body.offsetTop 
                                    - flyInSequence[i].offsetTop  
                                    - flyInSequence[i].offsetHeight);
         flyInSequence[i].style.visibility= "visible";
         window.setTimeout ("FlyFromTop (flyInSequence[i],0);",dpx);
      }
      else if(flyInDirection[i] == 'bottom') {
         // Fly From Bottom
         setTop(flyInSequence[i], document.body.offsetHeight
                                    - flyInSequence[i].offsetTop);
         flyInSequence[i].style.visibility= "visible";
         window.setTimeout ("FlyFromBottom (flyInSequence[i],0);",dpx);
      }
      else if(flyInDirection[i] == 'right') {
         // Fly From Right
         setLeft(flyInSequence[i], document.body.offsetWidth
                                    - flyInSequence[i].offsetLeft);
         flyInSequence[i].style.visibility= "visible";
         window.setTimeout ("FlyFromRight (flyInSequence[i],0);",dpx);
      }
      else if(flyInDirection[i] == 'left') {
         // Fly From Left
         setLeft(flyInSequence[i], document.body.offsetLeft
                                    - flyInSequence[i].offsetLeft
                                    - flyInSequence[i].offsetWidth);
         flyInSequence[i].style.visibility= "visible";
         window.setTimeout ("FlyFromLeft (flyInSequence[i],0);",dpx);
      }
   }
}

