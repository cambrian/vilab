/************************************************************
 *  Copyright 1999, Hyoungsoo Yoon. All Rights Reserved.
 *  This software is distributed on an "AS IS" basis, 
 *  WITHOUT WARRANTY OF ANY KIND, either express or implied.
 ************************************************************/

// Global variables
var mSGF = "";
var boardColor = "gold";
var backgroundColor = "ivory";
var commentsHolder = "";

var whitePlayer = "";
var blackPlayer = "";
var gameResult = "";

var fso, hFile;
var filePath = "";
var fileTitle = "";
var fileHeader = "";
function CreateHtmlFile()
{
  hFile = fso.CreateTextFile(filePath, true);
  hFile.WriteLine('<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0//EN">');
  hFile.WriteLine('<HTML lang="en">') ;
  hFile.WriteLine('<HEAD>') ;
  hFile.WriteLine('<META http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">');
  hFile.WriteLine('<TITLE>') ;
  hFile.WriteLine(fileTitle) ;
  hFile.WriteLine('</TITLE>') ;

  // Write style tags here.
  hFile.WriteLine('<STYLE TYPE="text/css">');
  hFile.Write('BODY { ');
  hFile.Write('background: ' + backgroundColor + '; ');
  hFile.WriteLine('}');
  hFile.Write('TABLE.goboard { ');
  hFile.Write('margin: 0px; ');
  hFile.Write('padding: 0.0in; ');
  hFile.Write('border: 0.0in ; ');
  hFile.Write('background: ' + boardColor + '; ');
  hFile.WriteLine('}');
  hFile.Write('TABLE.goboard TR { ');
  hFile.Write('margin: 0in; ');
  hFile.Write('padding: 0in; ');
  hFile.Write('border: 0in; ');
  hFile.WriteLine('}');
  hFile.Write('TABLE.goboard TD { ');
  hFile.Write('margin: 0in; ');
  hFile.Write('padding: 0in; ');
  hFile.WriteLine('}');
  hFile.Write('DIV.arrows { ');
  hFile.Write('text-align: center; ');
  hFile.WriteLine('}');
  hFile.Write('DIV.comments { ');
  hFile.Write('text-align: left; ');
  hFile.Write('margin-left: 0.2in; ');
  hFile.WriteLine('}');
  hFile.Write('#result { ');
  hFile.Write('position: relative; ');
  hFile.Write('visibility: hidden; ');
  hFile.Write('color: red; ');
  hFile.Write('font-size: 15pt; ');
  hFile.WriteLine('}');
  hFile.WriteLine('</STYLE>');

  // Browser sniffer
  hFile.WriteLine('<SCRIPT type="text/javascript" language="JavaScript">');
  hFile.WriteLine('  var isNav4, isIE4;');
  hFile.WriteLine('  if (parseInt(navigator.appVersion.charAt(0)) >= 4) {');
  hFile.WriteLine('     if (navigator.appName == "Netscape") {');
  hFile.WriteLine('	isNav4 = true;');
  hFile.WriteLine('     } else if (navigator.appVersion.indexOf("MSIE") != -1) {');
  hFile.WriteLine('	isIE4 = true;');
  hFile.WriteLine('     }');
  hFile.WriteLine('  }');
  hFile.WriteLine('</SCR' + 'IPT>');
  // Go board
  hFile.WriteLine('<SCRIPT type="text/javascript" language="JavaScript"');
  hFile.WriteLine('src="showResult.js">');
  hFile.WriteLine('</SCR' + 'IPT>');
  hFile.WriteLine('<SCRIPT type="text/javascript" language="JavaScript"');
  hFile.WriteLine('src="goBoard.js">');
  hFile.WriteLine('</SCR' + 'IPT>');
  hFile.WriteLine('<SCRIPT type="text/javascript" language="JavaScript"');
  hFile.WriteLine('src="boardImages.js">');
  hFile.WriteLine('</SCR' + 'IPT>');

  // Move functions
  hFile.WriteLine('<SCRIPT type="text/javascript" language="JavaScript"');
  hFile.WriteLine('src="displayMoves.js">');
  hFile.WriteLine('</SCR' + 'IPT>');

  hFile.WriteLine('</HEAD>') ;
  hFile.WriteLine('<BODY>') ;
}

function CloseHtmlFile()
{
  hFile.WriteLine('</BODY>');
  hFile.WriteLine('</HTML>');
  hFile.Close();
}

// Default (empty) board
var board = new Array();
for(k=0;k<19;k++) {
  board[k] = new Array("2", "5", "5", "5", "5", "5", "5", "5", "5",
             "5", "5", "5", "5", "5", "5", "5", "5", "5", "8");
}
board[0] = ["1", "4", "4", "4", "4", "4", "4", "4", "4",
            "4", "4", "4", "4", "4", "4", "4", "4", "4", "7"];
board[18] = ["3", "6", "6", "6", "6", "6", "6", "6", "6",
            "6", "6", "6", "6", "6", "6", "6", "6", "6", "9"];
board[3][3] = "H"; board[3][9] = "H"; board[3][15] = "H";
board[9][3] = "H"; board[9][9] = "H"; board[9][15] = "H";
board[15][3] = "H"; board[15][9] = "H"; board[15][15] = "H";

// Move sequence
var move = new Array();
var mvAdded = new Array();

// VW rectangle
var xstart = 0;
var ystart = 0;
var xend =  18;
var yend =  18;

// Parse "Minimal-SGF" String mSGF
function  parseMSgf() {

  var sgf = mSGF.split(";");
  var a = "a";

  var numAdded = 0;
  var mm = 0;
  for(m=1;m<sgf.length;m++) {
    var firstChar = sgf[m].charAt(0);
    if(firstChar == "V") { 
      var secondChar = sgf[m].charAt(1);
      if(secondChar == "W") {
	xstart = sgf[1].charCodeAt(3) - a.charCodeAt(0);
	ystart = sgf[1].charCodeAt(4) - a.charCodeAt(0);
	xend =  sgf[1].charCodeAt(6) - a.charCodeAt(0);
	yend =  sgf[1].charCodeAt(7) - a.charCodeAt(0);
      } // else if(secondChar == "[") it's value attribute.
    } else if(firstChar == "C") {  // trick....
      var setup = sgf[m].split("\n");
      for(u=1;u<setup.length;u++) {
        var fChar = setup[u].charAt(0);
	if(fChar == "P") {
	  var aClr = setup[u].charAt(1);
	  if(aClr == "W") {
	    var p1 = setup[u].split("[");
	    var p2 = p1[1].split("]");
	    whitePlayer = p2[0];
	  } else if(aClr == "B") {
	    var p1 = setup[u].split("[");
	    var p2 = p1[1].split("]");
	    blackPlayer = p2[0];
	  } //else ignore...
	} else if(fChar == "R") {
	  if(setup[u].charAt(1) == "E") {
	    var p1 = setup[u].split("[");
	    var p2 = p1[1].split("]");
	    gameResult = p2[0];
	  } //else ignore...
        }
      }
    } else if(firstChar == "A") {
      var aColor = sgf[m].charAt(1);
      if(aColor == "B" || aColor == "W" || aColor == "E") {
        var mvAd = sgf[m].split("[");
        for(h=1;h<mvAd.length;h++) {
          var r = mvAd[h].charCodeAt(0) - a.charCodeAt(0);
          var s = mvAd[h].charCodeAt(1) - a.charCodeAt(0);
          mvAdded[numAdded] = new Object();
          mvAdded[numAdded].color = aColor;
          mvAdded[numAdded].x = r;
          mvAdded[numAdded].y = s;
          numAdded++;
        }
      } //else ignore...
    } else if(firstChar == "E") {
      var mvDel = sgf[m].split("[");
      for(h=1;h<mvDel.length;h++) {
        var c = mvDel[h].charCodeAt(0) - a.charCodeAt(0);
        var d = mvDel[h].charCodeAt(1) - a.charCodeAt(0);
	  move[mm] = new Object();
	  move[mm].color = "E";
	  move[mm].x = c;
	  move[mm].y = d;
	  mm++;
      }
    } else if(firstChar == "B" || firstChar == "W") {
      var p = sgf[m].charCodeAt(2) - a.charCodeAt(0);
      var q = sgf[m].charCodeAt(3) - a.charCodeAt(0);
      move[mm] = new Object();
      move[mm].color = sgf[m].charAt(0);
      move[mm].x = p;
      move[mm].y = q;
      mm++;
    } //else do nothing...
  }
}

function  displayGoBoard() {

  // [1] "Parse" Minimal-SGF String
  //parseMSgf();

  // [2] Write move sequence
  hFile.WriteLine('<SCRIPT type="text/javascript" language="JavaScript">');
  hFile.WriteLine('var move = new Array();');
  for(n=0;n<move.length;n++) {
    hFile.WriteLine(' move['+n+'] = new Object();');
    hFile.Write(' move['+n+'].color = "' + move[n].color + '";');
    hFile.Write(' move['+n+'].x = ' + move[n].x + ';');
    hFile.WriteLine(' move['+n+'].y = ' + move[n].y + ';');
  }
  hFile.WriteLine('var mvAdded = new Array();');
  for(n=0;n<mvAdded.length;n++) {
    hFile.WriteLine(' mvAdded['+n+'] = new Object();');
    hFile.Write(' mvAdded['+n+'].color = "' + mvAdded[n].color + '";');
    hFile.Write(' mvAdded['+n+'].x = ' + mvAdded[n].x + ';');
    hFile.WriteLine(' mvAdded['+n+'].y = ' + mvAdded[n].y + ';');
  }
  hFile.WriteLine('</SCR' + 'IPT>');

  // [3] Write heading
  hFile.WriteLine('<H2>');
  hFile.WriteLine(fileHeader);
  hFile.WriteLine('</H2>');

  var boardWidth = (xend-xstart+1)*15;
  var boardHeight = (yend-ystart+1)*15;

  // [4-1] Draw outer table  
  hFile.Write('<TABLE>');
  hFile.Write('<TBODY>');
  hFile.Write('<TR>');
  hFile.WriteLine('<TD noWrap vAlign="top">');

  // [5] "Draw" Go Board
  hFile.Write('<TABLE class="goboard" id="boardlayer">');
  hFile.WriteLine('<TBODY>');
  hFile.Write('<TR>');
  hFile.WriteLine('<TD noWrap>');
  for(j=ystart;j<=yend;j++) {
    for(i=xstart;i<=xend;i++) {
      hFile.Write('<IMG name="board_'+i+'_'+j+'" src="gifs/' + board[i][j] + '.gif">');
    }
    hFile.WriteLine('<BR>');
  }
  hFile.Write('</TD>');
  hFile.WriteLine('</TR>');
  hFile.Write('</TBODY>');
  hFile.WriteLine('</TABLE>');

  // [6] Show move-buttons
  hFile.Write('<DIV class="arrows" ');
  hFile.WriteLine('style="width: ' + boardWidth + ';">');
  hFile.Write('&nbsp;<A href="javascript:moveToBeginning();" ');
  hFile.Write('onMouseOver="self.status=\'Move To Beginning\'; return true;"');
  hFile.Write('onMouseOut="self.status=\'\'; return true;"');
  hFile.Write('title="Move To Beginning"');
  hFile.WriteLine('><IMG src="gifs/lleft.gif" border="0"></A>&nbsp;&nbsp;');
  hFile.Write('<A href="javascript:moveBackward();" ');
  hFile.Write('onMouseOver="self.status=\'Move Backward\'; return true;"');
  hFile.Write('onMouseOut="self.status=\'\'; return true;"');
  hFile.Write('title="Move Backward"');
  hFile.WriteLine('><IMG src="gifs/left.gif" border="0"></A>&nbsp;&nbsp;');
  hFile.Write('<A href="javascript:playMoves();"');
  hFile.Write('onMouseOver="self.status=\'Continuous Play\'; return true;"');
  hFile.Write('onMouseOut="self.status=\'\'; return true;"');
  hFile.Write('title="Continuous Play"');
  hFile.WriteLine('><IMG src="gifs/clock.gif" border="0"></A>&nbsp;&nbsp;');
  hFile.Write('<A href="javascript:moveForward();"');
  hFile.Write('onMouseOver="self.status=\'Move Forward\'; return true;"');
  hFile.Write('onMouseOut="self.status=\'\'; return true;"');
  hFile.Write('title="Move Forward"');
  hFile.WriteLine('><IMG src="gifs/right.gif" border="0"></A>&nbsp;&nbsp;');
  hFile.Write('<A href="javascript:moveToEnd();"');
  hFile.Write('onMouseOver="self.status=\'Move To End\'; return true;"');
  hFile.Write('onMouseOut="self.status=\'\'; return true;"');
  hFile.Write('title="Move To End"');
  hFile.WriteLine('><IMG src="gifs/rright.gif" border="0"></A>');
  hFile.WriteLine('</DIV>');

  // [4-2] Draw outer table  
  hFile.WriteLine('</TD><TD vAlign="top">');
  hFile.WriteLine('<DIV class="comments">');
  hFile.WriteLine(commentsHolder);

  hFile.WriteLine(' ');
  //hFile.WriteLine('<H3>Players</H3>');
  hFile.WriteLine('<P>');
  hFile.Write('<IMG src="gifs/W.gif">&nbsp;&nbsp; ');
  hFile.WriteLine(whitePlayer);
  hFile.WriteLine('<BR>');
  hFile.Write('<IMG src="gifs/B.gif">&nbsp;&nbsp; ');
  hFile.WriteLine(blackPlayer);
  hFile.WriteLine('</P>');
  //hFile.WriteLine('<H3>Result</H3>');
  hFile.Write("Haven't figured out the result? ");
  hFile.Write('Click ');
  hFile.WriteLine('<A href="javascript:showResult()">here</A>.');
  hFile.WriteLine('<DIV id="result">');
  hFile.WriteLine('Result: ' + gameResult);
  hFile.WriteLine('</DIV>');
  hFile.WriteLine('<BR><BR>');
  hFile.WriteLine(' ');
  hFile.WriteLine('<H3>Download</H3>');
  hFile.WriteLine('<P>');
  hFile.WriteLine('<UL>');
  hFile.Write('<LI>');
  hFile.Write('<A href="' + newSgfName + '">SGF File</A>');
  //hFile.Write('<A href="' + newHtmlName + '">HTML File</A>');
  hFile.WriteLine('</LI>');
  hFile.Write('<LI>');
  hFile.Write('HTML File');
  hFile.WriteLine('</LI>');
  hFile.WriteLine('</UL>');
  hFile.WriteLine('</P>');
/*
  hFile.WriteLine('<H3>Comments</H3>');
  hFile.WriteLine('<P>');
  hFile.WriteLine(' ');
  hFile.WriteLine('</P>');
  hFile.WriteLine(' ');
*/
  hFile.WriteLine('</DIV>');
  hFile.Write('</TD>');
  hFile.Write('</TR>');
  hFile.Write('</TBODY>');
  hFile.WriteLine('</TABLE>');

  // [7] Start sripting
  hFile.WriteLine('<SCRIPT type="text/javascript" language="JavaScript">');
  hFile.WriteLine('milliSec = 1000;');
  hFile.WriteLine('var initDelay = 2000;');
  hFile.WriteLine('addMoves();');
  hFile.WriteLine('window.setTimeout ("playMoves();",initDelay);');
  hFile.WriteLine('</SCR' + 'IPT>');

  // [8] Write the rest of the document here
  hFile.WriteLine('<DIV class="mainbody">');
  hFile.WriteLine('<!-- Main part here -->');
  hFile.WriteLine(' ');
  hFile.WriteLine('</DIV>');

  // [9] Copyright statement
  hFile.WriteLine('<HR>');
  hFile.WriteLine('<SCRIPT type="text/javascript" language="JavaScript"');
  hFile.WriteLine('src="copyright.js">');
  hFile.WriteLine('</SCR' + 'IPT>');
}
commentsHolder = "<!-- Comments Here --> ";


// Main part
var ForReading = 1;
var ABCD = "abcdefghijklmnopqrs";  // Hack!!! ;(
var newSgfName = "";
var newHtmlName = "";
var folderPath = "cfolder";
fso = new ActiveXObject("Scripting.FileSystemObject");

var f = fso.GetFolder(folderPath);
var fc = new Enumerator(f.files);
for (; !fc.atEnd(); fc.moveNext())
{
  var fileOldName = fc.item();
  var ts = fso.OpenTextFile(fileOldName, ForReading);
  mSGF = ts.ReadAll();
  ts.Close();

  // clean up
  //move.length = 0;
  //moveAdded.length = 0;
  move = new Array();
  mvAdded = new Array();

  parseMSgf();
  var newName = "";
  for(n=0;n<16;n++) {
     newName += ABCD.charAt(move[n].x);
     newName += ABCD.charAt(move[n].y);
  }
  newSgfName = newName + '.sgf';
  newHtmlName = newName + '.html';

  //change sgf name
  //var f2 = fso.GetFile(fileOldName);
  //f2.Move (newSgfName);

  filePath = newHtmlName;
  fileTitle = 'IJL: Internet Joseki Library | Cho Hunhyun | ' + newSgfName;
  fileHeader = newSgfName;

  CreateHtmlFile();
  displayGoBoard();
  CloseHtmlFile();
}

