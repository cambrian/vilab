/************************************************************
 *  boardImages.js
 *  Copyright 1999, Hyoungsoo Yoon. All Rights Reserved.
 *  This software is distributed on an "AS IS" basis, 
 *  WITHOUT WARRANTY OF ANY KIND, either express or implied.
 ************************************************************/

// Board and stone images
var B = new Image(15,15);
B.src = 'g/B.gif';
var W = new Image(15,15);
W.src = 'g/W.gif';

var brd = new Array();
for(i=0;i<19;i++) {
  brd[i] = new Array();
  for(j=0;j<19;j++) {
    brd[i][j] = new Image(15,15);
    brd[i][j].src = 'g/' + board[i][j] + '.gif';
  }
}
