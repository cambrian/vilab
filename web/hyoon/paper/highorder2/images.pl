# LaTeX2HTML 98.1p1 release (March 2nd, 1998)
# Associate images original text with physical files.


$key = q/{inline}{group1groupover2}{inline}MSF=1.4;AAT;/;
$cached_env_img{$key} = q|<IMG
 WIDTH="12" HEIGHT="30" ALIGN="MIDDLE" BORDER="0"
 SRC="img10.gif"
 ALT="$ {\begingroup 1\endgroup \over 2}$">|; 

$key = q/{indisplay}displaystylesqrt{N}{indisplay}MSF=1.4;AAT;/;
$cached_env_img{$key} = q|<IMG
 WIDTH="27" HEIGHT="34" ALIGN="MIDDLE" BORDER="0"
 SRC="img103.gif"
 ALT="$\displaystyle \sqrt{N}$">|; 

$key = q/{indisplay}displaystylesqrt{d_k}{indisplay}MSF=1.4;AAT;/;
$cached_env_img{$key} = q|<IMG
 WIDTH="31" HEIGHT="35" ALIGN="MIDDLE" BORDER="0"
 SRC="img111.gif"
 ALT="$\displaystyle \sqrt{d_k}$">|; 

$key = q/{indisplay}displaystyleprod_{i<j}^{}{indisplay}MSF=1.4;AAT;/;
$cached_env_img{$key} = q|<IMG
 WIDTH="21" HEIGHT="46" ALIGN="MIDDLE" BORDER="0"
 SRC="img56.gif"
 ALT="$\displaystyle \prod_{i<j}^{}$">|; 

$key = q/{indisplay}displaystyle{frac{e^{-frac{1}{2}s^2}z}{(e^beta-1)^{-1}+H(s)}}{indisplay}MSF=1.4;AAT;/;
$cached_env_img{$key} = q|<IMG
 WIDTH="117" HEIGHT="53" ALIGN="MIDDLE" BORDER="0"
 SRC="img139.gif"
 ALT="$\displaystyle {\frac{e^{-\frac{1}{2}s^2}z}{(e^\beta -1)^{-1} + H(s)}}$">|; 

$key = q/{inline}gamma_{1}^{t}{inline}MSF=1.4;AAT;/;
$cached_env_img{$key} = q|<IMG
 WIDTH="16" HEIGHT="29" ALIGN="MIDDLE" BORDER="0"
 SRC="img153.gif"
 ALT="$ \gamma_{1}^{t}$">|; 

$key = q/{indisplay}displaystylegamma_{2}^{t}{indisplay}MSF=1.4;AAT;/;
$cached_env_img{$key} = q|<IMG
 WIDTH="16" HEIGHT="30" ALIGN="MIDDLE" BORDER="0"
 SRC="img114.gif"
 ALT="$\displaystyle \gamma_{2}^{t}$">|; 

$key = q/{indisplay}scriptstyle{frac{(r+tsqrt{q})^2}{1-q}}{indisplay}MSF=1.4;AAT;/;
$cached_env_img{$key} = q|<IMG
 WIDTH="48" HEIGHT="32" ALIGN="MIDDLE" BORDER="0"
 SRC="img74.gif"
 ALT="$\scriptstyle {\frac{(r+t\sqrt{q})^2}{1-q}}$">|; 

$key = q/{indisplay}displaystylegamma_{k}^{}{indisplay}MSF=1.4;AAT;/;
$cached_env_img{$key} = q|<IMG
 WIDTH="17" HEIGHT="25" ALIGN="MIDDLE" BORDER="0"
 SRC="img31.gif"
 ALT="$\displaystyle \gamma_{k}^{}$">|; 

$key = q/{indisplay}displaystyleleft[vphantom{e^{-beta}+(1-e^{-beta})H(frac{sqrt{q-R^2}t-Rz}{sqrt{1-q}})}right.{indisplay}MSF=1.4;AAT;/;
$cached_env_img{$key} = q|<IMG
 WIDTH="13" HEIGHT="56" ALIGN="MIDDLE" BORDER="0"
 SRC="img131.gif"
 ALT="$\displaystyle \left[\vphantom{
e^{-\beta} + (1-e^{-\beta})H(\frac{\sqrt{q-R^2}t-Rz}{\sqrt{1-q}})
}\right.$">|; 

$key = q/{indisplay}displaystylegamma_{1}^{}{indisplay}MSF=1.4;AAT;/;
$cached_env_img{$key} = q|<IMG
 WIDTH="16" HEIGHT="25" ALIGN="MIDDLE" BORDER="0"
 SRC="img38.gif"
 ALT="$\displaystyle \gamma_{1}^{}$">|; 

$key = q/{indisplay}displaystyle{frac{gamma_2}{sqrt{frac{N(N-1)}{2}}}}{indisplay}MSF=1.4;AAT;/;
$cached_env_img{$key} = q|<IMG
 WIDTH="66" HEIGHT="55" ALIGN="MIDDLE" BORDER="0"
 SRC="img34.gif"
 ALT="$\displaystyle {\frac{\gamma_2}{\sqrt{\frac{N(N-1)}{2}}}}$">|; 

$key = q/{inline}{frac{1}{2}}{inline}MSF=1.4;AAT;/;
$cached_env_img{$key} = q|<IMG
 WIDTH="12" HEIGHT="30" ALIGN="MIDDLE" BORDER="0"
 SRC="img143.gif"
 ALT="$ {\frac{1}{2}}$">|; 

$key = q/{indisplay}displaystyle{frac{q_2}{1-q_2}}{indisplay}MSF=1.4;AAT;/;
$cached_env_img{$key} = q|<IMG
 WIDTH="43" HEIGHT="38" ALIGN="MIDDLE" BORDER="0"
 SRC="img82.gif"
 ALT="$\displaystyle {\frac{q_2}{1-q_2}}$">|; 

$key = q/{indisplay}displaystyleleft(vphantom{{array}{c}Nk{array}}right.{indisplay}MSF=1.4;AAT;/;
$cached_env_img{$key} = q|<IMG
 WIDTH="15" HEIGHT="47" ALIGN="MIDDLE" BORDER="0"
 SRC="img24.gif"
 ALT="$\displaystyle \left(\vphantom{ \begin{array}{c}
N \\\\
k
\end{array} }\right.$">|; 

$key = q/{inline}rightarrow{inline}MSF=1.4;AAT;/;
$cached_env_img{$key} = q|<IMG
 WIDTH="17" HEIGHT="13" ALIGN="BOTTOM" BORDER="0"
 SRC="img16.gif"
 ALT="$ \rightarrow$">|; 

$key = q/{indisplay}displaystyleeta^{(2)}_{}{indisplay}MSF=1.4;AAT;/;
$cached_env_img{$key} = q|<IMG
 WIDTH="25" HEIGHT="33" ALIGN="MIDDLE" BORDER="0"
 SRC="img157.gif"
 ALT="$\displaystyle \eta^{(2)}_{}$">|; 

$key = q/{indisplay}displaystyle{frac{1}{pi}}{indisplay}MSF=1.4;AAT;/;
$cached_env_img{$key} = q|<IMG
 WIDTH="15" HEIGHT="44" ALIGN="MIDDLE" BORDER="0"
 SRC="img140.gif"
 ALT="$\displaystyle {\frac{1}{\pi}}$">|; 

$key = q/{inline}{group1groupoversqrt{2}}{inline}MSF=1.4;AAT;/;
$cached_env_img{$key} = q|<IMG
 WIDTH="21" HEIGHT="30" ALIGN="MIDDLE" BORDER="0"
 SRC="img2.gif"
 ALT="$ {\begingroup 1\endgroup \over \sqrt {2}}$">|; 

$key = q/{indisplay}displaystylesum_{i<j}^{}{indisplay}MSF=1.4;AAT;/;
$cached_env_img{$key} = q|<IMG
 WIDTH="23" HEIGHT="46" ALIGN="MIDDLE" BORDER="0"
 SRC="img42.gif"
 ALT="$\displaystyle \sum_{i<j}^{}$">|; 

$key = q/{inline}gamma_{2}^{t}{inline}MSF=1.4;AAT;/;
$cached_env_img{$key} = q|<IMG
 WIDTH="16" HEIGHT="29" ALIGN="MIDDLE" BORDER="0"
 SRC="img154.gif"
 ALT="$ \gamma_{2}^{t}$">|; 

$key = q/{indisplay}displaystylelangle{indisplay}MSF=1.4;AAT;/;
$cached_env_img{$key} = q|<IMG
 WIDTH="8" HEIGHT="28" ALIGN="MIDDLE" BORDER="0"
 SRC="img58.gif"
 ALT="$\displaystyle \langle$">|; 

$key = q/{inline}ll{inline}MSF=1.4;AAT;/;
$cached_env_img{$key} = q|<IMG
 WIDTH="17" HEIGHT="25" ALIGN="MIDDLE" BORDER="0"
 SRC="img108.gif"
 ALT="$ \ll$">|; 

$key = q/{indisplay}displaystylesum{indisplay}MSF=1.4;AAT;/;
$cached_env_img{$key} = q|<IMG
 WIDTH="23" HEIGHT="36" ALIGN="MIDDLE" BORDER="0"
 SRC="img57.gif"
 ALT="$\displaystyle \sum$">|; 

$key = q/{inline}{group1groupoverpi}{inline}MSF=1.4;AAT;/;
$cached_env_img{$key} = q|<IMG
 WIDTH="13" HEIGHT="30" ALIGN="MIDDLE" BORDER="0"
 SRC="img6.gif"
 ALT="$ {\begingroup 1\endgroup \over \pi}$">|; 

$key = q/{indisplay}displaystyletheta{indisplay}MSF=1.4;AAT;/;
$cached_env_img{$key} = q|<IMG
 WIDTH="10" HEIGHT="13" ALIGN="BOTTOM" BORDER="0"
 SRC="img52.gif"
 ALT="$\displaystyle \theta$">|; 

$key = q/{indisplay}displaystyleint_{-infty}^{infty}{indisplay}MSF=1.4;AAT;/;
$cached_env_img{$key} = q|<IMG
 WIDTH="31" HEIGHT="46" ALIGN="MIDDLE" BORDER="0"
 SRC="img137.gif"
 ALT="$\displaystyle \int_{-\infty}^{\infty}$">|; 

$key = q/{indisplay}displaystylesqrt{frac{2}{pi}}{indisplay}MSF=1.4;AAT;/;
$cached_env_img{$key} = q|<IMG
 WIDTH="29" HEIGHT="53" ALIGN="MIDDLE" BORDER="0"
 SRC="img136.gif"
 ALT="$\displaystyle \sqrt{\frac{2}{\pi}}$">|; 

$key = q/{indisplay}displaystylerangle{indisplay}MSF=1.4;AAT;/;
$cached_env_img{$key} = q|<IMG
 WIDTH="8" HEIGHT="28" ALIGN="MIDDLE" BORDER="0"
 SRC="img59.gif"
 ALT="$\displaystyle \rangle$">|; 

$key = q/{inline}pm{inline}MSF=1.4;AAT;/;
$cached_env_img{$key} = q|<IMG
 WIDTH="14" HEIGHT="25" ALIGN="MIDDLE" BORDER="0"
 SRC="img22.gif"
 ALT="$ \pm$">|; 

$key = q/{indisplay}scriptstyleprime{indisplay}MSF=1.4;AAT;/;
$cached_env_img{$key} = q|<IMG
 WIDTH="6" HEIGHT="13" ALIGN="BOTTOM" BORDER="0"
 SRC="img155.gif"
 ALT="$\scriptstyle \prime$">|; 

$key = q/{indisplay}scriptstylemu{indisplay}MSF=1.4;AAT;/;
$cached_env_img{$key} = q|<IMG
 WIDTH="10" HEIGHT="25" ALIGN="MIDDLE" BORDER="0"
 SRC="img44.gif"
 ALT="$\scriptstyle \mu$">|; 

$key = q/{indisplay}scriptstylenu{indisplay}MSF=1.4;AAT;/;
$cached_env_img{$key} = q|<IMG
 WIDTH="9" HEIGHT="13" ALIGN="BOTTOM" BORDER="0"
 SRC="img87.gif"
 ALT="$\scriptstyle \nu$">|; 

$key = q/{indisplay}displaystyle{frac{N(N-1)}{4}}{indisplay}MSF=1.4;AAT;/;
$cached_env_img{$key} = q|<IMG
 WIDTH="66" HEIGHT="47" ALIGN="MIDDLE" BORDER="0"
 SRC="img80.gif"
 ALT="$\displaystyle {\frac{N(N-1)}{4}}$">|; 

$key = q/{inline}eta^{(1)}_{}{inline}MSF=1.4;AAT;/;
$cached_env_img{$key} = q|<IMG
 WIDTH="25" HEIGHT="32" ALIGN="MIDDLE" BORDER="0"
 SRC="img12.gif"
 ALT="$ \eta^{(1)}_{}$">|; 

$key = q/{inline}{frac{1}{sqrt{2}}}{inline}MSF=1.4;AAT;/;
$cached_env_img{$key} = q|<IMG
 WIDTH="21" HEIGHT="30" ALIGN="MIDDLE" BORDER="0"
 SRC="img141.gif"
 ALT="$ {\frac{1}{\sqrt{2}}}$">|; 

$key = q/{indisplay}displaystyleleft.vphantom{{array}{c}Nk{array}}right){indisplay}MSF=1.4;AAT;/;
$cached_env_img{$key} = q|<IMG
 WIDTH="15" HEIGHT="47" ALIGN="MIDDLE" BORDER="0"
 SRC="img26.gif"
 ALT="$\displaystyle \left.\vphantom{ \begin{array}{c}
N \\\\
k
\end{array} }\right)$">|; 

$key = q/{indisplay}displaystyle{frac{gamma_1}{sqrt{N}}}{indisplay}MSF=1.4;AAT;/;
$cached_env_img{$key} = q|<IMG
 WIDTH="30" HEIGHT="38" ALIGN="MIDDLE" BORDER="0"
 SRC="img33.gif"
 ALT="$\displaystyle {\frac{\gamma_1}{\sqrt{N}}}$">|; 

$key = q/{indisplay}displaystyledelta{indisplay}MSF=1.4;AAT;/;
$cached_env_img{$key} = q|<IMG
 WIDTH="9" HEIGHT="13" ALIGN="BOTTOM" BORDER="0"
 SRC="img54.gif"
 ALT="$\displaystyle \delta$">|; 

$key = q/{indisplay}displaystylesqrt{frac{2}{N(N-1)}}{indisplay}MSF=1.4;AAT;/;
$cached_env_img{$key} = q|<IMG
 WIDTH="80" HEIGHT="58" ALIGN="MIDDLE" BORDER="0"
 SRC="img41.gif"
 ALT="$\displaystyle \sqrt{\frac{2}{N(N-1)}}$">|; 

$key = q/{inline}{frac{1}{pi}}{inline}MSF=1.4;AAT;/;
$cached_env_img{$key} = q|<IMG
 WIDTH="13" HEIGHT="30" ALIGN="MIDDLE" BORDER="0"
 SRC="img142.gif"
 ALT="$ {\frac{1}{\pi}}$">|; 

$key = q/{inline}sigma{inline}MSF=1.4;AAT;/;
$cached_env_img{$key} = q|<IMG
 WIDTH="11" HEIGHT="13" ALIGN="BOTTOM" BORDER="0"
 SRC="img89.gif"
 ALT="$ \sigma$">|; 

$key = q/{indisplay}scriptstylesigma{indisplay}MSF=1.4;AAT;/;
$cached_env_img{$key} = q|<IMG
 WIDTH="10" HEIGHT="13" ALIGN="BOTTOM" BORDER="0"
 SRC="img63.gif"
 ALT="$\scriptstyle \sigma$">|; 

$key = q/{indisplay}displaystylegamma_{1}^{2}{indisplay}MSF=1.4;AAT;/;
$cached_env_img{$key} = q|<IMG
 WIDTH="17" HEIGHT="31" ALIGN="MIDDLE" BORDER="0"
 SRC="img84.gif"
 ALT="$\displaystyle \gamma_{1}^{2}$">|; 

$key = q/{indisplay}displaystylerangle_{P(vec{x})}^{}{indisplay}MSF=1.4;AAT;/;
$cached_env_img{$key} = q|<IMG
 WIDTH="33" HEIGHT="28" ALIGN="MIDDLE" BORDER="0"
 SRC="img120.gif"
 ALT="$\displaystyle \rangle_{P(\vec{x})}^{}$">|; 

$key = q/{indisplay}displaystyleepsilon{indisplay}MSF=1.4;AAT;/;
$cached_env_img{$key} = q|<IMG
 WIDTH="8" HEIGHT="13" ALIGN="BOTTOM" BORDER="0"
 SRC="img117.gif"
 ALT="$\displaystyle \epsilon$">|; 

$key = q/{indisplay}displaystyleleft.vphantom{ln(1-q_1)+frac{q_1}{1-q_1}}right]{indisplay}MSF=1.4;AAT;/;
$cached_env_img{$key} = q|<IMG
 WIDTH="12" HEIGHT="47" ALIGN="MIDDLE" BORDER="0"
 SRC="img79.gif"
 ALT="$\displaystyle \left.\vphantom{
\ln(1-q_1) + \frac{q_1}{1-q_1} }\right]$">|; 

$key = q/{inline}vec{x}^{mu}_{}{inline}MSF=1.4;AAT;/;
$cached_env_img{$key} = q|<IMG
 WIDTH="18" HEIGHT="27" ALIGN="MIDDLE" BORDER="0"
 SRC="img46.gif"
 ALT="$ \vec{x}^{\mu}_{}$">|; 

$key = q/{indisplay}displaystyle{frac{R}{(q-R^2)^{1slash2}}}{indisplay}MSF=1.4;AAT;/;
$cached_env_img{$key} = q|<IMG
 WIDTH="75" HEIGHT="45" ALIGN="MIDDLE" BORDER="0"
 SRC="img148.gif"
 ALT="$\displaystyle {\frac{R}{(q-R^2)^{1/2}}}$">|; 

$key = q/{indisplay}scriptstylebeta{indisplay}MSF=1.4;AAT;/;
$cached_env_img{$key} = q|<IMG
 WIDTH="10" HEIGHT="25" ALIGN="MIDDLE" BORDER="0"
 SRC="img122.gif"
 ALT="$\scriptstyle \beta$">|; 

$key = q/{inline}{frac{p}{N}}{inline}MSF=1.4;AAT;/;
$cached_env_img{$key} = q|<IMG
 WIDTH="16" HEIGHT="28" ALIGN="MIDDLE" BORDER="0"
 SRC="img144.gif"
 ALT="$ {\frac{p}{N}}$">|; 

$key = q/{inline}mu{inline}MSF=1.4;AAT;/;
$cached_env_img{$key} = q|<IMG
 WIDTH="11" HEIGHT="25" ALIGN="MIDDLE" BORDER="0"
 SRC="img48.gif"
 ALT="$ \mu$">|; 

$key = q/{indisplay}displaystylesum_{i_1<dots<i_k}^{}{indisplay}MSF=1.4;AAT;/;
$cached_env_img{$key} = q|<IMG
 WIDTH="50" HEIGHT="46" ALIGN="MIDDLE" BORDER="0"
 SRC="img109.gif"
 ALT="$\displaystyle \sum_{i_1 < \dots < i_k}^{}$">|; 

$key = q/{indisplay}displaystyleleft.vphantom{int_0^inftyfrac{dr}{sqrt{2pi(1-q)}}e^{-frac{1}{2}frac{(r+tsqrt{q})^2}{1-q}}}right]{indisplay}MSF=1.4;AAT;/;
$cached_env_img{$key} = q|<IMG
 WIDTH="13" HEIGHT="56" ALIGN="MIDDLE" BORDER="0"
 SRC="img75.gif"
 ALT="$\displaystyle \left.\vphantom{
\int_0^\infty \frac{dr}{\sqrt{2\pi(1-q)}}
e^{-\frac{1}{2} \frac{(r+t\sqrt{q})^2}{1-q}}
}\right]$">|; 

$key = q/{indisplay}displaystyle{frac{1}{N}}{indisplay}MSF=1.4;AAT;/;
$cached_env_img{$key} = q|<IMG
 WIDTH="19" HEIGHT="44" ALIGN="MIDDLE" BORDER="0"
 SRC="img86.gif"
 ALT="$\displaystyle {\frac{1}{N}}$">|; 

$key = q/{indisplay}displaystyle{frac{sqrt{q-R^2}t-Rz}{sqrt{1-q}}}{indisplay}MSF=1.4;AAT;/;
$cached_env_img{$key} = q|<IMG
 WIDTH="101" HEIGHT="53" ALIGN="MIDDLE" BORDER="0"
 SRC="img132.gif"
 ALT="$\displaystyle {\frac{\sqrt{q-R^2}t-Rz}{\sqrt{1-q}}}$">|; 

$key = q/{inline}beta^{-1}_{}{inline}MSF=1.4;AAT;/;
$cached_env_img{$key} = q|<IMG
 WIDTH="26" HEIGHT="30" ALIGN="MIDDLE" BORDER="0"
 SRC="img124.gif"
 ALT="$ \beta^{-1}_{}$">|; 

$key = q/{indisplay}displaystyle{frac{N}{2}}{indisplay}MSF=1.4;AAT;/;
$cached_env_img{$key} = q|<IMG
 WIDTH="19" HEIGHT="45" ALIGN="MIDDLE" BORDER="0"
 SRC="img76.gif"
 ALT="$\displaystyle {\frac{N}{2}}$">|; 

$key = q/{inline}sim{inline}MSF=1.4;AAT;/;
$cached_env_img{$key} = q|<IMG
 WIDTH="14" HEIGHT="13" ALIGN="BOTTOM" BORDER="0"
 SRC="img101.gif"
 ALT="$ \sim$">|; 

$key = q/{inline}eta^{(2)}_{}{inline}MSF=1.4;AAT;/;
$cached_env_img{$key} = q|<IMG
 WIDTH="25" HEIGHT="32" ALIGN="MIDDLE" BORDER="0"
 SRC="img13.gif"
 ALT="$ \eta^{(2)}_{}$">|; 

$key = q/{inline}infty{inline}MSF=1.4;AAT;/;
$cached_env_img{$key} = q|<IMG
 WIDTH="17" HEIGHT="13" ALIGN="BOTTOM" BORDER="0"
 SRC="img17.gif"
 ALT="$ \infty$">|; 

$key = q/{indisplay}displaystylesum_{i=1}^{N}{indisplay}MSF=1.4;AAT;/;
$cached_env_img{$key} = q|<IMG
 WIDTH="23" HEIGHT="58" ALIGN="MIDDLE" BORDER="0"
 SRC="img20.gif"
 ALT="$\displaystyle \sum_{i=1}^{N}$">|; 

$key = q/{indisplay}displaystylesqrt{frac{N(N-1)}{2}}{indisplay}MSF=1.4;AAT;/;
$cached_env_img{$key} = q|<IMG
 WIDTH="80" HEIGHT="54" ALIGN="MIDDLE" BORDER="0"
 SRC="img104.gif"
 ALT="$\displaystyle \sqrt{\frac{N(N-1)}{2}}$">|; 

$key = q/{indisplay}displaystyle{frac{x}{sqrt{2}}}{indisplay}MSF=1.4;AAT;/;
$cached_env_img{$key} = q|<IMG
 WIDTH="25" HEIGHT="38" ALIGN="MIDDLE" BORDER="0"
 SRC="img100.gif"
 ALT="$\displaystyle {\frac{x}{\sqrt{2}}}$">|; 

$key = q/{indisplay}displaystyle{frac{q_2sqrt{q}(1-q)^{3slash2}}{(1-q_2)^2}}{indisplay}MSF=1.4;AAT;/;
$cached_env_img{$key} = q|<IMG
 WIDTH="96" HEIGHT="52" ALIGN="MIDDLE" BORDER="0"
 SRC="img98.gif"
 ALT="$\displaystyle {\frac{q_2\sqrt{q}(1-q)^{3/2}}{(1-q_2)^2}}$">|; 

$key = q/{figure}centerresizebox9.7cm!<comment_mark>epsfigfile=figure03.epscenter{figure}FSF=1.4;AAT;/;
$cached_env_img{$key} = q|<IMG
 WIDTH="379" HEIGHT="289"
 SRC="img158.gif"
 ALT="\begin{figure}\begin{center}
\resizebox{9.7cm}{!}%
{\epsfig{file=figure03.eps} }
\end{center}\end{figure}">|; 

$key = q/{indisplay}displaystyle{frac{(1-q)^{1slash2}}{(1-q_1)^2}}{indisplay}MSF=1.4;AAT;/;
$cached_env_img{$key} = q|<IMG
 WIDTH="65" HEIGHT="51" ALIGN="MIDDLE" BORDER="0"
 SRC="img134.gif"
 ALT="$\displaystyle {\frac{(1-q)^{1/2}}{(1-q_1)^2}}$">|; 

$key = q/{inline}Diamond{inline}MSF=1.4;AAT;/;
$cached_env_img{$key} = q|<IMG
 WIDTH="14" HEIGHT="13" ALIGN="BOTTOM" BORDER="0"
 SRC="img14.gif"
 ALT="$ \Diamond$">|; 

$key = q/{indisplay}displaystyleleft[vphantom{ln(1-q_2)+frac{q_2}{1-q_2}}right.{indisplay}MSF=1.4;AAT;/;
$cached_env_img{$key} = q|<IMG
 WIDTH="12" HEIGHT="47" ALIGN="MIDDLE" BORDER="0"
 SRC="img81.gif"
 ALT="$\displaystyle \left[\vphantom{
\ln(1-q_2) + \frac{q_2}{1-q_2} }\right.$">|; 

$key = q/{indisplay}displaystylevec{x}^{mu}_{}{indisplay}MSF=1.4;AAT;/;
$cached_env_img{$key} = q|<IMG
 WIDTH="18" HEIGHT="27" ALIGN="MIDDLE" BORDER="0"
 SRC="img45.gif"
 ALT="$\displaystyle \vec{x}^{\mu}_{}$">|; 

$key = q/{inline}{frac{N(N-1)}{2}}{inline}MSF=1.4;AAT;/;
$cached_env_img{$key} = q|<IMG
 WIDTH="49" HEIGHT="35" ALIGN="MIDDLE" BORDER="0"
 SRC="img106.gif"
 ALT="$ {\frac{N(N-1)}{2}}$">|; 

$key = q/{indisplay}displaystylesum_{i<j}^{N}{indisplay}MSF=1.4;AAT;/;
$cached_env_img{$key} = q|<IMG
 WIDTH="23" HEIGHT="58" ALIGN="MIDDLE" BORDER="0"
 SRC="img35.gif"
 ALT="$\displaystyle \sum_{i<j}^{N}$">|; 

$key = q/{indisplay}displaystyleleft[vphantom{int_0^inftyfrac{dr}{sqrt{2pi(1-q)}}e^{-frac{1}{2}frac{(r+tsqrt{q})^2}{1-q}}}right.{indisplay}MSF=1.4;AAT;/;
$cached_env_img{$key} = q|<IMG
 WIDTH="13" HEIGHT="56" ALIGN="MIDDLE" BORDER="0"
 SRC="img70.gif"
 ALT="$\displaystyle \left[\vphantom{
\int_0^\infty \frac{dr}{\sqrt{2\pi(1-q)}}
e^{-\frac{1}{2} \frac{(r+t\sqrt{q})^2}{1-q}}
}\right.$">|; 

$key = q/{indisplay}displaystylesum_{1lei_1<dots<i_kleN}^{}{indisplay}MSF=1.4;AAT;/;
$cached_env_img{$key} = q|<IMG
 WIDTH="83" HEIGHT="47" ALIGN="MIDDLE" BORDER="0"
 SRC="img30.gif"
 ALT="$\displaystyle \sum_{1 \le i_1 < \dots < i_k \le N}^{}$">|; 

$key = q/{indisplay}displaystyleleft.vphantom{frac{q_k-R_k^2}{1-q_k}+ln(1-q_k)}right]{indisplay}MSF=1.4;AAT;/;
$cached_env_img{$key} = q|<IMG
 WIDTH="12" HEIGHT="49" ALIGN="MIDDLE" BORDER="0"
 SRC="img162.gif"
 ALT="$\displaystyle \left.\vphantom{
\frac{q_k - R_k^2}{1-q_k} + \ln(1-q_k) }\right]$">|; 

$key = q/{indisplay}displaystylegamma_{2}^{}{indisplay}MSF=1.4;AAT;/;
$cached_env_img{$key} = q|<IMG
 WIDTH="16" HEIGHT="25" ALIGN="MIDDLE" BORDER="0"
 SRC="img39.gif"
 ALT="$\displaystyle \gamma_{2}^{}$">|; 

$key = q/{indisplay}displaystyle{frac{1}{n}}{indisplay}MSF=1.4;AAT;/;
$cached_env_img{$key} = q|<IMG
 WIDTH="14" HEIGHT="44" ALIGN="MIDDLE" BORDER="0"
 SRC="img61.gif"
 ALT="$\displaystyle {\frac{1}{n}}$">|; 

$key = q/{indisplay}displaystyleprod_{mu}^{}{indisplay}MSF=1.4;AAT;/;
$cached_env_img{$key} = q|<IMG
 WIDTH="21" HEIGHT="46" ALIGN="MIDDLE" BORDER="0"
 SRC="img51.gif"
 ALT="$\displaystyle \prod_{\mu}^{}$">|; 

$key = q/{inline}scriptstylemu{inline}MSF=1.4;AAT;/;
$cached_env_img{$key} = q|<IMG
 WIDTH="10" HEIGHT="25" ALIGN="MIDDLE" BORDER="0"
 SRC="img47.gif"
 ALT="$\scriptstyle \mu$">|; 

$key = q/{indisplay}displaystyle{frac{1}{sqrt{N}}}{indisplay}MSF=1.4;AAT;/;
$cached_env_img{$key} = q|<IMG
 WIDTH="30" HEIGHT="44" ALIGN="MIDDLE" BORDER="0"
 SRC="img115.gif"
 ALT="$\displaystyle {\frac{1}{\sqrt{N}}}$">|; 

$key = q/{indisplay}displaystylesum_{k=0}^{m}{indisplay}MSF=1.4;AAT;/;
$cached_env_img{$key} = q|<IMG
 WIDTH="23" HEIGHT="53" ALIGN="MIDDLE" BORDER="0"
 SRC="img27.gif"
 ALT="$\displaystyle \sum_{k=0}^{m}$">|; 

$key = q/{indisplay}scriptstylegamma_{2}{indisplay}MSF=1.4;AAT;/;
$cached_env_img{$key} = q|<IMG
 WIDTH="14" HEIGHT="25" ALIGN="MIDDLE" BORDER="0"
 SRC="img123.gif"
 ALT="$\scriptstyle \gamma_{2}$">|; 

$key = q/{indisplay}displaystylegamma_{k}^{2}{indisplay}MSF=1.4;AAT;/;
$cached_env_img{$key} = q|<IMG
 WIDTH="17" HEIGHT="31" ALIGN="MIDDLE" BORDER="0"
 SRC="img163.gif"
 ALT="$\displaystyle \gamma_{k}^{2}$">|; 

$key = q/{indisplay}displaystylesqrt{frac{1}{N}}{indisplay}MSF=1.4;AAT;/;
$cached_env_img{$key} = q|<IMG
 WIDTH="33" HEIGHT="53" ALIGN="MIDDLE" BORDER="0"
 SRC="img40.gif"
 ALT="$\displaystyle \sqrt{\frac{1}{N}}$">|; 

$key = q/{indisplay}displaystyle{frac{q_k-R_k^2}{1-q_k}}{indisplay}MSF=1.4;AAT;/;
$cached_env_img{$key} = q|<IMG
 WIDTH="54" HEIGHT="49" ALIGN="MIDDLE" BORDER="0"
 SRC="img161.gif"
 ALT="$\displaystyle {\frac{q_k - R_k^2}{1-q_k}}$">|; 

$key = q/{indisplay}displaystyleint_{0}^{infty}{indisplay}MSF=1.4;AAT;/;
$cached_env_img{$key} = q|<IMG
 WIDTH="29" HEIGHT="46" ALIGN="MIDDLE" BORDER="0"
 SRC="img71.gif"
 ALT="$\displaystyle \int_{0}^{\infty}$">|; 

$key = q/{indisplay}displaystylegamma_{1}^{t}{indisplay}MSF=1.4;AAT;/;
$cached_env_img{$key} = q|<IMG
 WIDTH="16" HEIGHT="30" ALIGN="MIDDLE" BORDER="0"
 SRC="img113.gif"
 ALT="$\displaystyle \gamma_{1}^{t}$">|; 

$key = q/{inline}rangle{inline}MSF=1.4;AAT;/;
$cached_env_img{$key} = q|<IMG
 WIDTH="8" HEIGHT="28" ALIGN="MIDDLE" BORDER="0"
 SRC="img65.gif"
 ALT="$ \rangle$">|; 

$key = q/{indisplay}displaystyleleft.vphantom{frac{q_1-R_1^2}{1-q_1}+ln(1-q_1)}right]{indisplay}MSF=1.4;AAT;/;
$cached_env_img{$key} = q|<IMG
 WIDTH="12" HEIGHT="49" ALIGN="MIDDLE" BORDER="0"
 SRC="img127.gif"
 ALT="$\displaystyle \left.\vphantom{
\frac{q_1 - R_1^2}{1-q_1} + \ln(1-q_1) }\right]$">|; 

$key = q/{indisplay}displaystylevec{x},{indisplay}MSF=1.4;AAT;/;
$cached_env_img{$key} = q|<IMG
 WIDTH="13" HEIGHT="14" ALIGN="BOTTOM" BORDER="0"
 SRC="img37.gif"
 ALT="$\displaystyle \vec{x}\,$">|; 

$key = q/{indisplay}displaystyle{frac{tsqrt{q}}{sqrt{1-q}}}{indisplay}MSF=1.4;AAT;/;
$cached_env_img{$key} = q|<IMG
 WIDTH="49" HEIGHT="47" ALIGN="MIDDLE" BORDER="0"
 SRC="img94.gif"
 ALT="$\displaystyle {\frac{t\sqrt{q}}{\sqrt{1-q}}}$">|; 

$key = q/{indisplay}displaystyleleft.vphantom{frac{q_2-R_2^2}{1-q_2}+ln(1-q_2)}right]{indisplay}MSF=1.4;AAT;/;
$cached_env_img{$key} = q|<IMG
 WIDTH="12" HEIGHT="49" ALIGN="MIDDLE" BORDER="0"
 SRC="img130.gif"
 ALT="$\displaystyle \left.\vphantom{
\frac{q_2 - R_2^2}{1-q_2} + \ln(1-q_2) }\right]$">|; 

$key = q/{indisplay}displaystyle{frac{gamma_2^t}{gamma_2}}{indisplay}MSF=1.4;AAT;/;
$cached_env_img{$key} = q|<IMG
 WIDTH="20" HEIGHT="48" ALIGN="MIDDLE" BORDER="0"
 SRC="img150.gif"
 ALT="$\displaystyle {\frac{\gamma_2^t}{\gamma_2}}$">|; 

$key = q/{indisplay}displaystyleleft.vphantom{e^{-beta}+(1-e^{-beta})H(frac{sqrt{q-R^2}t-Rz}{sqrt{1-q}})}right]{indisplay}MSF=1.4;AAT;/;
$cached_env_img{$key} = q|<IMG
 WIDTH="13" HEIGHT="56" ALIGN="MIDDLE" BORDER="0"
 SRC="img133.gif"
 ALT="$\displaystyle \left.\vphantom{
e^{-\beta} + (1-e^{-\beta})H(\frac{\sqrt{q-R^2}t-Rz}{\sqrt{1-q}})
}\right]$">|; 

$key = q/{indisplay}displaystyle{frac{q_1-R_1^2}{1-q_1}}{indisplay}MSF=1.4;AAT;/;
$cached_env_img{$key} = q|<IMG
 WIDTH="53" HEIGHT="49" ALIGN="MIDDLE" BORDER="0"
 SRC="img126.gif"
 ALT="$\displaystyle {\frac{q_1 - R_1^2}{1-q_1}}$">|; 

$key = q/{indisplay}displaystyle{frac{d_k}{2}}{indisplay}MSF=1.4;AAT;/;
$cached_env_img{$key} = q|<IMG
 WIDTH="20" HEIGHT="45" ALIGN="MIDDLE" BORDER="0"
 SRC="img159.gif"
 ALT="$\displaystyle {\frac{d_k}{2}}$">|; 

$key = q/{indisplay}displaystyleleft[vphantom{gamma_1sqrt{N}+gamma_2sqrt{frac{N(N-1)}{2}}}right.{indisplay}MSF=1.4;AAT;/;
$cached_env_img{$key} = q|<IMG
 WIDTH="13" HEIGHT="56" ALIGN="MIDDLE" BORDER="0"
 SRC="img102.gif"
 ALT="$\displaystyle \left[\vphantom{\gamma_1 \sqrt{N} +\gamma_2 \sqrt{\frac{N(N-1)}{2}}
}\right.$">|; 

$key = q/{indisplay}displaystyleleft.vphantom{gamma_1sqrt{N}+gamma_2sqrt{frac{N(N-1)}{2}}}right]^{2}_{}{indisplay}MSF=1.4;AAT;/;
$cached_env_img{$key} = q|<IMG
 WIDTH="19" HEIGHT="62" ALIGN="MIDDLE" BORDER="0"
 SRC="img105.gif"
 ALT="$\displaystyle \left.\vphantom{\gamma_1 \sqrt{N} +\gamma_2 \sqrt{\frac{N(N-1)}{2}}
}\right]^{2}_{}$">|; 

$key = q/{inline}{groupN(N-1)groupover2}{inline}MSF=1.4;AAT;/;
$cached_env_img{$key} = q|<IMG
 WIDTH="49" HEIGHT="35" ALIGN="MIDDLE" BORDER="0"
 SRC="img1.gif"
 ALT="$ {\begingroup N(N-1)\endgroup \over 2}$">|; 

$key = q/{indisplay}displaystyleeta^{(1)}_{}{indisplay}MSF=1.4;AAT;/;
$cached_env_img{$key} = q|<IMG
 WIDTH="25" HEIGHT="33" ALIGN="MIDDLE" BORDER="0"
 SRC="img156.gif"
 ALT="$\displaystyle \eta^{(1)}_{}$">|; 

$key = q/{indisplay}displaystyleprod_{i}^{}{indisplay}MSF=1.4;AAT;/;
$cached_env_img{$key} = q|<IMG
 WIDTH="21" HEIGHT="45" ALIGN="MIDDLE" BORDER="0"
 SRC="img53.gif"
 ALT="$\displaystyle \prod_{i}^{}$">|; 

$key = q/{indisplay}displaystylesum_{0lei_1ledotslei_mleN}^{}{indisplay}MSF=1.4;AAT;/;
$cached_env_img{$key} = q|<IMG
 WIDTH="86" HEIGHT="47" ALIGN="MIDDLE" BORDER="0"
 SRC="img19.gif"
 ALT="$\displaystyle \sum_{0\le i_1 \le \dots \le i_m \le N}^{}$">|; 

$key = q/{inline}le{inline}MSF=1.4;AAT;/;
$cached_env_img{$key} = q|<IMG
 WIDTH="14" HEIGHT="25" ALIGN="MIDDLE" BORDER="0"
 SRC="img23.gif"
 ALT="$ \le$">|; 

$key = q/{indisplay}displaystyle{frac{q_1sqrt{q}(1-q)^{3slash2}}{(1-q_1)^2}}{indisplay}MSF=1.4;AAT;/;
$cached_env_img{$key} = q|<IMG
 WIDTH="96" HEIGHT="52" ALIGN="MIDDLE" BORDER="0"
 SRC="img96.gif"
 ALT="$\displaystyle {\frac{q_1\sqrt{q}(1-q)^{3/2}}{(1-q_1)^2}}$">|; 

$key = q/{inline}gamma^{t}_{2}{inline}MSF=1.4;AAT;/;
$cached_env_img{$key} = q|<IMG
 WIDTH="16" HEIGHT="29" ALIGN="MIDDLE" BORDER="0"
 SRC="img121.gif"
 ALT="$ \gamma^{t}_{2}$">|; 

$key = q/{figure}centerresizebox9.7cm!<comment_mark>epsfigfile=figure02.epscenter{figure}FSF=1.4;AAT;/;
$cached_env_img{$key} = q|<IMG
 WIDTH="378" HEIGHT="304"
 SRC="img145.gif"
 ALT="\begin{figure}\begin{center}
\resizebox{9.7cm}{!}%
{\epsfig{file=figure02.eps} }
\end{center}\end{figure}">|; 

$key = q/{indisplay}displaystyle{frac{1}{(1-q)}}{indisplay}MSF=1.4;AAT;/;
$cached_env_img{$key} = q|<IMG
 WIDTH="48" HEIGHT="44" ALIGN="MIDDLE" BORDER="0"
 SRC="img146.gif"
 ALT="$\displaystyle {\frac{1}{(1-q)}}$">|; 

$key = q/{inline}beta{inline}MSF=1.4;AAT;/;
$cached_env_img{$key} = q|<IMG
 WIDTH="11" HEIGHT="26" ALIGN="MIDDLE" BORDER="0"
 SRC="img5.gif"
 ALT="$ \beta$">|; 

$key = q/{indisplay}scriptstyle{textstylefrac{1}{2}}{indisplay}MSF=1.4;AAT;/;
$cached_env_img{$key} = q|<IMG
 WIDTH="12" HEIGHT="30" ALIGN="MIDDLE" BORDER="0"
 SRC="img73.gif"
 ALT="$\scriptstyle {\textstyle\frac{1}{2}}$">|; 

$key = q/{inline}{frac{N(N+1)}{2}}{inline}MSF=1.4;AAT;/;
$cached_env_img{$key} = q|<IMG
 WIDTH="49" HEIGHT="35" ALIGN="MIDDLE" BORDER="0"
 SRC="img107.gif"
 ALT="$ {\frac{N(N+1)}{2}}$">|; 

$key = q/{indisplay}displaystyle{textstylefrac{1}{2}}{indisplay}MSF=1.4;AAT;/;
$cached_env_img{$key} = q|<IMG
 WIDTH="12" HEIGHT="30" ALIGN="MIDDLE" BORDER="0"
 SRC="img66.gif"
 ALT="$\displaystyle {\textstyle\frac{1}{2}}$">|; 

$key = q/{inline}gamma_{1}^{}{inline}MSF=1.4;AAT;/;
$cached_env_img{$key} = q|<IMG
 WIDTH="16" HEIGHT="25" ALIGN="MIDDLE" BORDER="0"
 SRC="img3.gif"
 ALT="$ \gamma_{1}^{}$">|; 

$key = q/{indisplay}displaystyleleft[vphantom{frac{q_k-R_k^2}{1-q_k}+ln(1-q_k)}right.{indisplay}MSF=1.4;AAT;/;
$cached_env_img{$key} = q|<IMG
 WIDTH="12" HEIGHT="49" ALIGN="MIDDLE" BORDER="0"
 SRC="img160.gif"
 ALT="$\displaystyle \left[\vphantom{
\frac{q_k - R_k^2}{1-q_k} + \ln(1-q_k) }\right.$">|; 

$key = q/{inline}gamma_{2}^{}{inline}MSF=1.4;AAT;/;
$cached_env_img{$key} = q|<IMG
 WIDTH="16" HEIGHT="25" ALIGN="MIDDLE" BORDER="0"
 SRC="img4.gif"
 ALT="$ \gamma_{2}^{}$">|; 

$key = q/{inline}gamma{inline}MSF=1.4;AAT;/;
$cached_env_img{$key} = q|<IMG
 WIDTH="11" HEIGHT="25" ALIGN="MIDDLE" BORDER="0"
 SRC="img116.gif"
 ALT="$ \gamma$">|; 

$key = q/{indisplay}displaystyle{frac{gamma_k}{sqrt{d_k}}}{indisplay}MSF=1.4;AAT;/;
$cached_env_img{$key} = q|<IMG
 WIDTH="32" HEIGHT="38" ALIGN="MIDDLE" BORDER="0"
 SRC="img29.gif"
 ALT="$\displaystyle {\frac{\gamma_k}{\sqrt{d_k}}}$">|; 

$key = q/{indisplay}displaystyle{array}{c}Nk{array}{indisplay}MSF=1.4;AAT;/;
$cached_env_img{$key} = q|<IMG
 WIDTH="29" HEIGHT="47" ALIGN="MIDDLE" BORDER="0"
 SRC="img25.gif"
 ALT="$\displaystyle \begin{array}{c}
N \\\\
k
\end{array}$">|; 

$key = q/{indisplay}displaystylesum_{mu=1}^{p}{indisplay}MSF=1.4;AAT;/;
$cached_env_img{$key} = q|<IMG
 WIDTH="24" HEIGHT="54" ALIGN="MIDDLE" BORDER="0"
 SRC="img119.gif"
 ALT="$\displaystyle \sum_{\mu=1}^{p}$">|; 

$key = q/{indisplay}scriptstyle{frac{q}{1-q}}{indisplay}MSF=1.4;AAT;/;
$cached_env_img{$key} = q|<IMG
 WIDTH="23" HEIGHT="25" ALIGN="MIDDLE" BORDER="0"
 SRC="img95.gif"
 ALT="$\scriptstyle {\frac{q}{1-q}}$">|; 

$key = q/{indisplay}displaystyle{frac{pgamma_2^2}{sqrt{2pi}}}{indisplay}MSF=1.4;AAT;/;
$cached_env_img{$key} = q|<IMG
 WIDTH="33" HEIGHT="49" ALIGN="MIDDLE" BORDER="0"
 SRC="img97.gif"
 ALT="$\displaystyle {\frac{p\gamma_2^2}{\sqrt{2\pi}}}$">|; 

$key = q/{indisplay}displaystyle{frac{1}{p}}{indisplay}MSF=1.4;AAT;/;
$cached_env_img{$key} = q|<IMG
 WIDTH="13" HEIGHT="44" ALIGN="MIDDLE" BORDER="0"
 SRC="img118.gif"
 ALT="$\displaystyle {\frac{1}{p}}$">|; 

$key = q/{inline}Box{inline}MSF=1.4;AAT;/;
$cached_env_img{$key} = q|<IMG
 WIDTH="13" HEIGHT="13" ALIGN="BOTTOM" BORDER="0"
 SRC="img15.gif"
 ALT="$ \Box$">|; 

$key = q/{indisplay}displaystyle{frac{q_2-R_2^2}{1-q_2}}{indisplay}MSF=1.4;AAT;/;
$cached_env_img{$key} = q|<IMG
 WIDTH="53" HEIGHT="49" ALIGN="MIDDLE" BORDER="0"
 SRC="img129.gif"
 ALT="$\displaystyle {\frac{q_2 - R_2^2}{1-q_2}}$">|; 

$key = q/{indisplay}displaystyle{frac{dr}{sqrt{2pi(1-q)}}}{indisplay}MSF=1.4;AAT;/;
$cached_env_img{$key} = q|<IMG
 WIDTH="77" HEIGHT="45" ALIGN="MIDDLE" BORDER="0"
 SRC="img72.gif"
 ALT="$\displaystyle {\frac{dr}{\sqrt{2\pi(1-q)}}}$">|; 

$key = q/{indisplay}displaystyle{frac{e^{-frac{1}{2}s^2}t}{(e^beta-1)^{-1}+H(s)}}{indisplay}MSF=1.4;AAT;/;
$cached_env_img{$key} = q|<IMG
 WIDTH="117" HEIGHT="53" ALIGN="MIDDLE" BORDER="0"
 SRC="img138.gif"
 ALT="$\displaystyle {\frac{e^{-\frac{1}{2}s^2}t}{(e^\beta -1)^{-1} + H(s)}}$">|; 

$key = q/{indisplay}displaystylesum_{i}^{}{indisplay}MSF=1.4;AAT;/;
$cached_env_img{$key} = q|<IMG
 WIDTH="23" HEIGHT="45" ALIGN="MIDDLE" BORDER="0"
 SRC="img55.gif"
 ALT="$\displaystyle \sum_{i}^{}$">|; 

$key = q/{indisplay}displaystylegamma_{k}^{t}{indisplay}MSF=1.4;AAT;/;
$cached_env_img{$key} = q|<IMG
 WIDTH="17" HEIGHT="30" ALIGN="MIDDLE" BORDER="0"
 SRC="img164.gif"
 ALT="$\displaystyle \gamma_{k}^{t}$">|; 

$key = q/{indisplay}displaystyleprod_{sigma=1}^{n}{indisplay}MSF=1.4;AAT;/;
$cached_env_img{$key} = q|<IMG
 WIDTH="24" HEIGHT="53" ALIGN="MIDDLE" BORDER="0"
 SRC="img62.gif"
 ALT="$\displaystyle \prod_{\sigma=1}^{n}$">|; 

$key = q/{inline}langle{inline}MSF=1.4;AAT;/;
$cached_env_img{$key} = q|<IMG
 WIDTH="8" HEIGHT="28" ALIGN="MIDDLE" BORDER="0"
 SRC="img64.gif"
 ALT="$ \langle$">|; 

$key = q/{indisplay}displaystylegamma_{2}^{2}{indisplay}MSF=1.4;AAT;/;
$cached_env_img{$key} = q|<IMG
 WIDTH="17" HEIGHT="31" ALIGN="MIDDLE" BORDER="0"
 SRC="img85.gif"
 ALT="$\displaystyle \gamma_{2}^{2}$">|; 

$key = q/{inline}mathgroup{inline}MSF=1.4;AAT;/;
$cached_env_img{$key} = q|<IMG
 WIDTH="5" HEIGHT="15" ALIGN="BOTTOM" BORDER="0"
 SRC="img7.gif"
 ALT="$ \mathgroup$">|; 

$key = q/{inline}symoperators{inline}MSF=1.4;AAT;/;
$cached_env_img{$key} = q|<IMG
 WIDTH="11" HEIGHT="13" ALIGN="BOTTOM" BORDER="0"
 SRC="img8.gif"
 ALT="$ \symoperators$">|; 

$key = q/{indisplay}displaystylelim_{nrightarrow0}^{}{indisplay}MSF=1.4;AAT;/;
$cached_env_img{$key} = q|<IMG
 WIDTH="26" HEIGHT="35" ALIGN="MIDDLE" BORDER="0"
 SRC="img60.gif"
 ALT="$\displaystyle \lim_{n\rightarrow 0}^{}$">|; 

$key = q/{indisplay}displaystyleleft[vphantom{frac{q_1-R_1^2}{1-q_1}+ln(1-q_1)}right.{indisplay}MSF=1.4;AAT;/;
$cached_env_img{$key} = q|<IMG
 WIDTH="12" HEIGHT="49" ALIGN="MIDDLE" BORDER="0"
 SRC="img125.gif"
 ALT="$\displaystyle \left[\vphantom{
\frac{q_1 - R_1^2}{1-q_1} + \ln(1-q_1) }\right.$">|; 

$key = q/{inline}nolimits^{-1}_{}{inline}MSF=1.4;AAT;/;
$cached_env_img{$key} = q|<IMG
 WIDTH="18" HEIGHT="30" ALIGN="MIDDLE" BORDER="0"
 SRC="img9.gif"
 ALT="$ \nolimits^{-1}_{}$">|; 

$key = q/{indisplay}displaystyleleft[vphantom{ln(1-q_1)+frac{q_1}{1-q_1}}right.{indisplay}MSF=1.4;AAT;/;
$cached_env_img{$key} = q|<IMG
 WIDTH="12" HEIGHT="47" ALIGN="MIDDLE" BORDER="0"
 SRC="img77.gif"
 ALT="$\displaystyle \left[\vphantom{
\ln(1-q_1) + \frac{q_1}{1-q_1} }\right.$">|; 

$key = q/{indisplay}displaystyleint_{x}^{infty}{indisplay}MSF=1.4;AAT;/;
$cached_env_img{$key} = q|<IMG
 WIDTH="29" HEIGHT="46" ALIGN="MIDDLE" BORDER="0"
 SRC="img99.gif"
 ALT="$\displaystyle \int_{x}^{\infty}$">|; 

$key = q/{indisplay}displaystyleleft[vphantom{frac{q_2-R_2^2}{1-q_2}+ln(1-q_2)}right.{indisplay}MSF=1.4;AAT;/;
$cached_env_img{$key} = q|<IMG
 WIDTH="12" HEIGHT="49" ALIGN="MIDDLE" BORDER="0"
 SRC="img128.gif"
 ALT="$\displaystyle \left[\vphantom{
\frac{q_2 - R_2^2}{1-q_2} + \ln(1-q_2) }\right.$">|; 

$key = q/{inline}delta_{x0}^{}{inline}MSF=1.4;AAT;/;
$cached_env_img{$key} = q|<IMG
 WIDTH="21" HEIGHT="26" ALIGN="MIDDLE" BORDER="0"
 SRC="img69.gif"
 ALT="$ \delta_{x0}^{}$">|; 

$key = q/{indisplay}displaystyle{frac{2}{N(N-1)}}{indisplay}MSF=1.4;AAT;/;
$cached_env_img{$key} = q|<IMG
 WIDTH="66" HEIGHT="44" ALIGN="MIDDLE" BORDER="0"
 SRC="img88.gif"
 ALT="$\displaystyle {\frac{2}{N(N-1)}}$">|; 

$key = q/{indisplay}displaystylemu{indisplay}MSF=1.4;AAT;/;
$cached_env_img{$key} = q|<IMG
 WIDTH="11" HEIGHT="25" ALIGN="MIDDLE" BORDER="0"
 SRC="img50.gif"
 ALT="$\displaystyle \mu$">|; 

$key = q/{indisplay}displaystyleleft[vphantom{sum_{k=0}^mgamma_ksqrt{d_k}}right.{indisplay}MSF=1.4;AAT;/;
$cached_env_img{$key} = q|<IMG
 WIDTH="13" HEIGHT="56" ALIGN="MIDDLE" BORDER="0"
 SRC="img110.gif"
 ALT="$\displaystyle \left[\vphantom{\sum_{k=0}^m \gamma_k \sqrt{d_k} }\right.$">|; 

$key = q/{indisplay}displaystyle{frac{1}{d_k}}{indisplay}MSF=1.4;AAT;/;
$cached_env_img{$key} = q|<IMG
 WIDTH="20" HEIGHT="44" ALIGN="MIDDLE" BORDER="0"
 SRC="img165.gif"
 ALT="$\displaystyle {\frac{1}{d_k}}$">|; 

$key = q/{inline}delta{inline}MSF=1.4;AAT;/;
$cached_env_img{$key} = q|<IMG
 WIDTH="9" HEIGHT="13" ALIGN="BOTTOM" BORDER="0"
 SRC="img67.gif"
 ALT="$ \delta$">|; 

$key = q/{inline}equiv{inline}MSF=1.4;AAT;/;
$cached_env_img{$key} = q|<IMG
 WIDTH="14" HEIGHT="13" ALIGN="BOTTOM" BORDER="0"
 SRC="img68.gif"
 ALT="$ \equiv$">|; 

$key = q/{indisplay}displaystyle{frac{q_1}{q_2}}{indisplay}MSF=1.4;AAT;/;
$cached_env_img{$key} = q|<IMG
 WIDTH="19" HEIGHT="38" ALIGN="MIDDLE" BORDER="0"
 SRC="img151.gif"
 ALT="$\displaystyle {\frac{q_1}{q_2}}$">|; 

$key = q/{indisplay}displaystyle{frac{1-R^2}{(q-R^2)^{1slash2}}}{indisplay}MSF=1.4;AAT;/;
$cached_env_img{$key} = q|<IMG
 WIDTH="75" HEIGHT="49" ALIGN="MIDDLE" BORDER="0"
 SRC="img147.gif"
 ALT="$\displaystyle {\frac{1-R^2}{(q-R^2)^{1/2}}}$">|; 

$key = q/{inline}ne{inline}MSF=1.4;AAT;/;
$cached_env_img{$key} = q|<IMG
 WIDTH="14" HEIGHT="26" ALIGN="MIDDLE" BORDER="0"
 SRC="img90.gif"
 ALT="$ \ne$">|; 

$key = q/{figure}centerresizebox9.7cm!<comment_mark>epsfigfile=figure01.epscenter{figure}FSF=1.4;AAT;/;
$cached_env_img{$key} = q|<IMG
 WIDTH="378" HEIGHT="426"
 SRC="img36.gif"
 ALT="\begin{figure}\begin{center}
\resizebox{9.7cm}{!}%
{\epsfig{file=figure01.eps} }
\end{center}\end{figure}">|; 

$key = q/{inline}gamma_{i}^{}{inline}MSF=1.4;AAT;/;
$cached_env_img{$key} = q|<IMG
 WIDTH="15" HEIGHT="25" ALIGN="MIDDLE" BORDER="0"
 SRC="img28.gif"
 ALT="$ \gamma_{i}^{}$">|; 

$key = q/{inline}approx{inline}MSF=1.4;AAT;/;
$cached_env_img{$key} = q|<IMG
 WIDTH="14" HEIGHT="13" ALIGN="BOTTOM" BORDER="0"
 SRC="img11.gif"
 ALT="$ \approx$">|; 

$key = q/{inline}vec{x},{inline}MSF=1.4;AAT;/;
$cached_env_img{$key} = q|<IMG
 WIDTH="13" HEIGHT="14" ALIGN="BOTTOM" BORDER="0"
 SRC="img18.gif"
 ALT="$ \vec{x}\,$">|; 

$key = q/{inline}gamma_{k}^{}{inline}MSF=1.4;AAT;/;
$cached_env_img{$key} = q|<IMG
 WIDTH="17" HEIGHT="25" ALIGN="MIDDLE" BORDER="0"
 SRC="img32.gif"
 ALT="$ \gamma_{k}^{}$">|; 

$key = q/{indisplay}displaystyle{frac{(1-q)^{1slash2}}{(1-q_2)^2}}{indisplay}MSF=1.4;AAT;/;
$cached_env_img{$key} = q|<IMG
 WIDTH="65" HEIGHT="51" ALIGN="MIDDLE" BORDER="0"
 SRC="img135.gif"
 ALT="$\displaystyle {\frac{(1-q)^{1/2}}{(1-q_2)^2}}$">|; 

$key = q/{indisplay}displaystyle{frac{gamma_1^t}{gamma_1}}{indisplay}MSF=1.4;AAT;/;
$cached_env_img{$key} = q|<IMG
 WIDTH="20" HEIGHT="48" ALIGN="MIDDLE" BORDER="0"
 SRC="img149.gif"
 ALT="$\displaystyle {\frac{\gamma_1^t}{\gamma_1}}$">|; 

$key = q/{indisplay}displaystyleleft.vphantom{ln(1-q_2)+frac{q_2}{1-q_2}}right]{indisplay}MSF=1.4;AAT;/;
$cached_env_img{$key} = q|<IMG
 WIDTH="12" HEIGHT="47" ALIGN="MIDDLE" BORDER="0"
 SRC="img83.gif"
 ALT="$\displaystyle \left.\vphantom{
\ln(1-q_2) + \frac{q_2}{1-q_2} }\right]$">|; 

$key = q/{inline}nu{inline}MSF=1.4;AAT;/;
$cached_env_img{$key} = q|<IMG
 WIDTH="11" HEIGHT="13" ALIGN="BOTTOM" BORDER="0"
 SRC="img91.gif"
 ALT="$ \nu$">|; 

$key = q/{indisplay}displaystyle{frac{N(N-1)}{2}}{indisplay}MSF=1.4;AAT;/;
$cached_env_img{$key} = q|<IMG
 WIDTH="66" HEIGHT="47" ALIGN="MIDDLE" BORDER="0"
 SRC="img43.gif"
 ALT="$\displaystyle {\frac{N(N-1)}{2}}$">|; 

$key = q/{indisplay}displaystyle{frac{1}{sqrt{2pi}}}{indisplay}MSF=1.4;AAT;/;
$cached_env_img{$key} = q|<IMG
 WIDTH="33" HEIGHT="44" ALIGN="MIDDLE" BORDER="0"
 SRC="img92.gif"
 ALT="$\displaystyle {\frac{1}{\sqrt{2\pi}}}$">|; 

$key = q/{indisplay}displaystylesum_{1lei_1<dots<i_mleN}^{}{indisplay}MSF=1.4;AAT;/;
$cached_env_img{$key} = q|<IMG
 WIDTH="86" HEIGHT="47" ALIGN="MIDDLE" BORDER="0"
 SRC="img21.gif"
 ALT="$\displaystyle \sum_{1 \le i_1 < \dots < i_m \le N}^{}$">|; 

$key = q/{indisplay}displaystyleleft.vphantom{sum_{k=0}^mgamma_ksqrt{d_k}}right]^{2}_{}{indisplay}MSF=1.4;AAT;/;
$cached_env_img{$key} = q|<IMG
 WIDTH="19" HEIGHT="62" ALIGN="MIDDLE" BORDER="0"
 SRC="img112.gif"
 ALT="$\displaystyle \left.\vphantom{\sum_{k=0}^m \gamma_k \sqrt{d_k} }\right]^{2}_{}$">|; 

$key = q/{indisplay}displaystyle{frac{pgamma_1^2}{sqrt{2pi}}}{indisplay}MSF=1.4;AAT;/;
$cached_env_img{$key} = q|<IMG
 WIDTH="33" HEIGHT="49" ALIGN="MIDDLE" BORDER="0"
 SRC="img93.gif"
 ALT="$\displaystyle {\frac{p\gamma_1^2}{\sqrt{2\pi}}}$">|; 

$key = q/{indisplay}displaystyle{frac{q_1}{1-q_1}}{indisplay}MSF=1.4;AAT;/;
$cached_env_img{$key} = q|<IMG
 WIDTH="43" HEIGHT="38" ALIGN="MIDDLE" BORDER="0"
 SRC="img78.gif"
 ALT="$\displaystyle {\frac{q_1}{1-q_1}}$">|; 

$key = q/{indisplay}displaystyle{frac{R_1}{R_2}}{indisplay}MSF=1.4;AAT;/;
$cached_env_img{$key} = q|<IMG
 WIDTH="23" HEIGHT="45" ALIGN="MIDDLE" BORDER="0"
 SRC="img152.gif"
 ALT="$\displaystyle {\frac{R_1}{R_2}}$">|; 

$key = q/{indisplay}displaystyleint{indisplay}MSF=1.4;AAT;/;
$cached_env_img{$key} = q|<IMG
 WIDTH="17" HEIGHT="45" ALIGN="MIDDLE" BORDER="0"
 SRC="img49.gif"
 ALT="$\displaystyle \int$">|; 

1;

