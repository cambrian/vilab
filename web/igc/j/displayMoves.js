/************************************************************
 *  displayMoves.js
 *  Copyright 1999, Hyoungsoo Yoon. All Rights Reserved.
 *  This software is distributed on an "AS IS" basis, 
 *  WITHOUT WARRANTY OF ANY KIND, either express or implied.
 ************************************************************/


var milliSec = 1000;
var currentMove = 0;

function addMoves()
{
  for(i=0;i<ma.length;i++) {
     var pos = "b_" + ma[i].x + "_" + ma[i].y;
     var posImg = document.images[pos];
     if(ma[i].c == "B") {
         posImg.src = B.src;
     } else {
         posImg.src = W.src;
     }
  }
}

function playMoves()
{
  if (currentMove != mo.length) {
     moveForward();
     window.setTimeout ("playMoves();",milliSec);
  }
}

function moveForward()
{
  if (currentMove != mo.length) {
     var i =  mo[currentMove].x;
     var j =  mo[currentMove].y;
     if(i>=0 && j>=0) {
       var pos = "b_" + i + "_" + j;
       var posImg = document.images[pos];
       if(mo[currentMove].c == "B") {
           posImg.src = B.src;
       } else if(mo[currentMove].c == "W") {
           posImg.src = W.src;
       } else if(mo[currentMove].c == "E") {
           posImg.src = "g/" + board[i][j] + ".gif";
       } //else do nothing.
     } //else Pass
     currentMove++;
     if(currentMove < mo.length && mo[currentMove].c == "E") {
        moveForward();
     }
  }
}

function moveBackward()
{
  if (currentMove != 0) {
     currentMove--;
     var i =  mo[currentMove].x;
     var j =  mo[currentMove].y;
     if(i>=0 && j>=0) {
       var pos = "b_" + i + "_" + j;
       document.images[pos].src = "g/" + board[i][j] + ".gif";
       if(mo[currentMove].c == "E") {
         for(z=currentMove-1;z>=0;z--) {
            if(i == mo[z].x && j == mo[z].y) {
               if(mo[z].c == "B") {
                 document.images[pos].src = B.src;
               } else if(mo[z].c == "W") {
                 document.images[pos].src = W.src;
               }
               break;
            }
         }
         moveBackward();
       }
     } // else Pass
  }
}

function moveToEnd()
{
  while (currentMove != mo.length) {
     moveForward();
  }
}

function moveToBeginning()
{
  while (currentMove != 0) {
     moveBackward();
  }
}
