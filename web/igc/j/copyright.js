/************************************************************
 *  copyright.js
 *  Copyright 1999, Hyoungsoo Yoon. All Rights Reserved.
 *  This software is distributed on an "AS IS" basis, 
 *  WITHOUT WARRANTY OF ANY KIND, either express or implied.
 ************************************************************/

document.write('<DIV class="copyright" style="text-align: center;">');
document.write('Copyright&nbsp;<SPAN class="forNS">&copy;</SPAN>&nbsp;1999,&nbsp;&nbsp;');
document.write('<A href="HTTP://www.vilab.com/igc">BlueCraft&nbsp;Inc.</A>&nbsp;&nbsp;');
document.write('All&nbsp;Rights&nbsp;Reserved.<BR>');
document.write('Last Modified:  ' + document.lastModified + ' UTC');
document.write('</DIV>');
