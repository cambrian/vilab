/************************************************************
 *  displayGoBoardWsh.js
 *  Copyright 1999, Hyoungsoo Yoon. All Rights Reserved.
 *  This software is distributed on an "AS IS" basis, 
 *  WITHOUT WARRANTY OF ANY KIND, either express or implied.
 ************************************************************/

// Global variables
var mSGF = "";
var boardColor = "gold";
var backgroundColor = "ivory";
var commentsHolder = "";

var fso, hFile;
var filePath = "";
var fileTitle = "";
var fileHeader = "";
function CreateHtmlFile()
{
  fso = new ActiveXObject("Scripting.FileSystemObject");
  hFile = fso.CreateTextFile(filePath, true);
  hFile.WriteLine('<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0//EN">');
  hFile.WriteLine('<HTML lang="en">') ;
  hFile.WriteLine('<HEAD>') ;
  hFile.WriteLine('<META http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">');
  hFile.WriteLine('<TITLE>') ;
  hFile.WriteLine(fileTitle) ;
  hFile.WriteLine('</TITLE>') ;

  // Write style tags here.
  hFile.WriteLine('<STYLE TYPE="text/css">');
  hFile.Write('BODY { ');
  hFile.Write('background: ' + backgroundColor + '; ');
  hFile.WriteLine('}');
  hFile.Write('TABLE.goboard { ');
  hFile.Write('margin: 0px; ');
  hFile.Write('padding: 0.0in; ');
  hFile.Write('border: 0.0in ; ');
  hFile.Write('background: ' + boardColor + '; ');
  hFile.WriteLine('}');
  hFile.Write('TABLE.goboard TR { ');
  hFile.Write('margin: 0in; ');
  hFile.Write('padding: 0in; ');
  hFile.Write('border: 0in; ');
  hFile.WriteLine('}');
  hFile.Write('TABLE.goboard TD { ');
  hFile.Write('margin: 0in; ');
  hFile.Write('padding: 0in; ');
  hFile.WriteLine('}');
  hFile.Write('DIV.arrows { ');
  hFile.Write('text-align: center; ');
  hFile.WriteLine('}');
  hFile.Write('DIV.comments { ');
  hFile.Write('text-align: left; ');
  hFile.Write('margin-left: 0.2in; ');
  hFile.WriteLine('}');
  hFile.WriteLine('</STYLE>');

  // Browser sniffer
  hFile.WriteLine('<SCRIPT type="text/javascript" language="JavaScript">');
  hFile.WriteLine('  var isNav4, isIE4;');
  hFile.WriteLine('  if (parseInt(navigator.appVersion.charAt(0)) >= 4) {');
  hFile.WriteLine('     if (navigator.appName == "Netscape") {');
  hFile.WriteLine('	isNav4 = true;');
  hFile.WriteLine('     } else if (navigator.appVersion.indexOf("MSIE") != -1) {');
  hFile.WriteLine('	isIE4 = true;');
  hFile.WriteLine('     }');
  hFile.WriteLine('  }');
  hFile.WriteLine('</SCR' + 'IPT>');
  // Go board
  hFile.WriteLine('<SCRIPT type="text/javascript" language="JavaScript"');
  hFile.WriteLine('src="goBoard.js">');
  hFile.WriteLine('</SCR' + 'IPT>');
  hFile.WriteLine('<SCRIPT type="text/javascript" language="JavaScript"');
  hFile.WriteLine('src="boardImages.js">');
  hFile.WriteLine('</SCR' + 'IPT>');

  // Move functions
  hFile.WriteLine('<SCRIPT type="text/javascript" language="JavaScript"');
  hFile.WriteLine('src="displayMoves.js">');
  hFile.WriteLine('</SCR' + 'IPT>');

  hFile.WriteLine('</HEAD>') ;
  hFile.WriteLine('<BODY>') ;
}

function CloseHtmlFile()
{
  hFile.WriteLine('</BODY>');
  hFile.WriteLine('</HTML>');
  hFile.Close();
}

// Default (empty) board
var board = new Array();
for(k=0;k<19;k++) {
  board[k] = new Array("2", "5", "5", "5", "5", "5", "5", "5", "5",
             "5", "5", "5", "5", "5", "5", "5", "5", "5", "8");
}
board[0] = ["1", "4", "4", "4", "4", "4", "4", "4", "4",
            "4", "4", "4", "4", "4", "4", "4", "4", "4", "7"];
board[18] = ["3", "6", "6", "6", "6", "6", "6", "6", "6",
            "6", "6", "6", "6", "6", "6", "6", "6", "6", "9"];
board[3][3] = "H"; board[3][9] = "H"; board[3][15] = "H";
board[9][3] = "H"; board[9][9] = "H"; board[9][15] = "H";
board[15][3] = "H"; board[15][9] = "H"; board[15][15] = "H";

// Move sequence
var move = new Array();
var mvAdded = new Array();

// VW rectangle
var xstart = 0;
var ystart = 0;
var xend =  18;
var yend =  18;

// Parse "Minimal-SGF" String mSGF
function  parseMSgf() {
  var sgf = mSGF.split(";");
  var a = "a";

  var numAdded = 0;
  var mm = 0;
  for(m=1;m<sgf.length;m++) {
    var firstChar = sgf[m].charAt(0);
    if(firstChar == "V") { 
      var secondChar = sgf[m].charAt(1);
      if(secondChar == "W") {
	xstart = sgf[1].charCodeAt(3) - a.charCodeAt(0);
	ystart = sgf[1].charCodeAt(4) - a.charCodeAt(0);
	xend =  sgf[1].charCodeAt(6) - a.charCodeAt(0);
	yend =  sgf[1].charCodeAt(7) - a.charCodeAt(0);
      } // else if(secondChar == "[") it's value attribute.
    } else if(firstChar == "A") {
      var aColor = sgf[m].charAt(1);
      if(aColor == "B" || aColor == "W" || aColor == "E") {
        var mvAd = sgf[m].split("[");
        for(h=1;h<mvAd.length;h++) {
          var r = mvAd[h].charCodeAt(0) - a.charCodeAt(0);
          var s = mvAd[h].charCodeAt(1) - a.charCodeAt(0);
          mvAdded[numAdded] = new Object();
          mvAdded[numAdded].color = aColor;
          mvAdded[numAdded].x = r;
          mvAdded[numAdded].y = s;
          numAdded++;
        }
      } //else ignore...
    } else if(firstChar == "E") {
      var mvDel = sgf[m].split("[");
      for(h=1;h<mvDel.length;h++) {
        var c = mvDel[h].charCodeAt(0) - a.charCodeAt(0);
        var d = mvDel[h].charCodeAt(1) - a.charCodeAt(0);
	  move[mm] = new Object();
	  move[mm].color = "E";
	  move[mm].x = c;
	  move[mm].y = d;
	  mm++;
      }
    } else if(firstChar == "B" || firstChar == "W") {
      var p = sgf[m].charCodeAt(2) - a.charCodeAt(0);
      var q = sgf[m].charCodeAt(3) - a.charCodeAt(0);
      move[mm] = new Object();
      move[mm].color = sgf[m].charAt(0);
      move[mm].x = p;
      move[mm].y = q;
      mm++;
    } //else do nothing...
  }
}

function  displayGoBoard() {

  // [1] "Parse" Minimal-SGF String
  parseMSgf();

  // [2] Write move sequence
  hFile.WriteLine('<SCRIPT type="text/javascript" language="JavaScript">');
  hFile.WriteLine('var move = new Array();');
  for(n=0;n<move.length;n++) {
    hFile.WriteLine(' move['+n+'] = new Object();');
    hFile.Write(' move['+n+'].color = "' + move[n].color + '";');
    hFile.Write(' move['+n+'].x = ' + move[n].x + ';');
    hFile.WriteLine(' move['+n+'].y = ' + move[n].y + ';');
  }
  hFile.WriteLine('var mvAdded = new Array();');
  for(n=0;n<mvAdded.length;n++) {
    hFile.WriteLine(' mvAdded['+n+'] = new Object();');
    hFile.Write(' mvAdded['+n+'].color = "' + mvAdded[n].color + '";');
    hFile.Write(' mvAdded['+n+'].x = ' + mvAdded[n].x + ';');
    hFile.WriteLine(' mvAdded['+n+'].y = ' + mvAdded[n].y + ';');
  }
  hFile.WriteLine('</SCR' + 'IPT>');

  // [3] Write heading
  hFile.WriteLine('<H2>');
  hFile.WriteLine(fileHeader);
  hFile.WriteLine('</H2>');

  var boardWidth = (xend-xstart+1)*15;
  var boardHeight = (yend-ystart+1)*15;

  // [4-1] Draw outer table  
  hFile.Write('<TABLE>');
  hFile.Write('<TBODY>');
  hFile.Write('<TR>');
  hFile.WriteLine('<TD noWrap vAlign="top">');

  // [5] "Draw" Go Board
  hFile.Write('<TABLE class="goboard" id="boardlayer">');
  hFile.WriteLine('<TBODY>');
  hFile.Write('<TR>');
  hFile.WriteLine('<TD noWrap>');
  for(j=ystart;j<=yend;j++) {
    for(i=xstart;i<=xend;i++) {
      hFile.Write('<IMG name="board_'+i+'_'+j+'" src="gifs/' + board[i][j] + '.gif">');
    }
    hFile.WriteLine('<BR>');
  }
  hFile.Write('</TD>');
  hFile.WriteLine('</TR>');
  hFile.Write('</TBODY>');
  hFile.WriteLine('</TABLE>');

  // [6] Show move-buttons
  hFile.Write('<DIV class="arrows" ');
  hFile.WriteLine('style="width: ' + boardWidth + ';">');
  hFile.Write('&nbsp;<A href="javascript:moveToBeginning();" ');
  hFile.Write('onMouseOver="self.status=\'Move To Beginning\'; return true;"');
  hFile.Write('onMouseOut="self.status=\'\'; return true;"');
  hFile.Write('title="Move To Beginning"');
  hFile.WriteLine('><IMG src="gifs/lleft.gif" border="0"></A>&nbsp;&nbsp;');
  hFile.Write('<A href="javascript:moveBackward();" ');
  hFile.Write('onMouseOver="self.status=\'Move Backward\'; return true;"');
  hFile.Write('onMouseOut="self.status=\'\'; return true;"');
  hFile.Write('title="Move Backward"');
  hFile.WriteLine('><IMG src="gifs/left.gif" border="0"></A>&nbsp;&nbsp;');
  hFile.Write('<A href="javascript:playMoves();"');
  hFile.Write('onMouseOver="self.status=\'Continuous Play\'; return true;"');
  hFile.Write('onMouseOut="self.status=\'\'; return true;"');
  hFile.Write('title="Continuous Play"');
  hFile.WriteLine('><IMG src="gifs/clock.gif" border="0"></A>&nbsp;&nbsp;');
  hFile.Write('<A href="javascript:moveForward();"');
  hFile.Write('onMouseOver="self.status=\'Move Forward\'; return true;"');
  hFile.Write('onMouseOut="self.status=\'\'; return true;"');
  hFile.Write('title="Move Forward"');
  hFile.WriteLine('><IMG src="gifs/right.gif" border="0"></A>&nbsp;&nbsp;');
  hFile.Write('<A href="javascript:moveToEnd();"');
  hFile.Write('onMouseOver="self.status=\'Move To End\'; return true;"');
  hFile.Write('onMouseOut="self.status=\'\'; return true;"');
  hFile.Write('title="Move To End"');
  hFile.WriteLine('><IMG src="gifs/rright.gif" border="0"></A>');
  hFile.WriteLine('</DIV>');

  // [4-2] Draw outer table  
  hFile.WriteLine('</TD><TD vAlign="top">');
  hFile.WriteLine('<DIV class="comments">');
  //hFile.WriteLine('comments here');
  hFile.WriteLine(commentsHolder);
  hFile.WriteLine('</DIV>');
  hFile.Write('</TD>');
  hFile.Write('</TR>');
  hFile.Write('</TBODY>');
  hFile.WriteLine('</TABLE>');

  // [7] Start sripting
  hFile.WriteLine('<SCRIPT type="text/javascript" language="JavaScript">');
  hFile.WriteLine('milliSec = 1000;');
  hFile.WriteLine('var initDelay = 2000;');
  hFile.WriteLine('addMoves();');
  hFile.WriteLine('window.setTimeout ("playMoves();",initDelay);');
  hFile.WriteLine('</SCR' + 'IPT>');

  // [8] Write the rest of the document here
  hFile.WriteLine('<DIV class="mainbody">');
  hFile.WriteLine('<!-- Main part here -->');
  hFile.WriteLine(' ');
  hFile.WriteLine('</DIV>');

  // [9] Copyright statement
  hFile.WriteLine('<HR>');
  hFile.WriteLine('<SCRIPT type="text/javascript" language="JavaScript"');
  hFile.WriteLine('src="copyright.js">');
  hFile.WriteLine('</SCR' + 'IPT>');
}


// Main part
//filePath = "E:\\JScript\\goboard\\test.htm";
//boardColor = "gold";
//backgroundColor = "ivory";

// [1] hua-jum

commentsHolder = "<!-- Comments Here --> ";
fileTitle = "IJL: Internet Joseki Library | Hua-Jum Joseki";
filePath = "H001.html";
fileHeader = "Hua-Jum 001";

mSGF = "(;VW[ga:sm];B[pd];W[qf];B[nd];W[rd];B[qc];W[qi];B[jc])";
//mSGF = "(;VW[ga:sm];B[pd];W[qf];B[nd];W[qj];B[jc])";
//mSGF = "(;VW[ga:sm];B[pd];W[qf];B[nd];W[rd];B[qh];W[qc];B[qe];W[re];B[pf];W[ob];B[rf])";

//mSGF = "(;VW[ga:sm];B[pd];W[qf];B[pf];W[pg];B[of];W[qe];B[qd];W[qj];B[jc])";
//mSGF = "(;VW[ga:sm];B[pd];W[qf];B[pf];W[pg];B[of];W[qe];B[qd];W[qj];B[jc];W[nc];B[oc];W[nd];B[pe];W[kd];B[jd];W[kf];B[je])";
//mSGF = "(;VW[ga:sm];B[pd];W[qf];B[pf];W[pg];B[of];W[qd];B[qe];W[re];B[pe];W[rg];B[qc];W[rd];B[og];W[ph];B[jc])";

//mSGF = "(;VW[ga:sm];AB[jc][pd];AW[qf];B[qh];W[qc];B[pc];W[qd];B[pe];W[rf];B[og])";
//mSGF = "(;VW[ga:sm];AB[jc][pd];AW[qf];B[qh];W[qc];B[pc];W[qd];B[pe];W[qb];B[pf];W[rf];B[oh])";

//mSGF = "(;VW[ga:sm];AB[jc][pd];AW[qf];B[pi];W[qc];B[pc];W[qd];B[pe];W[pf];B[qe];W[re];B[rd];W[rc];B[rf];W[sd];E[rd];B[of];W[og];B[pg];W[qg];B[rg];W[qh];B[ph];W[ri];B[nf])";

//mSGF = "(;VW[ga:sm];B[pd];W[nc];B[jc];W[qc];B[pc];W[qd];B[pe];W[pb];B[ob];W[qb];B[oc];W[qf];B[nd])";

//mSGF = "(;VW[ga:sm];B[pd];W[nc];B[jc];W[qf];B[pf];W[pg];B[of];W[qe];B[qd];W[md];B[oc];W[ob];B[nd];W[nb];B[pb];W[mf];B[ng];W[qi];B[rd])";
//mSGF = "(;VW[ga:sm];B[pd];W[nc];B[jc];W[qf];B[oe];W[qc];B[pc];W[qd];B[og])";

//mSGF = "(;VW[ga:sm];B[pd];W[nc];B[jc];W[qf];B[pf];W[pg];B[of];W[qc];B[qe];W[pc];B[qg])";
//mSGF = "(;VW[ga:sm];B[pd];W[nc];B[jc];W[qf];B[pf];W[pg];B[of];W[qc];B[pc];W[qd];B[qe];W[re];B[pe];W[rg];B[og];W[ph];B[je])";

//mSGF = "(;VW[ga:sm];B[pd];W[nc];B[jc];W[pf];B[nd];W[md];B[ne];W[oc];B[pe];W[qb];B[qf])";
//mSGF = "(;VW[ga:sm];B[pd];W[nc];B[jc];W[pf];B[of];W[qc];B[pc];W[qd];B[pe];W[qe];B[ne];W[pg];B[je])";
//mSGF = "(;VW[ga:sm];B[pd];W[nc];B[jc];W[pf];B[of];W[og];B[oe];W[qc];B[pc];W[qd];B[qb];W[pe];B[nd];W[pg];B[je])";


// [2] soh-mok
/*
commentsHolder = "<!-- Comments Here --> ";
fileTitle = "IJL: Internet Joseki Library | Soh-Mok Joseki";
filePath = "S013b.html";
fileHeader = "Soh-Mok 013b";

//mSGF = "(;VW[ga:sm];B[qd];W[oc];B[mc];W[oe];B[pf];W[ld];B[pc];W[ob])";
//mSGF = "(;VW[ga:sm];B[qd];W[oc];B[mc];W[oe];B[pf];W[ld];B[pc];W[ob];B[lc];W[kd];B[md];W[me];B[kc];W[jd];B[ic];W[nd];B[ph])";

//mSGF = "(;VW[ga:sm];B[qd];W[oc];B[mc];W[oe];B[pf];W[ph];B[of];W[md];B[lc];W[nf];B[ng];W[mf];B[oh];W[pj];B[pc];W[nc];B[ic])";

//mSGF = "(;VW[ga:sm];B[qd];W[oc];B[mc];W[pe];B[pd];W[od];B[oe];W[ne];B[of];W[md];B[pf];W[lc])";
//mSGF = "(;VW[ga:sm];B[qd];W[oc];B[mc];W[pe];B[pd];W[od];B[oe];W[ne];B[of];W[md];B[ld];W[le];B[kd];W[ke];B[jd];W[re];B[qb];W[qe];B[ob])";

//mSGF = "(;VW[ga:sm];B[qd];W[oc];B[mc];W[md];B[ld];W[nd];B[qg];W[lc];B[kc];W[lb];B[kd];W[mb])";
//mSGF = "(;VW[ga:sm];B[qd];W[oc];B[mc];W[md];B[ld];W[nd];B[kc];W[pf];B[qf];W[qg];B[pg];W[qe];B[rf];W[of];B[re];W[ph];B[qb];W[og];E[pg])";

//mSGF = "(;VW[ga:sm];B[qd];W[oc];B[mc];W[md];B[ld];W[me];B[nc];W[od];B[pf];W[of];B[pg];W[jd];B[le];W[lf];B[ke])";

//mSGF = "(;VW[ga:sm];B[qd];W[oc];B[md];W[oe];B[pf];W[kd];B[mf];W[mc])";
//mSGF = "(;VW[ga:sm];B[qd];W[oc];B[md];W[oe];B[pf];W[kd];B[mf];W[mc];B[nd];W[nc];B[od];W[pc];B[pd])";

//mSGF = "(;VW[ga:sm];B[qd];W[oc];B[md];W[ne];B[pf];W[qc];B[pd];W[pc];B[nd];W[od];B[oe];W[nf])";

//mSGF = "(;VW[ga:sm];B[qd];W[oc];B[ld];W[nd];B[pf];W[qb];B[qj])";

//mSGF = "(;VW[ga:sm];B[qd];W[oc];B[ld];W[nd];B[pc];W[pb];B[qb];W[pd];B[qc];W[pe];B[qf])";

//10
//mSGF = "(;VW[ga:sm];B[qd];W[oc];B[ld];W[nd];B[pc];W[pe];B[pd];W[qe];B[od];W[oe];B[nc];W[mc];B[ob];E[oc];W[md];B[ne])";

//mSGF = "(;VW[ga:sm];B[qd];W[oc];B[ld];W[nd];B[oe];W[pd];B[qe];W[pe];B[qf];W[pf];B[pg];W[qc];B[rc];W[qb])";
//mSGF = "(;VW[ga:sm];B[qd];W[oc];B[ld];W[nd];B[oe];W[pd];B[qe];W[pe];B[pf];W[qc];B[qg];W[rc];B[ne])";

//mSGF = "(;VW[ga:sm];B[qd];W[oc];B[ld];W[pe];B[pd];W[od];B[oe];W[md];B[of];W[me];B[qg];W[lc])";
//mSGF = "(;VW[ga:sm];B[qd];W[oc];B[ld];W[pe];B[pd];W[od];B[oe];W[md];B[ne];W[me];B[pf];W[lc])";

//mSGF = "(;VW[ga:sm];B[qd];W[oc];B[ld];W[of];B[qg];W[id];B[mf])";
//mSGF = "(;VW[ga:sm];B[qd];W[oc];B[ld];W[of];B[qg];W[id];B[mf];W[pd];B[qc];W[qe];B[re];W[rf];B[qf];W[pe];B[rd];W[oh])";

mSGF = "(;VW[ga:sm];B[qd];W[oc];B[ld];W[of];B[qg];W[id];B[mf];W[pd];B[qe];W[qc];B[rc];W[pf];B[qb];W[qf];B[rf];W[pc];B[rd];W[oh];B[nd];W[oe])";

//mSGF = "(;VW[ga:sm];B[qd];W[oc];B[ld];W[of];B[oe];W[ne];B[pe];W[nd];B[nf];W[mf];B[ng];W[le];B[og];W[kd])";
//mSGF = "(;VW[ga:sm];B[qd];W[oc];B[ld];W[of];B[oe];W[ne];B[pe];W[nd];B[nf];W[mf];B[ng];W[le];B[jd];W[qf];B[pf];W[og];B[pg];W[oh];B[qi];W[oj])";

//mSGF = "(;VW[ga:sm];B[qd];W[oc];B[ld];W[of];B[pg];W[og];B[pf];W[oh];B[oe];W[ne];B[nd];W[od];B[pe];W[md];B[nc];W[mc];B[nb];W[me];B[ob])";

//mSGF = "(;VW[ga:sm];B[qd];W[oc];B[ld];W[me];B[od];W[md];B[pc];W[ob];B[mc];W[nd];B[oe];W[lc];B[pb];W[kc])";
//mSGF = "(;VW[ga:sm];B[qd];W[oc];B[ld];W[me];B[od];W[md];B[pc];W[ob];B[mc];W[nd];B[nc];W[oe];B[pd];W[lc];B[lb];W[kc];B[nb];W[kb];B[ma];W[ke])";

//mSGF = "(;VW[ga:sm];B[qd];W[oc];B[kc];W[pd];B[qe];W[qc];B[rc];W[qb];B[pg])";

*/


CreateHtmlFile();
displayGoBoard();
CloseHtmlFile();
