/************************************************************
 *  playGoBoard.js
 *  Copyright 1999, Hyoungsoo Yoon. All Rights Reserved.
 *  This software is distributed on an "AS IS" basis, 
 *  WITHOUT WARRANTY OF ANY KIND, either express or implied.
 ************************************************************/

// Browser sniffer
var isNav4Up = false; 
var isIE4Up = false;
if (parseInt(navigator.appVersion.charAt(0)) >= 4) {
   if (navigator.appName == "Netscape") {
      isNav4Up = true
   } else if (navigator.appVersion.indexOf("MSIE") != -1) {
      isIE4Up = true
   }
}

// Write style tags here.
document.writeln('<STYLE type="text/css">');
document.writeln('TABLE.goboard {');
document.writeln('border-collapse: collapse;');
document.writeln('width: 0.0in;');
document.writeln('margin: 0px;');
document.writeln('padding: 0.0in;');
document.writeln('border: 0.0in;');
document.writeln('}');
document.writeln('TABLE.goboard TR {');
document.writeln('margin: 0in;');
document.writeln('padding: 0in;');
document.writeln('border: 0in;');
document.writeln('}');
document.writeln('TABLE.goboard TD {');
document.writeln('margin: 0in;');
document.writeln('padding: 0in;');
document.writeln('}');
document.write('#boardlayer { ');
document.write('position: absolute; ');
document.write('left: ' + lPos +'; top: ' + tPos +'; ');
document.write('visibility: hidden; ');
document.writeln('}');
document.write('#movelayer { ');
document.write('position: absolute; ');
document.write('left: ' + lPos +'; top: ' + tPos +'; ');
document.write('visibility: hidden; ');
document.writeln('}');
document.write('SPAN.moves { ');
document.write('position: relative; ');
document.write('visibility: hidden; ');
document.write('left: 0; top: 0; ');
document.writeln('}');
document.writeln('</STYLE>');

// Default (empty) board
var board = new Array();
for(k=0;k<19;k++) {
  board[k] = new Array("2", "5", "5", "5", "5", "5", "5", "5", "5",
             "5", "5", "5", "5", "5", "5", "5", "5", "5", "8");
}
board[0] = ["1", "4", "4", "4", "4", "4", "4", "4", "4",
            "4", "4", "4", "4", "4", "4", "4", "4", "4", "7"];
board[18] = ["3", "6", "6", "6", "6", "6", "6", "6", "6",
            "6", "6", "6", "6", "6", "6", "6", "6", "6", "9"];
board[3][3] = "H"; board[3][9] = "H"; board[3][15] = "H";
board[9][3] = "H"; board[9][9] = "H"; board[9][15] = "H";
board[15][3] = "H"; board[15][9] = "H"; board[15][15] = "H";

// Move sequence
var move = new Array();

// Global variables
var boardColor = "gold";
var mSGF = "";
var milliSec = 1000;
var lPos = 0;
var tPos = 0;

function  playGoBoard() {

  // [1] "Parse" Minimal-SGF String
  var sgf = mSGF.split(";");

  var a = "a";
  var xstart = 0;
  var ystart = 0;
  var xend =  18;
  var yend =  18;

  var mStart = 2;
  if(sgf[1].charAt(0) == "V") { 
    xstart = sgf[1].charCodeAt(3) - a.charCodeAt(0);
    ystart = sgf[1].charCodeAt(4) - a.charCodeAt(0);
    xend =  sgf[1].charCodeAt(6) - a.charCodeAt(0);
    yend =  sgf[1].charCodeAt(7) - a.charCodeAt(0);
  } else {
    mStart = 1;
  }
  for(m=mStart;m<sgf.length;m++) {
    var p = sgf[m].charCodeAt(2) - a.charCodeAt(0);
    var q = sgf[m].charCodeAt(3) - a.charCodeAt(0);
    var mm = m - mStart;
    move[mm] = new Object();
    move[mm].color = sgf[m].charAt(0);
    move[mm].x = p;
    move[mm].y = q;
  }

  var boardWidth = (xend-xstart+1)*15;
  var boardHeight = (yend-ystart+1)*15;

  // [2] "Draw" Go Board
  document.write('<DIV ');
  document.write('style="position: relative; ');
  document.write('height: ' + boardHeight + '; ');
  document.writeln('width: ' + boardWidth + ';">');
  document.write('<TABLE class="goboard" id="boardlayer" ');
  document.write('style="');
  document.write('position: absolute; ');
  document.write('left: ' + lPos +'; top: ' + tPos +'; ');
  document.writeln('background: ' + boardColor + ';">');
  document.write('<TBODY>');
  document.write('<TR>');
  document.writeln('<TD noWrap>');
  for(j=ystart;j<=yend;j++) {
    for(i=xstart;i<=xend;i++) {
      document.write('<IMG src="gifs/' + board[i][j] + '.gif">');
    }
    document.writeln('<BR>');
  }
  document.write('</TD>');
  document.write('</TR>');
  document.write('</TBODY>');
  document.writeln('</TABLE>');

  // [3] Draw moves (hidden)
  document.write('<TABLE class="goboard" id="movelayer" ');
  document.write('style="');
  document.write('position: absolute; ');
  document.write('left: ' + lPos +'; top: ' + tPos +';">');
  document.write('<TBODY>');
  document.write('<TR>');
  document.writeln('<TD noWrap>');
  for(j=ystart;j<=yend;j++) {
    for(i=xstart;i<=xend;i++) {
      var b = false;
      for(n=0;n<move.length;n++) {
        if(move[n].x == i && move[n].y == j) {
          document.write('<SPAN class="moves" id="Move' + n + '">');
          document.write('<IMG src="gifs/' + move[n].color + '.gif">');
          document.write('</SPAN>');
          b = true;
          break;
        }
      }
      if(b == false) {
        document.write('<SPAN ');
        document.write('style="visibility:hidden;">');
        document.write('<IMG src="gifs/' + board[i][j] + '.gif">');
        document.write('</SPAN>');
      }
    }
    document.writeln('<BR>');
  }
  document.write('</TD>');
  document.write('</TR>');
  document.write('</TBODY>');
  document.writeln('</TABLE>');
  document.writeln('</DIV>');

  // [4] Play moves
  currentNumber = 0;
  if(isNav4Up) {
    document.layers.boardlayer.visibility = "show";
    document.layers.movelayer.visibility = "show";
  } else if(isIE4Up) {
    document.all.boardlayer.style.visibility = "visible";
    document.all.movelayer.style.visibility = "visible";
  }

  //window.setTimeout ("playMoves();",2000);
}


function playMoves()
{
  if (currentNumber != move.length) {
     var mv = "Move" + currentNumber;
     if(isNav4Up) {
       document.layers[mv].visibility = "show";
     } else if(isIE4Up) {
       document.all[mv].style.visibility = "visible";
     } else {
       currentNumber = move.length;
       return;
     }
     window.setTimeout ("playMoves();",milliSec);
     currentNumber++;
  }
}

function moveForward()
{
  if (currentNumber != move.length) {
     var mv = "Move" + currentNumber;
     if(isNav4Up) {
       document.layers[mv].visibility = "show";
     } else if(isIE4Up) {
       document.all[mv].style.visibility = "visible";
     }
     currentNumber++;
  }
}

function moveBackward()
{
  if (currentNumber != 0) {
     currentNumber--;
     var mv = "Move" + currentNumber;
     if(isNav4Up) {
       document.layers[mv].visibility = "hide";
     } else if(isIE4Up) {
       document.all[mv].style.visibility = "hidden";
     }
  }
}
