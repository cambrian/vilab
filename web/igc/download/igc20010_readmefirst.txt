Igc2000 is a Windows client program for internet go servers.
The current version is 1.0.

Under the directory, Go/igs_clients/PC,
you can find the following three Igc2000 files.

igc20010.zip:               Igc2000 version 1.0
igc20010_readmefirst.txt:   This file
igc20010_releasenote.htm:   Release note


[Note]

Please read igc20010_releasenote.htm first 
before downloading igc20010.zip. Unzip this file and
run setup.exe to install Igc2000. For more information, 
please go to http://www.vilab.com/igc.

